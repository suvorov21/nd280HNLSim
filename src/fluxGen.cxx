#include "fluxGen.h"

#include <math.h>

#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TH1D.h"
#include "TH2D.h"

#include <TRandom.h>
#include <TRandom1.h>
#include <TRandom2.h>
#include <TRandom3.h>

//oaRuntimeParameters
#include "TParameters.h"
#include "TConfigManager.h"

#include "Units.h"
#include "Utils.h"
#include "TTwoBodyDecay.h"

namespace gen {
  const double gU2 = 1.;

  fluxGen::fluxGen() {
    initialized_ = false;
  }


  //-----------------------------------------------------------------------
  void fluxGen::Initialize(std::string input, std::vector<Double_t>* mass_array) {
    //-----------------------------------------------------------------------
    iniFlux_ = NULL;
    nTrigAll_ = 0;
    inputListFiles_ = input;

    // Near detector configuration to be used
    NDconfig_ = ND::params().GetParameterD("nd280HNLSim.NDconfig");

    // which mode to study Nu or Anti-Nu
    NuMode_ = ND::params().GetParameterD("nd280HNLSim.Mode");
    if (NuMode_ != -1 && NuMode_ != 1) {
      std::cout << "Illegal parameter nd280HNLSim.Mode = " << NuMode_ << std::endl << "Must be '1' or '-1'" << std::endl;
    }

    // Random seed
    Int_t Random = ND::params().GetParameterD("nd280HNLSim.Random");
    delete gRandom;
    switch (Random) {
    case 0:
      gRandom = new TRandom();
      break;
    case 1:
      gRandom = new TRandom1(0);
      break;
    case 2:
      gRandom = new TRandom2(0);
      break;
    case 3:
      gRandom = new TRandom3(0);
      break;
    default:
      gRandom = new TRandom1(0);
      break;
    }

    // Read the flux beam
    chain_ = new TChain("h3002");

    bool ok = ChainInputFiles();

    if (!ok) {
      std::cerr << "Error in 'fluxGen::Initialize'. Failed chaining the files. Cannot proceed." << std::endl;
      exit(1);
    }

    nTrigAll_ *= config::config_man().GetNTriggersInFile();

    if (nTrigAll_ == 0) {
      std::cerr << "Error in 'fluxGen::Initialize'. No POT available. Cannot proceed. " << std::endl;
      exit(1);
    }

    std::cout << "Chained " << chain_->GetEntries() << " entries" << std::endl;

    chain_->SetBranchAddress("ppid",  &ppid_);
    chain_->SetBranchAddress("ppi",   &ppi_);
    chain_->SetBranchAddress("xpi",   &xpi_);
    chain_->SetBranchAddress("npi",   &npi_);
    chain_->SetBranchAddress("ng",    &ng_);
    chain_->SetBranchAddress("norm",  &norm_);
    chain_->SetBranchAddress("cospibm", &cospibm_);

    chain_->SetBranchAddress("Enu",   &Enu_);
    chain_->SetBranchAddress("mode",  &mode_);
    chain_->SetBranchAddress("xnu",   &xnu_);
    chain_->SetBranchAddress("ynu",   &ynu_);

    chain_->SetBranchAddress("idfd",   &idfd_);

    // default Kmu2 branching for massless neutrino
    br_base_ = utils::WidthMesonToHNuLepton(units::mass_charged_kaon, units::f_kaon, units::Vus, units::mass_muon, 0.)  *units::t_charged_kaon;
    //br_base_ = 1.;

    initialized_ = true;

    // detector front square
    plane_ini_ = 4 * config::config_man().GetNDPlaneInit().xSizeND * config::config_man().GetNDPlaneInit().ySizeND;

    if (plane_ini_ == 0) {
      std::cerr << "Error in 'fluxGen::Initialize'. Detector front plane is 0" << std::endl;
      exit(1);
    }

    gHnuMass_ = *mass_array;
  }


  //-----------------------------------------------------------------------
  fluxGen::~fluxGen(){
    //-----------------------------------------------------------------------
    if (initialized_){
      delete chain_;
    }
  }

  //-----------------------------------------------------------------------
  bool fluxGen::Generate(hnlProdMode::hnlProdEnum mode, std::string OutputFile) {
    //-----------------------------------------------------------------------
    // Initialize Hnl production mode
    hnlProdMode ProdMode(mode);

    // check if the mode set is correct
    if (ProdMode.GetProdMode() == hnlProdMode::kInvalid) {
      std::cout << "Unnown HNL production mode " << mode << std::endl << "Skipping this mode" << std::endl;
      return false;
    }
    Int_t mode_number = (Int_t)ProdMode.GetProdMode();

    // create file for flux
    TFile *FluxFile = new TFile(OutputFile.c_str(), "RECREATE");
    FluxFile->cd();

    // create header tree with common information
    TTree* header = new TTree("header", "");

    MinMass_ = gHnuMass_[0];
    MaxMass_ = gHnuMass_[gHnuMass_.size() - 1];
    gSizeMu_ = gHnuMass_.size();

    header->Branch("mode",        &mode_number);
    header->Branch("minmass",     &MinMass_);
    header->Branch("maxmass",     &MaxMass_);
    header->Branch("size",        &gSizeMu_);
    header->Branch("numode",      &NuMode_);
    // Fill header tree
    header->Fill();
    header->Write("", TObject::kOverwrite);

    // find max mass index for this mode due to kinematics
    Int_t MaxMassIndex = -1;
    for (Int_t i = 0; i < gSizeMu_; ++i) {
      if (gHnuMass_[i] >= ProdMode.GetParentMass() - ProdMode.GetProdMass())
        break;
      MaxMassIndex = i;
    }

    std::vector<Double_t> branching;
    branching.resize(MaxMassIndex+1, 0);
    std::vector<TH1D*> HnuFluxHistos;
    HnuFluxHistos.resize(MaxMassIndex+1, NULL);

    for (Int_t i = 0; i <= MaxMassIndex; ++i) {
      // calculate weight of decay
      branching[i] = utils::WidthMesonToHNuLepton(ProdMode.GetParentMass(), ProdMode.GetFormFactor(), ProdMode.GetCabibbo(), ProdMode.GetProdMass(), gHnuMass_[i]) * ProdMode.GetTBase();
      //branching[i] = 1.;

      HnuFluxHistos[i] = new TH1D(Form("hnuFlux%i",  i), "", 150, 0., 15.);
      HnuFluxHistos[i]->GetXaxis()->SetTitle("E, GeV");
      HnuFluxHistos[i]->GetYaxis()->SetTitle("flux[/10^{21} p.o.t./cm^2/GeV]");
      HnuFluxHistos[i]->SetTitleFont(22,"xyz");
      HnuFluxHistos[i]->SetTitleSize(0.05,"xyz");
      HnuFluxHistos[i]->GetYaxis()->SetTitleOffset(0.9);
    }

    baseline_ = new TH1D("baseline","", 150, 0., 15.);
    baseline_->GetXaxis()->SetTitle("E, GeV");
    baseline_->GetYaxis()->SetTitle("flux[/10^{21} p.o.t./cm^{2}/GeV]");
    baseline_->SetTitleFont(22,"xyz");
    baseline_->SetTitleSize(0.05,"xyz");
    baseline_->GetYaxis()->SetTitleOffset(0.9);

    parent_ = new TH2D("parent","", 200, 0., 10., 150, 0, 1.5);
    parent_->GetXaxis()->SetTitle("P, GeV");
    parent_->GetYaxis()->SetTitle("theta w.r.t to beam axis");
    parent_->GetYaxis()->SetTitleOffset(0.9);


    // Neutrino flux vars. There are different from Kaon flux vars!!
    Float_t xpi[3], npi[3], norm, Enu, xnu, ynu, Enu_orig;
    Int_t mass_index, helicity, charge_par;

    TTree* FluxTree = new TTree("hnuflux", "");
    FluxTree->Branch("xpi",         xpi,    "xpi_[3]/F");
    FluxTree->Branch("npi",         npi,    "npi_[3]/F");
    FluxTree->Branch("norm",        &norm);
    FluxTree->Branch("Enu",         &Enu);
    FluxTree->Branch("Enu_orig",    &Enu_orig);
    FluxTree->Branch("xnu",         &xnu);
    FluxTree->Branch("ynu",         &ynu);
    FluxTree->Branch("massI",       &mass_index);
    FluxTree->Branch("helicity",    &helicity);
    FluxTree->Branch("charge_par",  &charge_par);

    kin::TTwoBodyDecay decay(ProdMode.GetParentMass(), ProdMode.GetProdMass(), 0.);

    nEntries_ = chain_->GetEntries();
    Int_t CheckProgress = round(nEntries_ / 5);

    std::cout << "Generating reaction " << ProdMode.GetReaction() << std::endl;

    for (Long64_t i = 0; i < nEntries_; ++i) {

      chain_->GetEntry(i);

      if (i % CheckProgress == 0){
        Double_t ratio = (double)i/nEntries_;
        std::cout << "Entry: " << i << " of " << nEntries_ << " (" << int(100*ratio + 0.5) << "%)" << std::endl;
      }
      //want particular detector configuration so not to double count the events
      if(idfd_ != config::config_man().GetNDConfigInit())
        continue;

      // check parent mode
      if( mode_ != (Int_t)ProdMode.GetParentEnum() && mode_ != (Int_t)ProdMode.GetParentEnum() + 10 )
          continue;

      // get parent charge
      if (mode_/10 == 1 || mode_/10 == 3) charge_par =  1;
      if (mode_/10 == 2 || mode_/10 == 4) charge_par = -1;

      // get parent kinematics
      TVector3 posDecay(xpi_[0], xpi_[1], xpi_[2]);

      //3d momentum
      TVector3 p3dParent(npi_[0], npi_[1], npi_[2]);
      TVector3 p3dParentDir   = p3dParent.Unit();

      p3dParent *= ppi_;

      //original position of the neutrino point in global coordinate
      TVector3 posDet;

      posDet.SetXYZ(config::config_man().GetNDPlaneInit().xPosND+xnu_, config::config_man().GetNDPlaneInit().yPosND+ynu_, config::config_man().GetNDPlaneInit().zPosND);

      //recalculate original weight given parent momentum and position in detector plane
      decay.SetMassProd2(0.);
      std::vector<std::pair<Double_t, Double_t> > weight_energy1;
      weight_energy1 = decay.Weight(p3dParent, posDecay, posDet);

      if(weight_energy1.size() < 1)
        continue;

      Double_t weight1  = weight_energy1[0].first; //exactly one solution for mass-less neutrino
      Enu = weight_energy1[0].second;

      //save origin neutrino energy. May be usefull for systematics analysis
      Enu_orig = Enu_;

      //will go into denominator
      if(weight1 == 0.)
        continue;

      // assume dealing with near detector
      // so the flux is per detector
      // need to account for this to get flux per cm^2

      weight1 *= plane_ini_;

      //scale by gU
      norm_ *= gU2;


      // want to scale everything to have final result for 10^21 pot
      // so that in  original files
      weight1 *= nTrigAll_;
      weight1 /= config::config_man().GetNTriggersInFile();

      Double_t angle  = acos(cospibm_);

      //initial kaon spectra
      parent_->Fill(ppi_, angle, norm_/weight1);

      //throw position at detector plane
      posDet = GenerateDetPos();

      // calculate weights and fill histograms
      Double_t  weight2;
      Int_t     bin;
      Double_t  binWidth;

      // baseline
      decay.SetMassProd1(units::mass_muon);
      decay.SetMassProd2(0.);
      std::vector<std::pair<Double_t, Double_t> > weight_energy2 = decay.Weight(p3dParent, posDecay, posDet);
      for (unsigned int k = 0; k < weight_energy2.size(); ++k) {
        weight2  = weight_energy2[k].first;
        Enu      = weight_energy2[k].second;
        Int_t bin = baseline_->FindBin(Enu);
        Double_t binWidth = baseline_->GetBinWidth(bin);
        baseline_->Fill(Enu, norm_*weight2*pow(weight1, -1.)/(binWidth));
      }

      //want to have flux per GeV!

      // Hnu prodaction for different masses
      decay.SetMassProd1(ProdMode.GetProdMass());
      for (Int_t j = 0; j <= MaxMassIndex; ++j) {
        decay.SetMassProd2(gHnuMass_[j]);
        weight_energy2 = decay.Weight(p3dParent, posDecay,  posDet);

        for (unsigned int k = 0; k < weight_energy2.size(); ++k ) {

          weight2  = weight_energy2[k].first;
          Enu      = weight_energy2[k].second;

          bin = HnuFluxHistos[j]->FindBin(Enu);
          binWidth = HnuFluxHistos[j]->GetBinWidth(bin);
          Double_t FluxWeight = norm_*weight2*pow(weight1, -1.)* branching[j]/(br_base_);
          //Double_t FluxWeight = norm_*weight2*pow(weight1, -1.)/(br_base_);
          if (FluxWeight == 0 || Enu == 0)
            continue;

          xpi[0] = xpi_[0];
          xpi[1] = xpi_[1];
          xpi[2] = xpi_[2];
          npi[0] = ppi_*npi_[0];
          npi[1] = ppi_*npi_[1];
          npi[2] = ppi_*npi_[2];
          norm = FluxWeight;
          xnu = posDet.X() - config::config_man().DetConfig().Plane().xPosND;
          ynu = posDet.Y() - config::config_man().DetConfig().Plane().yPosND;
          mass_index = j;

          // Define helicity
          Double_t rl = pow(ProdMode.GetProdMass()/ProdMode.GetParentMass(), 2);
          Double_t rN = pow(gHnuMass_[j]/ProdMode.GetParentMass(), 2);
          Double_t lambda = 1+pow(rl,2)+pow(rN,2)-2*(rl+rN+rl*rN);
          Double_t polarisation = -charge_par * ( (rl-rN)*sqrt(lambda) )/( rl + rN - pow(rl-rN,2) );

          Double_t rndm = gRandom->Uniform(-1,1);
          if (rndm > polarisation)
            helicity = -1;
          else
            helicity = +1;

          HnuFluxHistos[j]->Fill(Enu, FluxWeight / binWidth);
          FluxTree->Fill();
        }
      }
    }
    for (Int_t i = 0; i < MaxMassIndex; ++i)
      HnuFluxHistos[i]->Write();
    baseline_->Write();

    FluxTree->Write("", TObject::kOverwrite);
    FluxFile->Close();

    FluxFile->Delete();

    return true;
  }


  //-----------------------------------------------------------------------
  bool fluxGen::ChainInputFiles() {
    //-----------------------------------------------------------------------
    nTrigAll_ = 0;

    std::ifstream fList(inputListFiles_.c_str());
    if(fList.good()) {
      while(fList.good()) {
        std::string fileName;
        getline(fList, fileName);
        if(fList.eof()) break;

        config::config_man().ReadInitialFluxConfig(fileName);

        if (chain_->Add(fileName.c_str()))
          nTrigAll_++;

      }
    }

    return config::config_man().GetInitReady();
  }

  //-----------------------------------------------------------------------
  TVector3 fluxGen::GenerateDetPos() {
    //-----------------------------------------------------------------------
    Double_t r1 = gRandom->Rndm();
    Double_t r2 = gRandom->Rndm();

    Double_t x_size = config::config_man().DetConfig().Plane().xSizeND;
    Double_t y_size = config::config_man().DetConfig().Plane().ySizeND;
    //x
    Double_t xtrim = 2.*x_size*r1 - x_size;
    //y
    Double_t ytrim = 2.*x_size*r2 - y_size;

    //pos
    Double_t x_pos = config::config_man().DetConfig().Plane().xPosND;
    Double_t y_pos = config::config_man().DetConfig().Plane().yPosND;
    Double_t z_pos = config::config_man().DetConfig().Plane().zPosND;


    return TVector3(x_pos + xtrim, y_pos + ytrim, z_pos);
  }
}