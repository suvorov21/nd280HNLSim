#include "TUnitsParser.h"
#include "Units.h"
#include <iostream>
#include <sstream>
#include <string>
#include <math.h>

TUnitsParser::TUnitsParser() {

  // Define units. Add to this list as needed.

  // Length
  units_["km"] = units::km;
  units_["m"] = units::m;
  units_["cm"] = units::cm;
  units_["mm"] = units::mm;
  units_["um"] = units::micrometer;
  units_["nm"] = units::nanometer;
  units_["Ang"] = units::angstrom;

  // Area
  units_["km2"] = units::km2;
  units_["m2"] = units::m2;
  units_["cm2"] = units::cm2;
  units_["mm2"] = units::mm2;
  units_["barn"] = units::barn;
  units_["mbarn"] = units::millibarn;
  units_["mubarn"] = units::microbarn;
  units_["nbarn"] = units::nanobarn;
  units_["pbarn"] = units::picobarn;

  // Volume
  units_["km3"] = units::km3;
  units_["m3"] = units::m3;
  units_["cm3"] = units::cm3;
  units_["mm3"] = units::mm3;

  // Degree
  units_["rad"] = units::radian;
  units_["mrad"] = units::milliradian;
  units_["sr"] = units::steradian;
  units_["deg"] = units::degree;

  // Time
  units_["s"] = units::second;
  units_["ms"] = units::millisecond;
  units_["mus"] = units::microsecond;
  units_["ns"] = units::nanosecond;
  units_["ps"] = units::picosecond;

  // Frequency
  units_["Hz"] = units::hertz;
  units_["kHz"] = units::kilohertz;
  units_["MHz"] = units::megahertz;

  // Electric Charge
  units_["e+"] = units::eplus;
  units_["C"] = units::coulomb;

  // Energy
  units_["eV"] = units::eV;
  units_["keV"] = units::keV;
  units_["MeV"] = units::MeV;
  units_["GeV"] = units::GeV;
  units_["TeV"] = units::TeV;
  units_["PeV"] = units::PeV;
  units_["J"] = units::joule;

  // Energy/Length
  units_["GeV/cm"] = units::GeV / units::cm;
  units_["MeV/cm"] = units::MeV / units::cm;
  units_["keV/cm"] = units::keV / units::cm;
  units_["eV/cm"] = units::eV / units::cm;

  // Inverse energy
  units_["1/eV"] = 1.0 / units_["eV"];
  units_["1/keV"] = 1.0 / units_["keV"];
  units_["1/MeV"] = 1.0 / units_["MeV"];
  units_["1/GeV"] = 1.0 / units_["GeV"];

  // Mass
  units_["mg"] = units::milligram;
  units_["g"] = units::gram;
  units_["kg"] = units::kilogram;

  // Volumic Mass
  units_["g/cm3"] = units::gram / units::cm3;
  units_["mg/cm3"] = units::milligram / units::cm3;
  units_["kg/m3"] = units::kilogram / units::cm3;

  // Mass/Surface
  units_["g/cm2"] = units::gram / units::cm2;
  units_["mg/cm2"] = units::milligram / units::cm2;
  units_["kg/cm2"] = units::kilogram / units::cm2;

  // Surface/Mass
  units_["cm2/g"] = units::cm2 / units::gram;

  // Energy*Surface/Mass
  units_["eV*cm2/g"] = units::eV * units::cm2 / units::gram;
  units_["keV*cm2/g"] = units::keV * units::cm2 / units::gram;
  units_["MeV*cm2/g"] = units::MeV * units::cm2 / units::gram;
  units_["GeV*cm2/g"] = units::GeV * units::cm2 / units::gram;

  // Power
  units_["W"] = units::watt;

  // Force
  units_["N"] = units::newton;

  // Pressure
  units_["Pa"] = units::pascal;
  units_["bar"] = units::bar;
  units_["atm"] = units::atmosphere;

  // Electric current
  units_["A"] = units::ampere;
  units_["mA"] = units::milliampere;
  units_["muA"] = units::microampere;
  units_["nA"] = units::nanoampere;

  // Electric potential
  units_["V"] = units::volt;
  units_["kV"] = units::kilovolt;
  units_["MV"] = units::megavolt;

  // Magnetic flux
  units_["Wb"] = units::weber;

  // Magnetic flux density
  units_["T"] = units::tesla;
  units_["kG"] = units::kilogauss;
  units_["G"] = units::gauss;

  // Speed
  units_["cm/us"] = units_["cm"] / units_["mus"];
  units_["cm/ns"] = units_["cm"] / units_["ns"];
  units_["mm/ns"] = units_["mm"] / units_["ns"];

  // Length/Energy
  units_["mm/MeV"] = units_["mm"] / units_["MeV"];
  units_["mm/keV"] = units_["mm"] / units_["keV"];
  units_["cm/MeV"] = units_["cm"] / units_["MeV"];
  units_["cm/keV"] = units_["cm"] / units_["keV"];

  // Dummy units_ for diffusion coefficient
  units_["cm/sqrt(cm)"] = units_["cm"] / sqrt(units_["cm"]);
  units_["mm/sqrt(cm)"] = units_["mm"] / sqrt(units_["cm"]);
  units_["um/sqrt(cm)"] = units_["um"] / sqrt(units_["cm"]);

  // Dummy units for electron mobility
  units_["cm2/(Vs)"] = units_["cm2"] / (units_["V"] * units_["s"]);
}

TUnitsParser::~TUnitsParser() = default;

std::string TUnitsParser::Convert2DoubleWithUnit(const std::string& input) {

  double value;
  std::string unit;

  std::istringstream line(input);
  if (!(line >> value >> unit)) {
    std::cerr << "ND::UnitsParser: badly formatted input string. Returning 0." << std::endl;
    return "0.0";
  }

  // Check if requested unit is in map.
  if (units_.find(unit) == units_.end()) {
    std::cerr << "ND::UnitsParser: requested unit '" << unit << "' not found. Returning 0." << std::endl;
    return "0.0";
  }

  value = value * units_[unit];

  char s[256];
  sprintf(s, "%f", value);

  return s;

}

void TUnitsParser::PrintListOfUnits() {

  std::cout << std::endl;
  std::cout << "***** List of available units *****" << std::endl << std::endl;
  for (auto & unit : units_)
    std::cout << unit.first << std::endl;
  std::cout << std::endl;

}
