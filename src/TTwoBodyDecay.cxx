#include "TTwoBodyDecay.h"

#include <cmath>


//phase space decay generation and boost
#include "TMath.h"
#include "TRandom1.h"
#include "Units.h"

namespace kin {

  /*!
   * constructor given parent and products masses
   */
  TTwoBodyDecay::TTwoBodyDecay(Double_t M_parent, Double_t M_prod1, Double_t M_prod2) : M_parent_(M_parent){

    if(M_parent_<units::min_mass){
      std::cout<<" kin::TwoBodyDecay(): Parent has too small mass, cannot continue " << M_parent_ << std::endl;
      exit(1);
    }

    if(M_parent_< M_prod1 +  M_prod2){
      std::cout<<" kin::TwoBodyDecay(): Parent has too small mass, cannot continue \n" <<
        M_parent_ << " prod 1 " << M_prod1 << " prod 2 " << M_prod2 <<std::endl;
      exit(1);
    }

    //utility parameters calculation
    norm_ = 1.; //default

    //set product masses
    M_prod_[0] = M_prod1;
    M_prod_[1] = M_prod2;

    //CM variables
    Ecm_prod_[0] = (M_parent_*M_parent_ - M_prod_[1]*M_prod_[1] + M_prod_[0]*M_prod_[0])/(2.0*M_parent_);
    Ecm_prod_[1] = M_parent_ - Ecm_prod_[0];

    Pcm_prod_ = sqrt(Ecm_prod_[1]*Ecm_prod_[1] - M_prod_[1]*M_prod_[1]);

    beta_cm_[0] = beta_cm_[1] = 0.;

    if(Ecm_prod_[0]!=0)
      beta_cm_[0] = Pcm_prod_/Ecm_prod_[0];

    if(Ecm_prod_[1]!=0)
      beta_cm_[1] = Pcm_prod_/Ecm_prod_[1];

  }


  void TTwoBodyDecay::SetMassProd1(Double_t mass) {
    if(M_parent_< mass +  M_prod_[1]){
      std::cout<<" kin::SetMassProd1(): Parent has too small mass, cannot set new prod 1 \n" <<
        M_parent_ << " prod 1 " << mass << "prod 2 " << M_prod_[1] <<std::endl;
      return;
    }
    M_prod_[0] = mass;

    // recalculate CM variables
    Ecm_prod_[0] = (M_parent_*M_parent_ - M_prod_[1]*M_prod_[1] + M_prod_[0]*M_prod_[0])/(2.0*M_parent_);
    Ecm_prod_[1] = M_parent_ - Ecm_prod_[0];

    Pcm_prod_ = sqrt(Ecm_prod_[1]*Ecm_prod_[1] - M_prod_[1]*M_prod_[1]);

    if(Ecm_prod_[0]!=0)
      beta_cm_[0] = Pcm_prod_/Ecm_prod_[0];

    if(Ecm_prod_[1]!=0)
      beta_cm_[1] = Pcm_prod_/Ecm_prod_[1];
 }


  void TTwoBodyDecay::SetMassProd2(Double_t mass) {
    if(M_parent_< mass +  M_prod_[0]){
      std::cout<<" kin::SetMassProd2(): Parent has too small mass, cannot set new prod 2 \n" <<
        M_parent_ << " prod 1 " << M_prod_[0] << "prod 2 " << mass <<std::endl;
      return;
    }

    M_prod_[1] = mass;

    // recalculate CM variables
    Ecm_prod_[0] = (M_parent_*M_parent_ - M_prod_[1]*M_prod_[1] + M_prod_[0]*M_prod_[0])/(2.0*M_parent_);
    Ecm_prod_[1] = M_parent_ - Ecm_prod_[0];

    Pcm_prod_ = sqrt(Ecm_prod_[1]*Ecm_prod_[1] - M_prod_[1]*M_prod_[1]);

    if(Ecm_prod_[0]!=0)
      beta_cm_[0] = Pcm_prod_/Ecm_prod_[0];

    if(Ecm_prod_[1]!=0)
      beta_cm_[1] = Pcm_prod_/Ecm_prod_[1];
 }

  /*!
   * weight
   */
  std::vector<std::pair< Double_t, Double_t> > TTwoBodyDecay::Weight(const TVector3& P3d_parent, const TVector3& pos_decay, const TVector3& pos_det) const {

    std::vector<std::pair< Double_t, Double_t> >  weight_energy;

    Double_t E_prod2 = 0.;
    (void)E_prod2;

    //calculate angle and distance to the detector point
    TVector3 ddir(pos_det -  pos_decay);

    Double_t L  = ddir.Mag();
    ddir        = ddir.Unit();

    //parent energy and momentum
    Double_t Plab_parent        = P3d_parent.Mag();
    Double_t Elab_parent        = sqrt(Plab_parent*Plab_parent + M_parent_*M_parent_);
    Double_t beta_parent        = Plab_parent/Elab_parent;


    Double_t gamma_parent = Elab_parent/M_parent_;

    TVector3 dir(P3d_parent.Unit());

    // theta is the angle between parent direction and  to detector (forced here for second product)
    Double_t theta_lab = dir.Angle(ddir);

    Double_t cos_theta_lab = cos(theta_lab); //cos of theta in lab

    // calculate energy in lab frame
    Double_t tmp    = gamma_parent*gamma_parent*beta_parent*beta_parent*cos_theta_lab*cos_theta_lab;
    Double_t A      = gamma_parent*gamma_parent - tmp;
    Double_t B      = gamma_parent*Ecm_prod_[1];
    Double_t C      = Ecm_prod_[1]*Ecm_prod_[1] + tmp*M_prod_[1]*M_prod_[1];


    // check that the solution is possible
    if(B*B-A*C < 0.){
      weight_energy.emplace_back( 0., 0.);
      return weight_energy;
    }

    // two solutions hold so have to be careful here
    // need to consider two main cases:
    // v_cm > v_parent_lab --> take the largest solution since the smallest corresponds to negative cos_theta_lab
    // v_cm < v_parent_lab --> have to deal with both, each gives its own probability value

    std::vector<int> bin_tmp;
    bin_tmp.push_back(1);


    if(beta_cm_[1]<beta_parent)
      bin_tmp.push_back(-1);

    for (int i : bin_tmp){
      Double_t E2       = (B + i*sqrt(B*B - A*C))/A;
      Double_t P2       = sqrt(E2*E2 - M_prod_[1]*M_prod_[1]);
      Double_t weight   = norm_;

      /*!
       * calculate w(E_lab, Omega_lab) = (P_lab*E_cm)*w(P_cm, Omega_cm)/(P_cm*P_cm)
       * w(P_cm, Omega_cm) = 1/(4*TMath::Pi())*delta(P-P_cm)
       * in order to get the distribution in lab frame need to change the variable under the delta function
       * which basically involves retrieving d(P_cm)/d(P_lab)
       * having two solutions leads to having two final delta-functions in lab frame
       */

      // g_cm
      Double_t g_cm   = beta_parent/beta_cm_[1];

      // D function
      Double_t D      = 1 + gamma_parent*gamma_parent*(1-g_cm*g_cm)*tan(theta_lab)*tan(theta_lab);

      // 1/|d(P_cm)/d(P_lab)|
      Double_t tmp_P  = cos_theta_lab*(g_cm + i*sqrt(D))/(gamma_parent*(1 - beta_parent*beta_parent*cos_theta_lab*cos_theta_lab));
      Double_t my = 1/g_cm;

      // final distribution in the lab frame
      Double_t w_E_Omega = fabs(P2*Ecm_prod_[1]/(Pcm_prod_*Pcm_prod_)*tmp_P);

      //here assume the surface iz Z oriented!!!!!
      weight *= w_E_Omega*ddir.z()/(4*TMath::Pi()*L*L);
      weight_energy.emplace_back(weight, E2);

    }

    return weight_energy;
  }

  /*!
   * generate decay
   */
  bool TTwoBodyDecay::Generate(const TVector3& P3d_parent){

    //parent energy
    Double_t P_parent = P3d_parent.Mag();
    Double_t E_parent = sqrt(P_parent*P_parent + M_parent_*M_parent_);


    TLorentzVector lv(P3d_parent, E_parent);

    if(!event_.SetDecay(lv, 2 , M_prod_))
      return false;

    event_.Generate();

    products_[0] = *event_.GetDecay(0);
    products_[1] = *event_.GetDecay(1);

    return true;

  }

  bool TTwoBodyDecay::Generate(Double_t E_parent, const TVector3& dir_parent){

    //parent energy
    if (E_parent < M_parent_)
      return false;

    Double_t P_parent = sqrt(E_parent*E_parent - M_parent_*M_parent_);

    TVector3 P3d_parent = dir_parent.Unit();
    P3d_parent *= P_parent;

    TLorentzVector lv(P3d_parent, E_parent);

    if(!event_.SetDecay(lv, 2 , M_prod_))
      return false;

    event_.Generate();

    products_[0] = *event_.GetDecay(0);
    products_[1] = *event_.GetDecay(1);

    return true;

  }





}
