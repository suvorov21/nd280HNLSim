#include "THNLDecayGen.h"

#include <math.h>


//phase space decay generation and boost
#include "TMath.h"
#include "TRandom1.h"
#include "Units.h"
#include "Utils.h"

namespace DecGen {
  /*!
   * generate decay
   */
  THNLDecayGen::THNLDecayGen(Double_t ParentMass, const std::vector<Double_t>& productMass, hnlDecayMode::hnlDecayEnum mode) {
    mode_ = mode;
    M_parent_ = ParentMass;
    Products_number_ = productMass.size();
    ProdMass_ = productMass;

    // calculate maximum of differential width for each mode
    long double Xm2, XL, XP, Xe, Xmu, Xm, Xp, a, b, S;
    long double gl, gr;
    switch (mode_) {
    // 2-body decay
    case hnlDecayMode::kMumPip:
      XL = ProdMass_[0] * ProdMass_[0] / (M_parent_ * M_parent_);
      XP = ProdMass_[1] * ProdMass_[1] / (M_parent_ * M_parent_);
      S = 1 + XP*XP + XL*XL - 2*XL - 2*XP - 2*XP*XL;
      MaxWeight_ = (1-XL)*(1-XL) - XP*(1+XL) + sqrt(S)/2 * (1-XL);
      break;
    case hnlDecayMode::kElemPip:
      XL = ProdMass_[0] * ProdMass_[0] / (M_parent_ * M_parent_);
      XP = ProdMass_[1] * ProdMass_[1] / (M_parent_ * M_parent_);
      S = 1 + XP*XP + XL*XL - 2*XL - 2*XP - 2*XP*XL;
      MaxWeight_ = (1-XL)*(1-XL) - XP*(1+XL) + sqrt(S)/2 * (1-XL);
      break;
    case hnlDecayMode::kPimMup:
      XL = ProdMass_[1] * ProdMass_[1] / (M_parent_ * M_parent_);
      XP = ProdMass_[0] * ProdMass_[0] / (M_parent_ * M_parent_);
      S = 1 + XP*XP + XL*XL - 2*XL - 2*XP - 2*XP*XL;
      MaxWeight_ = (1-XL)*(1-XL) - XP*(1+XL) + sqrt(S)/2 * (1-XL);
      break;
    case hnlDecayMode::kPimElep:
      XL = ProdMass_[1] * ProdMass_[1] / (M_parent_ * M_parent_);
      XP = ProdMass_[0] * ProdMass_[0] / (M_parent_ * M_parent_);
      S = 1 + XP*XP + XL*XL - 2*XL - 2*XP - 2*XP*XL;
      MaxWeight_ = (1-XL)*(1-XL) - XP*(1+XL) + sqrt(S)/2 * (1-XL);
      break;

      // 3-body decay
      case hnlDecayMode::kMumElepNue:
      Xm2 = ProdMass_[0] * ProdMass_[0] / (M_parent_ * M_parent_);
      Xe = 0.5 - 0.5*Xm2;
      MaxWeight_ = Xe * (1 - Xe - Xm2) * 2;
      break;

    case hnlDecayMode::kElemMupNue:
      Xm2 = ProdMass_[1] * ProdMass_[1] / (M_parent_ * M_parent_);
      Xe = 0.5 - 0.5*Xm2;
      MaxWeight_ = Xe * (1 - Xe - Xm2) * 2;
      break;

    case hnlDecayMode::kMumElepNumu:
      Xm2 = ProdMass_[0] * ProdMass_[0] / (M_parent_ * M_parent_);
      Xmu = 0.5 + 0.5*Xm2;
      MaxWeight_ = Xmu * (1 - Xmu + Xm2) * 2;
      break;

    case hnlDecayMode::kElemMupNumu:
      Xm2 = ProdMass_[1] * ProdMass_[1] / (M_parent_ * M_parent_);
      Xmu = 0.5 + 0.5*Xm2;
      MaxWeight_ = Xmu * (1 - Xmu + Xm2) * 2;
      break;

    case hnlDecayMode::kElemElepNue:
      Xm2 = ProdMass_[0] * ProdMass_[0] / (M_parent_ * M_parent_);
      a = 3/2 - 2 * units::sin_thw2;
      b = 0.5;
      Xp = 0.5 - Xm2 * (a -b) / (a + b);
      Xm = 0.5 - Xm2 * (a + b) / (a - b);
      MaxWeight_ = (Xp * (1-Xp) * (a+b)*(a+b) + Xm * (1 - Xm)*(a-b)*(a-b) + 2*Xm2*(2 - Xm - Xp) * (a*a - b*b));
      break;
    case hnlDecayMode::kElemElepNuMix:
      gl = -0.5 + units::sin_thw2;
      gr = units::sin_thw2;
      a = 3 * gl - 5 * gr;
      b = gl - gr;
      Xm2 = ProdMass_[0] * ProdMass_[0] / (M_parent_ * M_parent_);
      Xp = 0.5 - Xm2 * (a -b) / (a + b);
      Xm = 0.5 - Xm2 * (a + b) / (a - b);
      MaxWeight_ = (Xp * (1-Xp) * (a+b)*(a+b) + Xm * (1 - Xm)*(a-b)*(a-b) + 2*Xm2*(2 - Xm - Xp) * (a*a - b*b));
      break;

    case hnlDecayMode::kMumMupNumu:
      Xm2 = ProdMass_[0] * ProdMass_[0] / (M_parent_ * M_parent_);
      a = 3/2 - 2 * units::sin_thw2;
      b = 0.5;
      Xp = 0.5 - Xm2 * (a -b) / (a + b);
      Xm = 0.5 - Xm2 * (a + b) / (a - b);
      MaxWeight_ = (Xp * (1-Xp) * (a+b)*(a+b) + Xm * (1 - Xm)*(a-b)*(a-b) + 2*Xm2*(2 - Xm - Xp) * (a*a - b*b));
      break;
    case hnlDecayMode::kMumMupNuMix:
      gl = -0.5 + units::sin_thw2;
      gr = units::sin_thw2;
      a = 3 * gl - 5 * gr;
      b = gl - gr;
      Xm2 = ProdMass_[0] * ProdMass_[0] / (M_parent_ * M_parent_);
      Xp = 0.5 - Xm2 * (a -b) / (a + b);
      Xm = 0.5 - Xm2 * (a + b) / (a - b);
      MaxWeight_ = (Xp * (1-Xp) * (a+b)*(a+b) + Xm * (1 - Xm)*(a-b)*(a-b) + 2*Xm2*(2 - Xm - Xp) * (a*a - b*b));
      break;

    default:
      MaxWeight_ = 1.;
      break;

    }
  }

  long double THNLDecayGen::Generate(const TVector3& P3d_parent) {
    //parent energy
    TVector3 P_parent = P3d_parent.Unit();
    Double_t E_parent = sqrt(P_parent*P_parent + M_parent_*M_parent_);
    return Generate(E_parent, P_parent, P_parent);
  }

  long double THNLDecayGen::Generate(Double_t E_parent, const TVector3& dir_parent, const TVector3& spin) {
    //parent energy
    if (E_parent < M_parent_)
      return 0.;

    Double_t P_parent = sqrt(E_parent*E_parent - M_parent_*M_parent_);

    TVector3 P3d_parent = dir_parent.Unit();
    P3d_parent *= P_parent;

    TLorentzVector lv(P3d_parent, E_parent);

    // generate decay
    if (ProdMass_.size() == 2)  {
      Double_t pm[2];
      pm[0] = ProdMass_[0];
      pm[1] = ProdMass_[1];
      if(!event_.SetDecay(lv, Products_number_ , pm))
        return 0.;
    }

    if (ProdMass_.size() == 3) {
      Double_t pm[3];
      pm[0] = ProdMass_[0];
      pm[1] = ProdMass_[1];
      pm[2] = ProdMass_[2];
      if(!event_.SetDecay(lv, Products_number_ , pm))
        return 0.;
    }

    long double weight = event_.Generate();

    // final Lorentz vectors
    for (Int_t i = 0; i < Products_number_; ++i) {
      products_.push_back(*event_.GetDecay(i));
    }

    // return to CM frame for diff width calculations
    std::vector<TLorentzVector> productsCMF;
    Double_t beta = P_parent / E_parent;
    TVector3 boost = - dir_parent.Unit();
    boost *= beta;

    for (unsigned int i = 0; i < products_.size(); ++i) {
      productsCMF.push_back(products_[i]);
      productsCMF[i].Boost(boost);
    }

    // diff width calculations
    long double Xm2, XL, XP, Xe, Xmu, Weight, Xm, Xp, a, b, cosp, S;
    long double gl, gr;
    switch (mode_) {
    // 2-body decays
    case hnlDecayMode::kMumPip:
      XL = ProdMass_[0] * ProdMass_[0] / (M_parent_ * M_parent_);
      XP = ProdMass_[1] * ProdMass_[1] / (M_parent_ * M_parent_);
      S = 1 + XP*XP + XL*XL - 2*XL - 2*XP - 2*XP*XL;
      cosp = spin.Unit().Dot( productsCMF[0].Vect().Unit() );
      Weight = (1-XL)*(1-XL) - XP*(1+XL) - sqrt(S)/2 * (1-XL) * cosp;
      break;
    case hnlDecayMode::kElemPip:
      XL = ProdMass_[0] * ProdMass_[0] / (M_parent_ * M_parent_);
      XP = ProdMass_[1] * ProdMass_[1] / (M_parent_ * M_parent_);
      S = 1 + XP*XP + XL*XL - 2*XL - 2*XP - 2*XP*XL;
      cosp = spin.Unit().Dot( productsCMF[0].Vect().Unit() );
      Weight = (1-XL)*(1-XL) - XP*(1+XL) - sqrt(S)/2 * (1-XL) * cosp;
      break;
    case hnlDecayMode::kPimMup:
      XL = ProdMass_[1] * ProdMass_[1] / (M_parent_ * M_parent_);
      XP = ProdMass_[0] * ProdMass_[0] / (M_parent_ * M_parent_);
      S = 1 + XP*XP + XL*XL - 2*XL - 2*XP - 2*XP*XL;
      cosp = spin.Unit().Dot( productsCMF[1].Vect().Unit() );
      Weight = (1-XL)*(1-XL) - XP*(1+XL) + sqrt(S)/2 * (1-XL) * cosp;
      break;
    case hnlDecayMode::kPimElep:
      XL = ProdMass_[1] * ProdMass_[1] / (M_parent_ * M_parent_);
      XP = ProdMass_[0] * ProdMass_[0] / (M_parent_ * M_parent_);
      S = 1 + XP*XP + XL*XL - 2*XL - 2*XP - 2*XP*XL;
      cosp = spin.Unit().Dot( productsCMF[1].Vect().Unit() );
      Weight = (1-XL)*(1-XL) - XP*(1+XL) + sqrt(S)/2 * (1-XL) * cosp;
      break;

    // 3-body decays
    case hnlDecayMode::kMumElepNue:
      Xm2 = ProdMass_[0] * ProdMass_[0] / (M_parent_ * M_parent_);
      Xe = 2 * productsCMF[1].E() / M_parent_;
      cosp = spin.Dot( productsCMF[1].Vect().Unit() );
      Weight = Xe * (1 - Xe - Xm2) * (1 + cosp);
      break;
    case hnlDecayMode::kElemMupNue:
      Xm2 = ProdMass_[1] * ProdMass_[1] / (M_parent_ * M_parent_);
      Xe = 2 * productsCMF[0].E() / M_parent_;
      cosp = spin.Dot( productsCMF[0].Vect().Unit() );
      Weight = Xe * (1 - Xe - Xm2) * (1 - cosp);
      break;

    case hnlDecayMode::kMumElepNumu:
      Xm2 = ProdMass_[0] * ProdMass_[0] / (M_parent_ * M_parent_);
      Xmu = 2 * productsCMF[0].E() / M_parent_;
      cosp = spin.Dot( productsCMF[0].Vect().Unit() );
      Weight = Xmu * (1 - Xmu + Xm2) * (1 - cosp);
      break;
    case hnlDecayMode::kElemMupNumu:
      Xm2 = ProdMass_[1] * ProdMass_[1] / (M_parent_ * M_parent_);
      Xmu = 2 * productsCMF[1].E() / M_parent_;
      cosp = spin.Dot( productsCMF[1].Vect().Unit() );
      Weight = Xmu * (1 - Xmu + Xm2) * (1 + cosp);
      break;

    case hnlDecayMode::kElemElepNue:
      Xm2 = ProdMass_[0] * ProdMass_[0] / (M_parent_ * M_parent_);
      a = 3/2 - 2 * units::sin_thw2;
      b = 0.5;
      Xp = productsCMF[1].P();
      Xm = productsCMF[0].P();
      Weight = (Xp * (1-Xp) * (a+b)*(a+b) + Xm * (1 - Xm)*(a-b)*(a-b) + 2*Xm2*(2 - Xm - Xp) * (a*a - b*b));
      break;
    case hnlDecayMode::kElemElepNuMix:
      Xm2 = ProdMass_[0] * ProdMass_[0] / (M_parent_ * M_parent_);
      gl  = -0.5 + units::sin_thw2;
      gr  = units::sin_thw2;
      a = 3 * gl - 5 * gr;
      b = gl - gr;
      Xp = productsCMF[1].P();
      Xm = productsCMF[0].P();
      Weight = (Xp * (1-Xp) * (a+b)*(a+b) + Xm * (1 - Xm)*(a-b)*(a-b) + 2*Xm2*(2 - Xm - Xp) * (a*a - b*b));
      break;

    case hnlDecayMode::kMumMupNumu:
      Xm2 = ProdMass_[0] * ProdMass_[0] / (M_parent_ * M_parent_);
      a = 3/2 - 2 * units::sin_thw2;
      b = 0.5;
      Xp = productsCMF[1].P();
      Xm = productsCMF[0].P();
      Weight = (Xp * (1-Xp) * (a+b)*(a+b) + Xm * (1 - Xm)*(a-b)*(a-b) + 2*Xm2*(2 - Xm - Xp) * (a*a - b*b));
      break;

    case hnlDecayMode::kMumMupNuMix:
      Xm2 = ProdMass_[0] * ProdMass_[0] / (M_parent_ * M_parent_);
      gl  = -0.5 + units::sin_thw2;
      gr  = units::sin_thw2;
      a = 3 * gl - 5 * gr;
      b = gl - gr;
      Xp = productsCMF[1].P();
      Xm = productsCMF[0].P();
      Weight = (Xp * (1-Xp) * (a+b)*(a+b) + Xm * (1 - Xm)*(a-b)*(a-b) + 2*Xm2*(2 - Xm - Xp) * (a*a - b*b));
      break;

    default:
      Weight = 1.;
      std::cerr << __func__ << "\t Decay mode is uninitialized" << std::endl;
      break;
    }

    return weight*Weight / MaxWeight_;
  }
}
