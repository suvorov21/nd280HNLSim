#include "TParameters.h"
#include <cstdlib>

using namespace std;

TParameters* TParameters::fParameters__ = NULL;

TParameters& ND::params() {
  return TParameters::Get();
}

TParameters::TParameters() {
}

TParameters::~TParameters() {
}

/// Read in each parameter file.
void TParameters::ReadInputFile(TString fileName, TString dirName, bool tryFile, bool fixParameters) {
  TString fullFileName;

  // Make full file name.
  if (!dirName.CompareTo(""))
    fullFileName = fileName.Data();
  else
    fullFileName = dirName.Data() + TString("/") + fileName.Data();


  ifstream inputFile(fullFileName.Data(), ios::in);

  // If input file doesn't exist.
  if (!inputFile) {
    // Just return if 'tryFile' is specified.
    if (tryFile) {
      std::cerr << "\n***** TParameters::ReadInputFile *****\n" << "Cannot open input file " << fullFileName.Data() << "." << std::endl;
      return;
    } else { // Otherwise, throw exception.
      std::cerr << "\n***** TParameters::ReadInputFile *****\n" << "Cannot open input file '" << fullFileName.Data() << "'." << std::endl;
      throw TBadParameterFileException();
    }
  }

  int inputState = 0;
  string inputString;
  string parameterName;
  string parameterValueUnit;
  string parameterValue;

  while (inputFile >> inputString) {
    if (inputState == 0) {
      if (inputString == "<")
        inputState = 1;
    } else if (inputState == 1) {
      parameterName = inputString;
      inputState = 2;
    } else if (inputState == 2) {
      if (inputString == "=")
        inputState = 3;
      else {
        std::cerr << "\n***** TParameters::ReadInputFile *****\n"
          "Input file '" << fileName << "'. Last parameter '" << parameterName << "'.\n" << "Cannot find symbol '='.\n"
          << "Badly formatted parameters file." << std::endl;
        throw TBadParameterFileException();
      }
    } else if (inputState == 3) {
      // parameterValue = atof(inputString.c_str());
      parameterValue = inputString.c_str();
      parameterValueUnit = inputString;
      inputState = 4;
    } else if (inputState == 4) {
      if (inputString == ">") {
        // Finished reading. Save parameter; but only if the parameter
        // isn't already 'fixed'
        if (fixedParameters__.find(parameterName) == fixedParameters__.end()) {
          mapOfParameters__[parameterName] = parameterValue;
          // If fixParameters bool is set, fix this parameter now.
          if (fixParameters)
            fixedParameters__.insert(parameterName);
        }
        inputState = 0;
      } else if (inputString == "<") {
        std::cerr << "\n***** TParameters::ReadInputFile *****\n" << "Input file '" << fileName << "'. Last parameter '" << parameterName
          << "'.\n" << "Unexpected symbol '<'.\n" << "Badly formatted parameters file." << std::endl;
        throw TBadParameterFileException();
      } else {
        // The parameter must have a unit. Resave the value with the correct unit.
        parameterValueUnit.append(" ");
        parameterValueUnit.append(inputString);

        // Use ND::TUnitsTableParser class to convert string of value+unit to
        // a double with value+unit.
        parameterValue = fUnitsParser__->Convert2DoubleWithUnit(parameterValueUnit);
      }
    }
  }

  if (inputState != 0) {
    std::cerr << "\n***** TParameters::ReadInputFile *****\n" << "Input file '" << fileName << "'. Last parameter '" << parameterName
      << "'.\n" << "Cannot find symbol '>' at the end of file.\n" << "Badly formatted parameters file." << std::endl;
    throw TBadParameterFileException();
  }

  inputFile.close();
}

void TParameters::PrintListOfParameters() {

  std::cout << "***** TParameters::PrintListOfParameters *****" << std::endl;
  std::cout << "List of parameters:" << std::endl;

  for (mapIterator__ i = mapOfParameters__.begin(); i != mapOfParameters__.end(); i++)
    std::cout << " " << (*i).first << " = " << (*i).second << std::endl;

  std::cout << std::endl;
}

int TParameters::TryLoadingParametersFile(std::string parameterName) {

  std::cout << "Trying to load parameters file for parameter: " << parameterName << std::endl;

  // Figure out the name of the package
  int pos = parameterName.find(".");
  TString packageName(parameterName.c_str(), pos);

  // and the file name for parameters file
  TString fileName = packageName + ".parameters.dat";

  // and the directory of parameters file.
  packageName.ToUpper();
  TString packageROOT = packageName + "ROOT";

  TString dirName = getenv(packageROOT.Data()) + TString("/parameters/");

  // Now try reading in this file. Last input variable is set to true,
  // indicating that we don't want to throw exception if a file is not found.
  ReadInputFile(fileName, dirName, true);

  // Now try to find this parameter again
  mapIterator__ i = mapOfParameters__.find(parameterName);
  if (i != mapOfParameters__.end())
    return 1;
  else {
    return 0;
  }
}

void TParameters::ReadParamOverrideFile(TString filename) {
  std::cout << "Using TParameters override file = " << filename << std::endl;

  // Setting final input variable to true forces the parameters
  // that are loaded to be 'fixed'; ie immutable.
  ReadInputFile(filename, "", false, true);
}

bool TParameters::HasParameter(string parameterName) {

  mapIterator__ i = mapOfParameters__.find(parameterName);

  if (i != mapOfParameters__.end())
    return 1;
  else {
    // Okay, we didn't find this parameter on the first try.
    // Let's see if we can try loading the parameters file.
    // The function will return whether this parameter
    // was found afterwards.
    return TryLoadingParametersFile(parameterName);
  }
}

int TParameters::GetParameterI(string parameterName) {
  if (HasParameter(parameterName)) {
    return atoi(mapOfParameters__[parameterName].c_str());
  } else {
    std::cerr << "\n***** TParameters::GetParameterAsInteger *****\n" << "Cannot find parameter '" << parameterName << "'." << std::endl;
    throw TNoSuchParameterException();
  }

  return -1;
}

double TParameters::GetParameterD(string parameterName) {
  if (HasParameter(parameterName)) {
    return atof(mapOfParameters__[parameterName].c_str());
  } else {
    std::cerr << "\n***** TParameters::GetParameterAsDouble *****\n" << "Cannot find parameter '" << parameterName << "'." << std::endl;
    throw TNoSuchParameterException();
  }

  return -1;

}

std::string TParameters::GetParameterS(string parameterName) {
  if (HasParameter(parameterName)) {
    return mapOfParameters__[parameterName];
  } else {
    std::cerr << "\n***** TParameters::GetParameterAsString *****\n" << "Cannot find parameter '" << parameterName << "'." << std::endl;
    throw TNoSuchParameterException();
  }

  return std::string();
}

void TParameters::ClearMapOfParameters() {
  mapOfParameters__.clear();
}
