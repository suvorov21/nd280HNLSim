#ifndef TUNITSPARSER_H
#define TUNITSPARSER_H
#include "TString.h"
#include <iostream>
#include <string>
#include <fstream>
#include <map>

/// This class provides a method for converting a
/// string like "1.5 cm" into a double with the
/// appropriate unit. To do so it defines a set
/// of units, using the same base units as in
/// oaEvent/src/HEPUnits.hxx: ie mm, ns, MeV...
/// Only a fairly limited set of units is defined.
class TUnitsParser {

  public:

    /// Constructor that creates the list of units.
    /// Try not to call this too often.
    TUnitsParser();
    ~TUnitsParser();

    /// Converts a string like "1.5 cm" into a double
    /// with the appropriate units.
    std::string Convert2DoubleWithUnit(const std::string& line);

    /// Prints all the defined units.
    void PrintListOfUnits();

  private:

    /// Internal map of unit ==> conversion factor.
    std::map<std::string, double> units_;
};

#endif  
