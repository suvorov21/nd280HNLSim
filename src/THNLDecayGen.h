#ifndef THNLDECAYGEN_H
#define THNLDECAYGEN_H

#include <iostream>

#include "TLorentzVector.h"
#include "TVector3.h"
#include "TGenPhaseSpace.h"

#include "Utils.h"

namespace DecGen {
  class THNLDecayGen {
  public:
    /// constructor
    THNLDecayGen(Double_t, const std::vector<Double_t>&, hnlDecayMode::hnlDecayEnum);
    virtual ~THNLDecayGen() = default;;

    /// get decay products in lab frame given momentum
    long double Generate(const TVector3&);

    /// get decay products in lab frame given energy and direction
    long double Generate(Double_t, const TVector3&, const TVector3&);

    /// get products lorentz vectors
    std::vector<TLorentzVector> GetProducts() {return products_;};
  protected:
    hnlDecayMode::hnlDecayEnum mode_;
    long double MaxWeight_;

    TGenPhaseSpace event_;

    Double_t M_parent_;

    Int_t Products_number_;
    std::vector<Double_t> ProdMass_;
    std::vector<TLorentzVector> products_;
  };

}

#endif
