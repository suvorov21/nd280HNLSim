#include "HeavyNu.h"  // project libs-------------------
#include "fluxGen.h"
#include "Units.h"
#include "Utils.h"
#include "TParameters.h"
#include "TTwoBodyDecay.h"
#include "THNLDecayGen.h"
#include "TConfigManager.h"

#include <iostream>   // standard libs------------------
#include <cstdlib>    // for std::exit
#include <string>
#include <unistd.h>
#include <sstream>      // for str to int conversion


#include "TFile.h"    // Root libs----------------------
#include "TTree.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraph.h"
#include "TLegend.h"
#include "TPad.h"
#include "TStyle.h"
#include "TBits.h"
#include "TSpline.h"
#include "TColor.h"
#include "TROOT.h"
#include <TRandom.h>
#include <TRandom1.h>
#include <TRandom2.h>
#include <TRandom3.h>
#include "TString.h"

//#define DEBUG

//-----------------------------------------------------------------------
HeavyNu::HeavyNu(int argc, char *argv[]) {
  //-----------------------------------------------------------------------
  std::string programName = argv[0];
  // clear vars
  inputListFiles_ = "";
  outputFileDir_ = "";
  regenerateFluxHnu_ = doSecParticlePlot_ = doEventsNumberPlot_ = regenerateFluxHnu_ = doFluxPlot_ = false;

  tpc_.clear();

  for (;;) {
    int c = getopt(argc, argv, "o:dsfmn:p:");
    if (c < 0) break;
    switch (c) {
      case 'o' : outputFileDir_       = optarg; break;
      case 'd' : doSecParticlePlot_   = true;   break;
      case 's' : doEventsNumberPlot_  = true;   break;
      case 'f' : doFluxPlot_          = true;   break;
      case 'm' : doControlScript_     = true;   break;
      case 'n' : regenerateFluxHnu_   = true;   inputListFiles_ = optarg; break;
      case 'p' : ND::params().ReadParamOverrideFile(optarg); break;
      case '?' : PrintUsage(programName);
    }
  }
  if (argc == 1)
    PrintUsage(programName);
  // If we need to recalculate Heavy nu flux
  if (regenerateFluxHnu_) {
    // Check if there are extra parameters
    if (argc != optind) {
      std::cerr << "Error in 'HeavyNu::HeavyNu'. Unknown parameters detected." << std::endl << std::endl;
      PrintUsage(programName);
    }

  } else {
    // read input flux files names
    FluxFileName_.resize(argc - optind);
    for (Int_t i = 0; i < argc - optind; ++i)
      FluxFileName_[i] = argv[optind + i];
  }

  if (outputFileDir_.compare("") == 0) {
    outputFileDir_ += getenv("ND280HNLSIMROOT");
    outputFileDir_ += "/MCStudy/";
  }
  std::cout << "Output directory is " << outputFileDir_ << std::endl;
}

//-----------------------------------------------------------------------
HeavyNu::~HeavyNu() {
  //-----------------------------------------------------------------------
  tpc_.clear();
}

//-----------------------------------------------------------------------
void HeavyNu::Initialize() {
  //-----------------------------------------------------------------------

  Int_t Random = ND::params().GetParameterD("nd280HNLSim.Random");
  delete gRandom;
    switch (Random) {
    case 0:
      gRandom = new TRandom();
      break;
    case 1:
      gRandom = new TRandom1(0);
      break;
    case 2:
      gRandom = new TRandom2(0);
      break;
    case 3:
      gRandom = new TRandom3(0);
      break;
    default:
      gRandom = new TRandom1(0);
      break;
  }

  POTscale_         = ND::params().GetParameterD("nd280HNLSim.POT");
  rdpPOT_           = ND::params().GetParameterD("nd280HNLSim.rdpPOT");
  if (rdpPOT_) {
    rdpNuPOT_    = (Float_t) ND::params().GetParameterD("nd280HNLSim.rdpPOTNu");
    rdpNuBarPOT_ = (Float_t) ND::params().GetParameterD("nd280HNLSim.rdpPOTNubar");
  }

  _anuNorm = new TGraph();
  // get Anu/nu data from file
  TString fileName = getenv("ND280HNLSIMROOT") + TString("/data/");
  TString Filename = fileName + "FluxA.dat";
  if (!LoadLimitGraph(_anuNorm, Filename.Data())) {
    std::cout << "WARNING in HeavyNu::Initialize. Something went wrong while making graph from Anu/Nu dat file" << std::endl;
    std::cout << "program will exit to avoid incorrect info output" << std::endl;
    std::exit(1);
  }

  // Define detector geometry
  // Define TPC positions
  zPosFV_.resize(3);
  zPosFV_[0] = config::config_man().DetConfig().FV().zPosFV1;
  zPosFV_[1] = config::config_man().DetConfig().FV().zPosFV2;
  zPosFV_[2] = config::config_man().DetConfig().FV().zPosFV3;

  Double_t xPosND = config::config_man().DetConfig().Plane().xPosND;
  Double_t yPosND = config::config_man().DetConfig().Plane().yPosND;
  Double_t zPosND = config::config_man().DetConfig().Plane().zPosND;

  Double_t xSizeFV = config::config_man().DetConfig().FV().xSizeFV;
  Double_t ySizeFV = config::config_man().DetConfig().FV().ySizeFV;
  Double_t zSizeFV = config::config_man().DetConfig().FV().zSizeFV;

  for (Int_t i = 0; i < 3; ++i) {
    TVector3 DetPos(xPosND, yPosND, zPosND + zPosFV_[i]);
    utils::TBox3D_AAB Detector(DetPos, xSizeFV, ySizeFV, zSizeFV);
    tpc_.push_back(Detector);
  }

  // create output file name beginning
  OutFileStart_ = "oa_nt_otx_00000001-";

  std::stringstream stream;
  Int_t SubRun = ND::params().GetParameterI("nd280HNLSim.SubRun");
  stream << SubRun;
  for (UInt_t i = 0; i < 4 - stream.str().size(); ++i)
    OutFileStart_ += "0";

  OutFileStart_ += stream.str();
  OutFileStart_ += "_";

  int ran;
  for (Int_t i = 0; i < 4; ++i) {
    ran = round(gRandom->Rndm() * 25 + 97);
    OutFileStart_ += char(ran);
  }

  // Create histoes for intermediate results & checks

  // Check correlations histo
  CorrNZ = new TH2D("NZ", "", 300, 0., 500000., 300, -5000., 110000.);
  CorrNE = new TH2D("NE", "", 300, 0., 500000., 300, 0., 20.);

  pt_histo = new TH1D("pt", "", 200, 0., 800);

  // What reactions we want to study
  /// K modes
  // K->mu(mu+pi)
  kKMuMuPi = ND::params().GetParameterD("nd280HNLSim.Reaction.kKMuMuPi");
  // K->mu(e+pi)
  kKMuElePi = ND::params().GetParameterD("nd280HNLSim.Reaction.kKMuElePi");
  // K->mu(pi+mu)
  kKMuPiMu = ND::params().GetParameterD("nd280HNLSim.Reaction.kKMuPiMu");
  // K->mu(pi+e)
  kKMuPiEle = ND::params().GetParameterD("nd280HNLSim.Reaction.kKMuPiEle");

  // K->mu(mu+e+Nu(e))
  kKMuMuEleNue = ND::params().GetParameterD("nd280HNLSim.Reaction.kKMuMuEleNue");
  // K->mu(e+mu+Nu(e))
  kKMuEleMuNue = ND::params().GetParameterD("nd280HNLSim.Reaction.kKMuEleMuNue");
  // K->mu(mu+e+Nu(mu))
  kKMuMuEleNumu = ND::params().GetParameterD("nd280HNLSim.Reaction.kKMuMuEleNumu");
  // K->mu(e+mu+Nu(mu))
  kKMuEleMuNumu = ND::params().GetParameterD("nd280HNLSim.Reaction.kKMuEleMuNumu");

  // K->mu(e+e+Nu(e))
  kKMuEleEleNue = ND::params().GetParameterD("nd280HNLSim.Reaction.kKMuEleEleNue");
  // K->mu(e+e+Nu(mu,tau))
  kKMuEleEleNuMix = ND::params().GetParameterD("nd280HNLSim.Reaction.kKMuEleEleNuMix");
  // K->mu(mu+mu+Nu(mu))
  kKMuMuMuNumu = ND::params().GetParameterD("nd280HNLSim.Reaction.kKMuMuMuNumu");
  // K->mu(mu+mu+Nu(e,tau))
  kKMuMuMuNuMix = ND::params().GetParameterD("nd280HNLSim.Reaction.kKMuMuMuNuMix");

  // K->e(e+pi)
  kKEleElePi = ND::params().GetParameterD("nd280HNLSim.Reaction.kKEleElePi");
  // K->e(mu+pi)
  kKEleMuPi = ND::params().GetParameterD("nd280HNLSim.Reaction.kKEleMuPi");
  // K->e(pi+e)
  kKElePiEle = ND::params().GetParameterD("nd280HNLSim.Reaction.kKElePiEle");
  // K->e(pi+mu)
  kKElePiMu = ND::params().GetParameterD("nd280HNLSim.Reaction.kKElePiMu");

  // K->e(mu+e+Nu(e))
  kKEleMuEleNue = ND::params().GetParameterD("nd280HNLSim.Reaction.kKEleMuEleNue");
  // K->e(e+mu+Nu(e))
  kKEleEleMuNue = ND::params().GetParameterD("nd280HNLSim.Reaction.kKEleEleMuNue");
  // K->e(mu+e+Nu(mu))
  kKEleMuEleNumu = ND::params().GetParameterD("nd280HNLSim.Reaction.kKEleMuEleNumu");
  // K->e(e+mu+Nu(mu))
  kKEleEleMuNumu = ND::params().GetParameterD("nd280HNLSim.Reaction.kKEleEleMuNumu");

  // K->e(e+e+Nu(e))
  kKEleEleEleNue = ND::params().GetParameterD("nd280HNLSim.Reaction.kKEleEleEleNue");
  // K->e(e+e+Nu(mu,tau))
  kKEleEleEleNuMix = ND::params().GetParameterD("nd280HNLSim.Reaction.kKEleEleEleNuMix");
  // K->e(mu+mu+Nu(mu))
  kKEleMuMuNumu = ND::params().GetParameterD("nd280HNLSim.Reaction.kKEleMuMuNumu");
  // K->e(mu+mu+Nu(e,tau))
  kKEleMuMuNuMix = ND::params().GetParameterD("nd280HNLSim.Reaction.kKEleMuMuNuMix");

  // Pi modes
  // Pi->e(e+e+Nu(e))
  kPiEleEleEleNue = ND::params().GetParameterD("nd280HNLSim.Reaction.kPiEleEleEleNue");
  // Pi->e(e+e+Nu(mu,tau))
  kPiEleEleEleNuMix = ND::params().GetParameterD("nd280HNLSim.Reaction.kPiEleEleEleNuMix");
  // Pi->e(mu+e+Nu(e))
  kPiEleMuEleNue = ND::params().GetParameterD("nd280HNLSim.Reaction.kPiEleMuEleNue");
  // Pi->e(e+mu+Nu(e))
  kPiEleEleMuNue = ND::params().GetParameterD("nd280HNLSim.Reaction.kPiEleEleMuNue");
  // Pi->e(mu+e+Nu(mu))
  kPiEleMuEleNumu = ND::params().GetParameterD("nd280HNLSim.Reaction.kPiEleMuEleNumu");
  // Pi->e(e+mu+Nu(mu))
  kPiEleEleMuNumu = ND::params().GetParameterD("nd280HNLSim.Reaction.kPiEleEleMuNumu");

  // Pi->mu(e+e+Nu(e))
  kPiMuEleEleNue = ND::params().GetParameterD("nd280HNLSim.Reaction.kPiMuEleEleNue");
  // Pi->mu(e+e+Nu(mu,tau))
  kPiMuEleEleNuMix = ND::params().GetParameterD("nd280HNLSim.Reaction.kPiMuEleEleNuMix");

  numode_ = 0;

  AntiHNL_ = (bool)ND::params().GetParameterD("nd280HNLSim.AntiHNL");

  // Decay position histo
  for (Int_t i = 0; i < 2; ++i) {
    std::string title =  "N decay position in ZY plane from K#rightarrow eN. M_{N} = 0.43 GeV";
    DecayZY_[i] = new TH2D(Form("ZY%i", i),"", 200, -100., 300., 200, -111., 111.);
    DecayZY_[i]->SetTitle(title.c_str());
    DecayZY_[i]->GetXaxis()->SetTitle("Z");
    DecayZY_[i]->GetYaxis()->SetTitle("Y");

    title =  "N decay position in ZX plane from K#rightarrow eN. M_{N} = 0.43 GeV";
    DecayZX_[i] = new TH2D(Form("ZX%i", i),"", 200, -100., 300., 200, -111., 111.);
    DecayZX_[i]->SetTitle(title.c_str());
    DecayZX_[i]->GetXaxis()->SetTitle("Z");
    DecayZX_[i]->GetYaxis()->SetTitle("X");
  }
  ToF_ = new TH2D("Time of flight difference", "", 100, 0., 5., 100, 0., 150.);
  ToF_->SetTitle("Time of flight difference");
  ToF_->GetYaxis()->SetTitle("#Delta ToF, ns");
  ToF_->GetXaxis()->SetTitle("Momentum, GeV/c");

  Ntof_ = new TH2D("Time of flight", "", 100, 0.15, 0.5, 100, 0., 150.);
  Ntof_->SetTitle("Time of flight difference");
  Ntof_->GetXaxis()->SetTitle("M, GeV");
  Ntof_->GetYaxis()->SetTitle("#Delta ToF, ns");

  Br_ = new TGraph();

  FluxReweight_ = (bool)ND::params().GetParameterD("nd280HNLSim.FluxReweight");
  if (FluxReweight_) {
    RunConfNum_ = 13;

    if ((Int_t)ND::params().GetParameterD("nd280HNLSim.Mode") > 0) {
      FirstRunConf_ = 0;
      LastRunConf_  = 7;
    } else {
      FirstRunConf_ = 8;
      LastRunConf_  = 12;
    }

    // define TFile reading vector of TH1D FLuxWeight 0 - nom, n -  kaon for run N
    std::vector<TFile*> FluxTuningFile;

    Filename = fileName + "run1_tune_39bins.root";
    FluxTuningFile[0] = new TFile("", "READ");
    Filename = fileName + "run2_tune_39bins.root";
    FluxTuningFile[1] = new TFile("", "READ");
    Filename = fileName + "run2_tune_39bins.root";
    FluxTuningFile[2] = new TFile("", "READ");
    Filename = fileName + "run3b_tune_39bins.root";
    FluxTuningFile[3] = new TFile("", "READ");
    Filename = fileName + "run3c_tune_39bins.root";
    FluxTuningFile[4] = new TFile("", "READ");
    Filename = fileName + "run4_tune_39bins.root";
    FluxTuningFile[5] = new TFile("", "READ");
    Filename = fileName + "run4_tune_39bins.root";
    FluxTuningFile[6] = new TFile("", "READ");
    Filename = fileName + "run5_tune_39bins.root";
    FluxTuningFile[7] = new TFile("", "READ");
    Filename = fileName + "run5_tune_39bins.root";
    FluxTuningFile[8] = new TFile("", "READ");
    Filename = fileName + "run6b_tune_39bins.root";
    FluxTuningFile[9] = new TFile("", "READ");
    Filename = fileName + "run6c_tune_39bins.root";
    FluxTuningFile[10] = new TFile("", "READ");
    Filename = fileName + "run6d_tune_39bins.root";
    FluxTuningFile[11] = new TFile("", "READ");
    Filename = fileName + "run6e_tune_39bins.root";
    FluxTuningFile[12] = new TFile("", "READ");
    Filename = fileName + "run6_tune_39bins.root";
    FluxTuningFile[13] = new TFile("", "READ");

    for (Int_t i = 0; i < RunConfNum_; ++i) {
      if ((Int_t)ND::params().GetParameterD("nd280HNLSim.Mode") > 0) {
        FLuxWeight_[i].first  = (TH1D*)FluxTuningFile[i]->Get("nd5_nom_numu");
        FLuxWeight_[i].second = (TH1D*)FluxTuningFile[i]->Get("nd5_kaon_numu");
      } else {
        FLuxWeight_[i].first  = (TH1D*)FluxTuningFile[i]->Get("nd5_nom_numub");
        FLuxWeight_[i].second = (TH1D*)FluxTuningFile[i]->Get("nd5_kaon_numub");
      }
    }

    // Events number tuned for every run normolized for 1E21
    for (Int_t i = 0; i < RunConfNum_+1; ++i) {
      TunedFlux_[i] = new TH1D(Form("tunedFlux%i", i), "", 250, 0., 0.5);
      TunedFlux_[i]->SetTitle("tuned flux");
      TunedFlux_[i]->GetXaxis()->SetTitle("M, GeV");
      TunedFlux_[i]->GetYaxis()->SetTitle("N");
      TunedFlux_[i]->SetTitleFont(22,"xyz");
      TunedFlux_[i]->GetYaxis()->SetTitleOffset(0.9);
    }

    for (Int_t i = 0; i < RunConfNum_; ++i)
      FluxTuningFile[i]->Close();
  }
}

bool HeavyNu::TreeHistoCreate(std::string reaction) {

  Int_t index = Events_.size();

  Events_.resize(index + 1);

  std::string title = reaction;
  // Event number histoes
  Events_[index].histo = new TH1D(Form("%i",index), "", 250, 0., 0.5);
  Events_[index].histo->SetTitle(title.c_str());
  Events_[index].histo->GetXaxis()->SetTitle("M, GeV");
  Events_[index].histo->GetYaxis()->SetTitle("N");
  Events_[index].histo->SetTitleFont(22,"xyz");
  Events_[index].histo->GetYaxis()->SetTitleOffset(0.9);

  // Secondary particle characteristics
  DecFirstP_.resize(index+1);
  DecFirstCos_.resize(index+1);
  DecSecondP_.resize(index+1);
  DecSecondCos_.resize(index+1);
  DecRelativeCos_.resize(index+1);
  tofMass.resize(index+1);


  DecFirstP_[index].resize(gSizeMu_);
  DecFirstCos_[index].resize(gSizeMu_);
  DecSecondP_[index].resize(gSizeMu_);
  DecSecondCos_[index].resize(gSizeMu_);
  DecRelativeCos_[index].resize(gSizeMu_);
  tofMass[index].resize(gSizeMu_);

  for (Int_t j = 0; j < gSizeMu_; ++j) {
    title = "#splitline{First product momentum}{" + reaction + "}";
    DecFirstP_[index][j] = new TH1D(Form("FP%i %i", index, j),"", 200, 0., 10);
    DecFirstP_[index][j]->SetTitle(title.c_str());
    DecFirstP_[index][j]->GetXaxis()->SetTitle("P, GeV/c");
    DecFirstP_[index][j]->GetYaxis()->SetTitle("N");
    DecFirstP_[index][j]->SetTitleFont(22,"xyz");
    DecFirstP_[index][j]->SetTitleSize(0.05,"xyz");
    DecFirstP_[index][j]->GetYaxis()->SetTitleOffset(0.9);

    title = "#splitline{Second product momentum}{" + reaction + "}";
    DecSecondP_[index][j] = new TH1D(Form("SP%i %i", index, j),"", 200, 0., 10);
    DecSecondP_[index][j]->SetTitle(title.c_str());
    DecSecondP_[index][j]->GetXaxis()->SetTitle("P, GeV/c");
    DecSecondP_[index][j]->GetYaxis()->SetTitle("N");
    DecSecondP_[index][j]->SetTitleFont(22,"xyz");
    DecSecondP_[index][j]->SetTitleSize(0.05,"xyz");
    DecSecondP_[index][j]->GetYaxis()->SetTitleOffset(0.9);
    title = "#splitline{First product cos(#theta)}{" + reaction + "}";
    DecFirstCos_[index][j] = new TH1D(Form("FC%i %i", index, j),"", 200, -1.1, 1.1);
    DecFirstCos_[index][j]->SetTitle(title.c_str());
    DecFirstCos_[index][j]->GetXaxis()->SetTitle("cos(#theta)");
    DecFirstCos_[index][j]->GetYaxis()->SetTitle("N");
    DecFirstCos_[index][j]->SetTitleFont(22,"xyz");
    DecFirstCos_[index][j]->SetTitleSize(0.05,"xyz");
    DecFirstCos_[index][j]->GetYaxis()->SetTitleOffset(0.9);

    title = "#splitline{Second product cos(#theta)}{" + reaction + "}";
    DecSecondCos_[index][j] = new TH1D(Form("SC%i %i", index, j),"", 200, -1.1, 1.1);
    DecSecondCos_[index][j]->SetTitle(title.c_str());
    DecSecondCos_[index][j]->GetXaxis()->SetTitle("cos(#theta)"); DecSecondCos_[index][j]->GetYaxis()->SetTitle("N");
    DecSecondCos_[index][j]->SetTitleFont(22,"xyz");
    DecSecondCos_[index][j]->SetTitleSize(0.05,"xyz");
    DecSecondCos_[index][j]->GetYaxis()->SetTitleOffset(0.9);

    title = "#splitline{Relative angle cos}{" + reaction + "}";
    DecRelativeCos_[index][j] = new TH1D(Form("R%i %i", index, j),"", 200, -1.1, 1.1);
    DecRelativeCos_[index][j]->SetTitle(title.c_str());
    DecRelativeCos_[index][j]->GetXaxis()->SetTitle("cos(#theta)");
    DecRelativeCos_[index][j]->GetYaxis()->SetTitle("N");
    DecRelativeCos_[index][j]->SetTitleFont(22,"xyz");
    DecRelativeCos_[index][j]->SetTitleSize(0.05,"xyz");
    DecRelativeCos_[index][j]->GetYaxis()->SetTitleOffset(0.9);

    title = reaction;
    tofMass[index][j] = new TH1D(Form("tof%i %i", index, j),"", 200, 0., 200.);
    tofMass[index][j]->SetTitle("");
    tofMass[index][j]->GetXaxis()->SetTitle("#Delta ToF, ns");
    tofMass[index][j]->GetYaxis()->SetTitle("N");
    tofMass[index][j]->SetTitleFont(22,"xyz");
    tofMass[index][j]->SetTitleSize(0.05,"xyz");
    tofMass[index][j]->GetYaxis()->SetTitleOffset(0.9);
  }

  return true;
}

//-----------------------------------------------------------------------
void HeavyNu::Process() {
  //-----------------------------------------------------------------------
  // flux file name
  std::string GenRoot;

  gen::fluxGen flux;

  // generate flux files
  if (regenerateFluxHnu_) {

    // create new mass array according to parameters file
    gSizeMu_ = ND::params().GetParameterD("nd280HNLSim.MassArraySize");
    Double_t MinMass = ND::params().GetParameterD("nd280HNLSim.MinMass");
    Double_t MaxMass = ND::params().GetParameterD("nd280HNLSim.MaxMass");

    if (MinMass == -1.)
      MinMass = units::mass_electron + units::mass_electron;
    if (MaxMass == -1.)
      MaxMass = units::mass_charged_kaon - units::mass_electron;

    // mass array
    gHnuMass_.resize(gSizeMu_, 0);
    Double_t step;
    if (gSizeMu_ == 1)
      step = 0;
    else
      step = (MaxMass - MinMass) / (gSizeMu_ - 1);

    for (Int_t i = 0; i < gSizeMu_; ++i)
      gHnuMass_[i] = MinMass + step * i;

    flux.Initialize(inputListFiles_, &gHnuMass_);
    gHnuMass_.clear();

    std::cout << std::endl << "Generating Hnu flux from " << inputListFiles_ << std::endl;

    GenRoot = outputFileDir_ + "HeavyNuFlux";
    // generate flux
    // set hnl production mode
    hnlProdMode::hnlProdEnum m;
    std::stringstream stream;
    bool ok;

    if (kKMuMuPi || kKMuElePi || kKMuPiMu || kKMuPiEle ||
    kKMuMuEleNumu || kKMuMuEleNue || kKMuEleMuNue || kKMuEleMuNumu ||
    kKMuEleEleNue || kKMuEleEleNuMix || kKMuMuMuNumu || kKMuMuMuNuMix) {
      m = hnlProdMode::kKmu2;
      stream << m;
      GenRoot += stream.str() + ".root";
      // generate the flux
      ok = flux.Generate(m, GenRoot);

      if (ok) {
        std::cout << "Flux was generated" << std::endl;
        FluxFileName_.push_back(GenRoot);
      } else {
        std::cout << "Error in 'HeavyNu::Process'. Flux was not generated for mode " << m << std::endl;
      }
    }

    if (kKEleElePi || kKEleMuPi || kKElePiEle || kKElePiMu ||
    kKEleMuEleNue || kKEleEleMuNue || kKEleMuEleNumu || kKEleEleMuNumu ||
    kKEleEleEleNue || kKEleEleEleNuMix || kKEleMuMuNumu || kKEleMuMuNuMix) {
      m = hnlProdMode::kKe2;
      stream.str("");
      stream << m;
      GenRoot = outputFileDir_ + "HeavyNuFlux";
      GenRoot += stream.str() + ".root";
      ok = flux.Generate(m, GenRoot);

      if (ok) {
        std::cout << "Flux was generated" << std::endl;
        FluxFileName_.push_back(GenRoot);
      } else {
        std::cout << "Error in 'HeavyNu::Process'. Flux was not generated for mode " << m << std::endl;
      }
    }

    // Pi -> mu + N
    if (kPiMuEleEleNue || kPiMuEleEleNuMix) {
      m = hnlProdMode::kPimu2;
      stream.str("");
      stream << m;
      GenRoot = outputFileDir_ + "HeavyNuFlux";
      GenRoot += stream.str() + ".root";
      ok = flux.Generate(m, GenRoot);

      if (ok) {
        std::cout << "Flux was generated" << std::endl;
        FluxFileName_.push_back(GenRoot);
      } else {
        std::cout << "Error in 'HeavyNu::Process'. Flux was not generated for mode " << m << std::endl;
      }
    }

    // Pi -> e + N
    if (kPiEleMuEleNue || kPiEleEleMuNue || kPiEleMuEleNumu || kPiEleEleMuNumu || kPiEleEleEleNue || kPiEleEleEleNuMix ) {
      m = hnlProdMode::kPiE2;
      stream.str("");
      stream << m;
      GenRoot = outputFileDir_ + "HeavyNuFlux";
      GenRoot += stream.str() + ".root";
      ok = flux.Generate(m, GenRoot);

      if (ok) {
        std::cout << "Flux was generated" << std::endl;
        FluxFileName_.push_back(GenRoot);
      } else {
        std::cout << "Error in 'HeavyNu::Process'. Flux was not generated for mode " << m << std::endl;
      }
    }
  }

  std::cout << std::endl << "Generating Hnu decays. If there are any errors with reading trees - regenerate the flux with flag -n" << std::endl;

  // open files with flux trees
  for (unsigned int i = 0; i < FluxFileName_.size(); ++i) {
    GenRoot = FluxFileName_[i];

    TFile* FluxFile = new TFile(GenRoot.c_str(), "READ");
    if (!FluxFile->IsOpen()) {
      std::cout << "Error in 'HeavyNu::Process'. File with heavy nu flux trees was not found" << std::endl;
      std::cout << "Skipping file " << FluxFileName_[i] << std::endl;
      continue;
    }
    std::cout << "File with heavy nu flux trees is  " << FluxFile->GetName() << std::endl;

    FluxFile->cd();

    // Read header tree
    TTree* FluxHeader_ = (TTree*)FluxFile->Get("header");

    Double_t MinMass, MaxMass;
    Int_t mode_number;
    Int_t numode = 0;
    FluxHeader_->SetBranchAddress("mode",        &mode_number);
    FluxHeader_->SetBranchAddress("minmass",     &MinMass);
    FluxHeader_->SetBranchAddress("maxmass",     &MaxMass);
    FluxHeader_->SetBranchAddress("size",        &gSizeMu_);
    FluxHeader_->SetBranchAddress("numode",      &numode);

    FluxHeader_->GetEntry(0);

    // check that all input files are for decaying similar meson K+ or K-
    if (Events_.size() != 0) {
      if (numode_ != numode) {
        std::cout << "You are trying to analyze FHC and RHC modes together. It's prohibited!" << std::endl <<
        "We will continue with mode " << numode_ << ". File with another mode will be skipped. File is " << FluxFileName_[i] << std::endl;
      }
    } else numode_ = numode;

    /*if (rdpPOT_ && MesonCharge_ == -1) {
      std::cout << "WARNING in HeavyNu::Process. RDP POT scaling is available only for neutrino mode, not nu-bar" << std::endl;
      std::cout << "program will exit to avoid incorrect info output" << std::endl;
      std::exit(1);
    }*/

    // Fill the mass array from header file
    Double_t step;
    if (gSizeMu_ == 1)
      step = 0;
    else
      step = (MaxMass - MinMass) / (gSizeMu_ - 1);
    gHnuMass_.resize(gSizeMu_, 0);

    for (Int_t j = 0; j < gSizeMu_; ++j) {
      gHnuMass_[j] = MinMass + step * j;
    }

    // if generate flux for one mass
    Int_t GenSize = ND::params().GetParameterD("nd280HNLSim.MassArraySize");
    Double_t GenMinMass = ND::params().GetParameterD("nd280HNLSim.MinMass");

    ValidMassiveIndex_ = -1;
    if (GenSize == 1) {
      for (Int_t i = 0; i < gSizeMu_; ++i)
        if (gHnuMass_[i] == GenMinMass) {
          ValidMassiveIndex_ = i;
          break;
        }
      if (ValidMassiveIndex_ == -1) {
        std::cout << "Error in HeavyNu::Process. Hnu flux file doesn't contain mass set in the params file for one mass generation" << std::endl;
        std::exit(1);
      } else std::cout << "One mass generation successful mass search. Mhnl = " << gHnuMass_[ValidMassiveIndex_] << std::endl;
    }

    // Read the flux tree
    TTree* HnuFlux = (TTree*)FluxFile->Get("hnuflux");

    HnuFlux->SetBranchAddress("xpi",        xpi_);
    HnuFlux->SetBranchAddress("npi",        npi_);
    HnuFlux->SetBranchAddress("norm",       &norm_);
    HnuFlux->SetBranchAddress("Enu",        &Enu_);
    HnuFlux->SetBranchAddress("Enu_orig",   &Enu_orig_);
    HnuFlux->SetBranchAddress("xnu",        &xnu_);
    HnuFlux->SetBranchAddress("ynu",        &ynu_);
    HnuFlux->SetBranchAddress("massI",      &mass_index_);
    HnuFlux->SetBranchAddress("helicity",   &helicity_);
    HnuFlux->SetBranchAddress("charge_par", &MesonCharge_);

    // what reactions we want to study
    if (mode_number == hnlProdMode::kKmu2) {
      hnlProdMode::hnlProdEnum prod = utils::GetProdEnum(mode_number);

      if (kKMuMuPi)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kMumPip);
      if (kKMuElePi)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kElemPip);
      if (kKMuPiMu)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kPimMup);
      if (kKMuPiEle)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kPimElep);

      if (kKMuMuEleNue)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kMumElepNue);
      if (kKMuEleMuNue)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kElemMupNue);
      if (kKMuMuEleNumu)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kMumElepNumu);
      if (kKMuEleMuNumu)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kElemMupNumu);

      if (kKMuEleEleNue)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kElemElepNue);
      if (kKMuEleEleNuMix)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kElemElepNuMix);
      if (kKMuMuMuNumu)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kMumMupNumu);
      if (kKMuMuMuNuMix)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kMumMupNuMix);
    }

    if (mode_number == hnlProdMode::kKe2) {
      hnlProdMode::hnlProdEnum prod = utils::GetProdEnum(mode_number);

      if (kKEleElePi)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kElemPip);
      if (kKEleMuPi)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kMumPip);
      if (kKElePiEle)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kPimElep);
      if (kKElePiMu)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kPimMup);

      if (kKEleMuEleNue)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kMumElepNue);
      if (kKEleEleMuNue)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kElemMupNue);
      if (kKEleMuEleNumu)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kMumElepNumu);
      if (kKEleEleMuNumu)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kElemMupNumu);

      if (kKEleEleEleNue)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kElemElepNue);
      if (kKEleEleEleNuMix)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kElemElepNuMix);
      if (kKEleMuMuNuMix)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kMumMupNuMix);
      if (kKEleMuMuNumu)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kMumMupNumu);

    }

    if (mode_number == hnlProdMode::kPimu2) {
      hnlProdMode::hnlProdEnum prod = utils::GetProdEnum(mode_number);
      if (kPiMuEleEleNue)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kElemElepNue);
      if (kPiMuEleEleNuMix)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kElemElepNuMix);

    }

    if (mode_number == hnlProdMode::kPiE2) {
      hnlProdMode::hnlProdEnum prod = utils::GetProdEnum(mode_number);
      if (kPiEleEleEleNue)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kElemElepNue);
      if (kPiEleEleEleNuMix)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kElemElepNuMix);
      if (kPiEleMuEleNue)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kMumElepNue);
      if (kPiEleEleMuNue)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kElemMupNue);
      if (kPiEleMuEleNumu)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kMumElepNumu);
      if (kPiEleEleMuNumu)
        HnuDecay(HnuFlux, prod, hnlDecayMode::kElemMupNumu);

    }
    delete FluxHeader_;
  }
}

//-----------------------------------------------------------------------
void HeavyNu::Finalize() {
  //-----------------------------------------------------------------------
  if (doSecParticlePlot_)
    PlotSecondaryParticles();
  if (doFluxPlot_)
    PlotHnuFluxData();
  if (doEventsNumberPlot_)
    PlotEventsNumber();
  if (doControlScript_)
    GenerateControlScript();
}

//-----------------------------------------------------------------------
void HeavyNu::HnuDecay(TTree*& FluxTree, hnlProdMode::hnlProdEnum& prod, hnlDecayMode::hnlDecayEnum dec){
  //-----------------------------------------------------------------------
  hnlDecayMode DecayMode(dec);

  if (DecayMode.GetDecayMode() == hnlDecayMode::kInvalid) {
    std::cout << "Unknown HNL decay mode " << std::endl << "Skipping this mode" << std::endl;
    return;
  }

  hnlProdMode ProdMode(prod);
  if (ProdMode.GetProdMode() == hnlProdMode::kInvalid) {
    std::cout << "Unknown HNL production mode " << std::endl << "Skipping this mode" << std::endl;
    return;
  }

  std::string reaction = ProdMode.GetReaction() + ", " + DecayMode.GetReaction();
  // create histoes for this mode
  if (!TreeHistoCreate(reaction)) {
    std::cerr << "Error in 'HeavyNu::HnuDecay'. Error while creating results trees & histies. Skipping this mode" << std::endl;
    return;
  }

  Events_[Events_.size()-1].ProdMode  = ProdMode.GetProdMode();
  Events_[Events_.size()-1].DecayMode = DecayMode.GetDecayMode();

  // create file for secondary particles tree
  std::string FileName;
  FileName = OutFileStart_;

  // gen random name
  int ran;
  for (Int_t i = 0; i < 8; ++i) {
    ran = round(gRandom->Rndm() * 25 + 97);
    FileName += char(ran);
  }

  FileName += "_hnlm_000.root";
  FileName = outputFileDir_ + FileName;
  output_ = FileName;

  auto* OutputFile =  new TFile(FileName.c_str(), "RECREATE");
  if (!OutputFile->IsOpen()) {
    std::cout << "Can't create output file for reaction" << DecayMode.GetReaction() << ". Skip this mode" << std::endl;
    return;
  }

  std::cout << "Generating mode " << reaction << std::endl;

  // Get secondary particles mass
  std::vector<Double_t> Prod_mass;
  Prod_mass = DecayMode.GetProdMass();

  // Set kinematics parameters
  std::vector<long double> DecayWidth;
  DecayWidth = DecayMode.GetWidth(gHnuMass_);

  for (Int_t i = 0; gHnuMass_[i] < 0.387; ++i) {
    Br_->SetPoint(i, gHnuMass_[i], utils::WidthMesonToHNuLepton(units::mass_charged_kaon, units::f_kaon, units::Vus, units::mass_muon, gHnuMass_[i])  *units::t_charged_kaon);
  }

  Long64_t Entries = FluxTree->GetEntries();

  fEvtNum_ = 0;

  OutputFile->cd();
  // create tree for secondary particles
  auto ProductTree = new TTree("nRooTracker","");
  fEvtFlags_ = nullptr;
  fEvtCode_ = nullptr;

  ProductTree->Branch("EvtFlags",       &fEvtFlags_);
  ProductTree->Branch("EvtCode",        &fEvtCode_);
  ProductTree->Branch("EvtNum",         &fEvtNum_,      "fEvtNum_/I");
  ProductTree->Branch("EvtXSec",        &fEvtXSec_);
  ProductTree->Branch("EvtDXSec",       &fEvtDXSec_);
  ProductTree->Branch("EvtWght",        &fEvtWght_);
  ProductTree->Branch("EvtProb",        &fEvtProb_);
  ProductTree->Branch("EvtVtx",          fEvtVtx_,      "fEvtVtx_[4]/D");
  ProductTree->Branch("StdHepN",        &fStdHepN_,     "fStdHepN_/I");
  ProductTree->Branch("StdHepPdg",       fStdHepPdg_,     "fStdHepPdg_[4]/I");
  ProductTree->Branch("StdHepStatus",    fStdHepStatus_,    "fStdHepStatus_[4]/I");
  ProductTree->Branch("StdHepX4",        fStdHepX4_,      "fStdHepX4_[4][4]/D");
  ProductTree->Branch("StdHepP4",        fStdHepP4_,      "fStdHepP4_[4][4]/D");
  ProductTree->Branch("StdHepPolz",      fStdHepPolz_,    "fStdHepPolz_[4][3]/D");
  ProductTree->Branch("StdHepFd",        fStdHepFd_,      "fStdHepFd_[4]/I");
  ProductTree->Branch("StdHepLd",        fStdHepLd_,      "fStdHepLd_[4]/I");
  ProductTree->Branch("StdHepFm",        fStdHepFm_,      "fStdHepFm_[4]/I");
  ProductTree->Branch("StdHepLm",        fStdHepLm_,      "fStdHepLm_[4]/I");
  ProductTree->Branch("NuParentPdg",    &fNuParentPdg_);
  ProductTree->Branch("NuParentDecMode",&fNuParentDecMode_);
  ProductTree->Branch("NuParentDecP4",   fNuParentDecP4_,   "fNuParentDecP4_[4]/D");
  ProductTree->Branch("NuParentDecX4",   fNuParentDecX4_,   "fNuParentDecX4_[4]/D");
  ProductTree->Branch("NuParentProP4",   fNuParentProP4_,   "fNuParentProP4_[4]/D");
  ProductTree->Branch("NuParentProX4",   fNuParentProX4_,   "fNuParentProX4_[4]/D");
  ProductTree->Branch("NuParentProNVtx",&fNuParentProNVtx_);

  Int_t CheckProgress = round(Entries / 5);

  Int_t index = Events_.size() - 1;

  for(Long64_t i = 0; i < Entries; ++i) {
    FluxTree->GetEntry(i);

    if (i % CheckProgress == 0){
      Double_t ratio = (double)i/Entries;
      std::cout << "Entry: " << i << " of " << Entries << " (" << int(100*ratio + 0.5) << "%)" << std::endl;
    }

    if (gHnuMass_[mass_index_] <= Prod_mass[0] + Prod_mass[1])
      continue;

    if (ValidMassiveIndex_ != -1)
      if (mass_index_ != ValidMassiveIndex_)
        continue;

    Double_t xPosND = config::config_man().DetConfig().Plane().xPosND;
    Double_t yPosND = config::config_man().DetConfig().Plane().yPosND;
    Double_t zPosND = config::config_man().DetConfig().Plane().zPosND;
    Double_t zSizeFV = config::config_man().DetConfig().FV().zSizeFV;
    TVector3 posDet(xnu_ + xPosND, ynu_ + yPosND, zPosND - zPosFV_[0] - zSizeFV);

    // Decay position of neutrino parent
    TVector3 posDecay(xpi_[0], xpi_[1], xpi_[2]);

    // Hnu decay position & probability of event
    TVector3 hnlDecayPoint;
    long double EventProb = EventProbability(posDet, posDecay, mass_index_, Enu_, hnlDecayPoint);

    if (EventProb == 0.)
      continue;

#ifdef DEBUG
  std::cout << gHnuMass_[mass_index_] << "     ";
  std::cout << EventProb << "     ";
#endif

    // ToF calculation
    Double_t p = sqrt(Enu_ * Enu_ - gHnuMass_[mass_index_] * gHnuMass_[mass_index_]);
    Double_t beta = p / Enu_;
    TVector3 HnuPath = posDet - posDecay;
    //std::cout << gHnuMass_[mass_index_] << "      " << Enu_ << "     " << beta << std::endl;
    //std::cout << HnuPath.Mag()/units::clight << "     " << HnuPath.Mag()/(beta*units::clight) << std::endl;
    Double_t dTOF = 10*HnuPath.Mag() * (1-beta)*1E9;
    dTOF /= beta*units::clight;

    // Due to redirecting heavy neutrino we need to recalculate number of events for new square
    EventProb *= norm_ * 4 * config::config_man().DetConfig().Plane().xSizeND * config::config_man().DetConfig().Plane().ySizeND;
    Double_t ProdProb = norm_ * 4 * config::config_man().DetConfig().Plane().xSizeND * config::config_man().DetConfig().Plane().ySizeND;

    // decay width for current mass
    EventProb *= DecayWidth[mass_index_];

#ifdef DEBUG
  std::cout << ProdProb << "     ";
  std::cout <<  DecayWidth[mass_index_] << "     ";
#endif

    if (FluxReweight_) {
      for (Int_t i = 0; i < RunConfNum_; ++i) {
        Double_t WeightKaonFlux = FLuxWeight_[i].second->GetBinContent(FLuxWeight_[i].second->FindBin(Enu_orig_));
        Double_t WeightNomFlux  = FLuxWeight_[i].first->GetBinContent(FLuxWeight_[i].first->FindBin(Enu_orig_));
        TunedFlux_[i]->Fill(gHnuMass_[mass_index_], EventProb * POTscale_ * WeightKaonFlux / WeightNomFlux);
      }
    }

    // scale on needed POT number
    if (rdpPOT_) {
      Double_t AnuNuRatio = _anuNorm->Eval(Enu_orig_, 0, "");
      EventProb *= rdpNuPOT_ + AnuNuRatio * rdpNuBarPOT_;
    } else EventProb *= POTscale_;

    TVector3 mom = posDet - posDecay;
    mom = mom.Unit();

    // compute polarization
    TLorentzVector P_K, P_N;
    P_K.SetXYZM(npi_[0], npi_[1], npi_[2], ProdMode.GetParentMass());
    P_N.SetVectM(sqrt(Enu_ * Enu_ - gHnuMass_[mass_index_] * gHnuMass_[mass_index_])*mom, gHnuMass_[mass_index_]);

    TVector3 boost = P_N.BoostVector();
    P_K.Boost(-boost);
    TVector3 dirK = P_K.Vect().Unit();

    TVector3 spin = - helicity_ * dirK;

    /// Calculate secondary particles kinematics
    DecGen::THNLDecayGen hnlDecayKin(gHnuMass_[mass_index_], Prod_mass, DecayMode.GetDecayMode());
    Double_t prob = hnlDecayKin.Generate(Enu_, posDet - posDecay, spin);

    // fill "number of events" histo
    Events_[index].histo->Fill(gHnuMass_[mass_index_], EventProb);
    ToF_->Fill(p, dTOF, EventProb);
    Ntof_->Fill(gHnuMass_[mass_index_], dTOF, EventProb);

    if ( prob == 0 )
      continue;

    // take in account differential width
    EventProb *= prob;

    std::vector<TLorentzVector> Lorentz;
    Lorentz.resize(2);
    Lorentz[0] = hnlDecayKin.GetProducts()[0];
    Lorentz[1] = hnlDecayKin.GetProducts()[1];

    if (round(100*gHnuMass_[mass_index_]) == 43) {
      DecayZX_[0]->Fill(hnlDecayPoint.Z(), hnlDecayPoint.X());
      DecayZY_[0]->Fill(hnlDecayPoint.Z(), hnlDecayPoint.Y());
    }
    if (round(100*gHnuMass_[mass_index_]) == 46) {
      DecayZX_[1]->Fill(hnlDecayPoint.Z(), hnlDecayPoint.X());
      DecayZY_[1]->Fill(hnlDecayPoint.Z(), hnlDecayPoint.Y());
    }

    // fill secondary particles characteristics histoes if we need
    if (doSecParticlePlot_) {
      DecFirstP_[index][mass_index_]->Fill(Lorentz[0].P(), EventProb);
      DecSecondP_[index][mass_index_]->Fill(Lorentz[1].P(), EventProb);

      // 3-vectors of momentum for angle calculation
      TVector3 first;
      first.SetXYZ(Lorentz[0].X(), Lorentz[0].Y(), Lorentz[0].Z());
      TVector3 second;
      second.SetXYZ(Lorentz[1].X(), Lorentz[1].Y(), Lorentz[1].Z());

      DecFirstCos_[index][mass_index_]->Fill( first.CosTheta(), EventProb );
      DecSecondCos_[index][mass_index_]->Fill( second.CosTheta(), EventProb );
      DecRelativeCos_[index][mass_index_]->Fill( cos( first.Angle(second) ), EventProb);
      tofMass[index][mass_index_]->Fill(dTOF, EventProb);
    }

    /// write data to tree. For variables information see header file
    fEvtVtx_[0] = hnlDecayPoint.X() / 100.;
    fEvtVtx_[1] = hnlDecayPoint.Y() / 100.;
    fEvtVtx_[2] = hnlDecayPoint.Z() / 100.;
    fEvtVtx_[3] = 0.;

    fStdHepN_ = 4;

    fStdHepPdg_[0] = 14;
    fStdHepPdg_[1] = 1000180400;
    if (AntiHNL_) {
      fStdHepPdg_[2] = (-1)*DecayMode.GetPDGCode()[0];
      fStdHepPdg_[3] = (-1)*DecayMode.GetPDGCode()[1];
    } else {
      fStdHepPdg_[2] = DecayMode.GetPDGCode()[0];
      fStdHepPdg_[3] = DecayMode.GetPDGCode()[1];
    }


    fStdHepStatus_[0] = 0;
    fStdHepStatus_[1] = 0;
    fStdHepStatus_[2] = 1;
    fStdHepStatus_[3] = 1;

    fStdHepP4_[0][0] = Enu_*mom.X();
    fStdHepP4_[0][1] = Enu_*mom.Y();
    fStdHepP4_[0][2] = Enu_*mom.Z();
    fStdHepP4_[0][3] = Enu_;

    fStdHepP4_[1][0] = 0;
    fStdHepP4_[1][1] = 0;
    fStdHepP4_[1][2] = 0;
    fStdHepP4_[1][3] = 40;

    // HNL parent particle information
    fNuParentDecP4_[0] = npi_[0];
    fNuParentDecP4_[1] = npi_[1];
    fNuParentDecP4_[2] = npi_[2];
    fNuParentDecP4_[3] = sqrt(npi_[0]*npi_[0] + npi_[1]*npi_[1] + npi_[2]*npi_[2] + units::mass_charged_kaon* units::mass_charged_kaon);

    // HNL daughter particle information
    fStdHepP4_[2][0] = Lorentz[0].X();
    fStdHepP4_[2][1] = Lorentz[0].Y();
    fStdHepP4_[2][2] = Lorentz[0].Z();
    fStdHepP4_[2][3] = Lorentz[0].E();

    fStdHepP4_[3][0] = Lorentz[1].X();
    fStdHepP4_[3][1] = Lorentz[1].Y();
    fStdHepP4_[3][2] = Lorentz[1].Z();
    fStdHepP4_[3][3] = Lorentz[1].E();

    TVector3 vect1 = Lorentz[0].Vect();
    TVector3 vect2 = Lorentz[1].Vect();
    vect1 *= -1.;
    vect1.SetZ(0.);
    vect2.SetZ(0.);
    TVector3 pt = vect2 - vect1;
    pt_histo->Fill(pt.Mag()*1000, EventProb);

    fNuParentPdg_ = ProdMode.GetParentCode() * MesonCharge_;

    fEvtWght_ = EventProb;

    fEvtProb_ = ProdProb;

    // store mass for pass through info
    fEvtXSec_ = gHnuMass_[mass_index_] * 1E36;

    // store original ordanary neutrino energy for systematics analysis
    fEvtDXSec_ = Enu_orig_;

    fNuParentDecX4_[0] = xpi_[0];
    fNuParentDecX4_[1] = xpi_[1];
    fNuParentDecX4_[2] = xpi_[2];
    fNuParentDecX4_[3] = 0.;

    /// Fill the tree
    ProductTree->Fill();
    ++fEvtNum_;
  }

  ProductTree->Write("", TObject::kOverwrite);
  pt_histo->Write("", TObject::kOverwrite);
  std::cout << "Reaction " << reaction << " was written into file " << OutputFile->GetName() << std::endl;

  OutputFile->Close();
}

//-----------------------------------------------------------------------
Double_t HeavyNu::EventProbability(const TVector3& posDet, const TVector3& posDecay, const Int_t& mass_index, const Double_t& Enu, TVector3& hnlDecayPoint) {
  //-----------------------------------------------------------------------
  Double_t factor = 1.;

  if (Enu == 0.)
    return 0.;

  Double_t p = sqrt(Enu * Enu - gHnuMass_[mass_index] * gHnuMass_[mass_index]);
  Double_t beta_gamma = p / gHnuMass_[mass_index];

  if (p != p)
    return 0.;

  /// Geometry
  // Hnu momentum
  utils::TRay NuMom(posDecay, posDet - posDecay);

  // Detector simple geometry with 3 TPC
  TVector3 DetPos;

  Double_t xPosND = config::config_man().DetConfig().Plane().xPosND;
  Double_t yPosND = config::config_man().DetConfig().Plane().yPosND;
  Double_t zPosND = config::config_man().DetConfig().Plane().zPosND;

  Double_t xSizeFV = config::config_man().DetConfig().FV().xSizeFV;
  Double_t ySizeFV = config::config_man().DetConfig().FV().ySizeFV;
  Double_t zSizeFV = config::config_man().DetConfig().FV().zSizeFV;

  // TPC 1
  DetPos.SetXYZ(xPosND, yPosND,zPosND + zPosFV_[0]);
  utils::TBox3D_AAB TPC1(DetPos, xSizeFV, ySizeFV, zSizeFV);
  // TPC 2
  DetPos.SetXYZ(xPosND, yPosND, zPosND + zPosFV_[1]);
  utils::TBox3D_AAB TPC2(DetPos, xSizeFV, ySizeFV, zSizeFV);
  //TPC3
  DetPos.SetXYZ(xPosND, yPosND, zPosND + zPosFV_[2]);
  utils::TBox3D_AAB TPC3(DetPos, xSizeFV, ySizeFV, zSizeFV);

  Double_t tmin1, tmax1, tmin2, tmax2, tmin3, tmax3, dt;

  // find enter/exit points of Heavy nu
  // check what detectors were crossed
  bool inside1, inside2, inside3;
  inside1 = inside2 = inside3 = false;

  inside1 = TPC1.Intersect(NuMom, tmin1, tmax1);
  inside2 = TPC2.Intersect(NuMom, tmin2, tmax2);
  inside3 = TPC3.Intersect(NuMom, tmin3, tmax3);

  //no interaction
  if (!inside1 && !inside2 && !inside3)
    return 0.;

  // look what TPCs were crossed
  if (inside1 && inside2 && inside3) {
    // cross all 3 TPC
    dt = tmax3 + tmax2 + tmax1 - tmin3 - tmin2 - tmin1;
  } else
  if (!inside1 && !inside2) {
    //cross only 3rd TPC
    dt = tmax3 - tmin3;
  } else
  if (!inside3 && inside1 && inside2) {
    //cross 1 & 2 TPC
    dt = tmax2 + tmax1 - tmin2 - tmin1;
  } else
  if (!inside2 && !inside3 && inside1) {
    // cross only 1st TPC
    dt = tmax1 - tmin1;
  } else {
    // cross 2 & 3 TPC
    dt = tmax3 + tmax2 - tmin3 - tmin2;
  }

  // Length in detector
  auto L = dt * NuMom.GetDir().Mag();

  // Decay position
  Double_t x, y, z, tDecay;
  tDecay = gRandom->Rndm() * dt;

  if (tDecay < tmax1 - tmin1) {
    // decay in 1st TPC
    tDecay += tmin1;
  } else
  if (tDecay < tmax2 - tmin2 + tmax1 - tmin1) {
    // decay in 2nd TPC
    tDecay += tmin2 - tmax1 + tmin1;
  } else {
    // decay in 3rd TPC
    tDecay += tmin3 - tmax2 + tmin2 - tmax1 + tmin1;
  }

  x = NuMom.GetPos().X() + NuMom.GetDir().X() * tDecay - xPosND;
  y = NuMom.GetPos().Y() + NuMom.GetDir().Y() * tDecay - yPosND;
  z = NuMom.GetPos().Z() + NuMom.GetDir().Z() * tDecay - zPosND;

  hnlDecayPoint.SetXYZ(x, y, z);
  TVector3 DetDist = posDet - posDecay;

  factor *= L / (beta_gamma * units::clight / units::cm);

  //std::cout << DetDist.Mag() << "    " << L << std::endl;

  //Double_t tau = 10E-6;
  //factor *= exp(-DetDist.Mag() / (tau * beta_gamma * units::clight / units::cm));

  //factor *= exp(-DetDist.Mag() / (tau * beta_gamma * units::clight / units::cm)) - exp(-(DetDist.Mag() + L)/ (tau * beta_gamma * units::clight / units::cm));

  // Error check
  if (factor != factor) {
    std::cerr << "Error in 'HeavyNu::EventProbability'. Decay probability is NaN" << std::endl;
    return 0.;
  }

  return factor;
}

//-----------------------------------------------------------------------
void HeavyNu::PlotEventsNumber() {
  //-----------------------------------------------------------------------
    /*TStyle *t2kStyle= new TStyle("T2K","T2K approved plots style");

    // use plain black on white colors
    t2kStyle->SetFrameBorderMode(0);
    t2kStyle->SetCanvasBorderMode(0);
    t2kStyle->SetPadBorderMode(0);
    t2kStyle->SetPadColor(0);
    t2kStyle->SetCanvasColor(0);
    t2kStyle->SetStatColor(0);
    t2kStyle->SetLegendBorderSize(1);

    // set the paper & margin sizes
    t2kStyle->SetPaperSize(20,26);
    t2kStyle->SetPadTopMargin(0.10);
    t2kStyle->SetPadRightMargin(0.16);
    t2kStyle->SetPadBottomMargin(0.16);
    t2kStyle->SetPadLeftMargin(0.13);

    // use large Times-Roman fonts
    t2kStyle->SetTextFont(132);
    t2kStyle->SetTextSize(0.06);
    t2kStyle->SetLabelFont(132,"x");
    t2kStyle->SetLabelFont(132,"y");
    t2kStyle->SetLabelFont(132,"z");
    t2kStyle->SetLabelSize(0.05,"x");
    t2kStyle->SetTitleSize(0.06,"x");
    t2kStyle->SetLabelSize(0.05,"y");
    t2kStyle->SetTitleSize(0.06,"y");
    t2kStyle->SetTitleOffset(0.9,"y");
    t2kStyle->SetTitleOffset(0.75,"z");
    t2kStyle->SetLabelSize(0.05,"z");
    t2kStyle->SetTitleSize(0.06,"z");
    t2kStyle->SetLabelFont(132,"t");
    t2kStyle->SetTitleFont(132,"x");
    t2kStyle->SetTitleFont(132,"y");
    t2kStyle->SetTitleFont(132,"z");
    t2kStyle->SetTitleFont(132,"t");
    t2kStyle->SetTitleFillColor(0);
    t2kStyle->SetTitleX(0.25);
    t2kStyle->SetTitleFontSize(0.06);
    t2kStyle->SetTitleFont(132,"pad");

    t2kStyle->SetTitleBorderSize(1);
    t2kStyle->SetPadBorderSize(1);
    t2kStyle->SetCanvasBorderSize(1);

    // use bold lines and markers
    t2kStyle->SetMarkerStyle(20);
    t2kStyle->SetHistLineWidth(1.85);
    t2kStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes

    // get rid of X error bars and y error bar caps
    t2kStyle->SetErrorX(0.001);

    // do not display any of the standard histogram decorations
    //t2kStyle->SetOptTitle(0);
    //t2kStyle->SetOptStat(0);
    t2kStyle->SetOptFit(0);

    // put tick marks on top and RHS of plots
    t2kStyle->SetPadTickX(1);
    t2kStyle->SetPadTickY(1);

    // Add a greyscale palette for 2D plots
    int ncol=50;
    double dcol = 1./float(ncol);
    double gray = 1;

    TColor **theCols = new TColor*[ncol];

    for (int i=0;i<ncol;i++) theCols[i] = new TColor(999-i,0.0,0.7,0.7);

    for (int j = 0; j < ncol; j++) {
        theCols[j]->SetRGB(gray,gray,gray);
        gray -= dcol;
    }

    int ColJul[100];
    for  (int i=0; i<100; i++) ColJul[i]=999-i;
    t2kStyle->SetPalette(ncol,ColJul);

    // Define a nicer color palette (red->blue)
    // Uncomment these lines for a color palette (default is B&W)
    t2kStyle->SetPalette(1,0);  // use the nice red->blue palette
    const Int_t NRGBs = 5;
    const Int_t NCont = 255;

    // Uncomment these colours for the rainbow (blue -> yellow -> red) palette
    Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };

    TColor::CreateGradientColorTable(NRGBs,stops,red,green,blue,NCont);

    t2kStyle->SetNumberContours(NCont+1);

  // End of definition of t2kStyle
  gROOT->SetStyle("T2K");*/

  /*bool efficiency      = (bool) ND::params().GetParameterD("nd280HNLSim.Efficiency");
  bool AsakaPrediction = (bool) ND::params().GetParameterD("nd280HNLSim.prediction");
  bool run26           = (bool) ND::params().GetParameterD("nd280HNLSim.CompareRun6");

  std::string pdffile = outputFileDir_ + "EventsNumber.pdf";
  std::string pdffilefirst = pdffile + "(";
  std::string pdffilelast = pdffile + ")";

  TCanvas *EventCanvas = new TCanvas("Events","",50,50,1000,800);
  EventCanvas->SetLeftMargin(0.14);

  EventCanvas->Print(pdffilefirst.c_str());
  EventCanvas->SetGrid();*/

  /*
  // Drawing branching ratio
  gPad->SetLogy();
  Br_->SetTitle("Kaon decay branching ratio");
  Br_->Draw("al");
  Br_->SetMaximum(4);
  Br_->SetMinimum(1E-1);
  Br_->GetXaxis()->SetTitle("M, GeV");
  Br_->GetYaxis()->SetTitle("Br(K#rightarrow #muN)/|U#mu|^{2}");
  Br_->GetYaxis()->SetTitleOffset(1.1);
  EventCanvas->Print(pdffile.c_str());
  EventCanvas->Clear();

  ToF_->Draw("colz");
  EventCanvas->Print(pdffile.c_str());
  EventCanvas->Clear();
  Ntof_->Draw("colz");
  EventCanvas->Print(pdffile.c_str());
  EventCanvas->Clear();*/

  std::string root_file = outputFileDir_ + "EventsNumber.root";
  auto* event_file = new TFile(root_file.c_str(), "RECREATE");
  for (auto & Event : Events_) {

    hnlProdMode prod(Event.ProdMode);
    hnlDecayMode dec(Event.DecayMode);

    std::string reaction = prod.GetMix() + dec.GetMix();
    Event.histo->SetName(reaction.c_str());
    Event.histo->Write("", TObject::kOverwrite);
  }

  event_file->Close();

  //Events histo ----> Graph
  /*for (unsigned int i = 0; i < Events_.size(); ++i) {
    TGraph *EventsGraph = new TGraph();
    Int_t nBin = Events_[i].histo->GetNbinsX();
    Int_t j = 0;
    for (Int_t k = 0; k < nBin; ++k) {
      Double_t content = Events_[i].histo->GetBinContent(k);
      if (content > 0.1) {
        EventsGraph->SetPoint(j, Events_[i].histo->GetBinCenter(k), content);
        ++j;
      }
    }

    EventsGraph->SetTitle("HNL at ND280 decay");
    EventCanvas->SetGrid();
    gPad->SetLogy();
    EventsGraph->Draw("alp");
    gPad->Modified();
    EventsGraph->GetXaxis()->SetTitle("M, GeV");
    EventsGraph->GetYaxis()->SetTitle("N");
    EventsGraph->SetMinimum(1E9);
    EventsGraph->SetMaximum(5 * Events_[i].histo->GetBinContent(Events_[i].histo->GetMaximumBin()));
    EventsGraph->GetXaxis()->SetLimits(0.,0.5);

    EventCanvas->Print(pdffile.c_str());
    EventCanvas->Clear();

  }

  for (unsigned int i = 0; i < Events_.size(); ++i) {
    Double_t FC = 2.3;
    Double_t nn = 0.;

    TString fileName = getenv("ND280HNLSIMROOT") + TString("/data/");
    fileName += "Efficiency";
    hnlDecayMode decay(Events_[i].DecayMode);

    if (decay.GetMix().compare("Umu") == 0 && efficiency && decay.GetProdMass().size() < 3) {
      fileName += "MuonPion";
      if (rdpPOT_) {
        FC = 3.89;
        nn = 1.;
        //FC = 2.3;
        //nn = 0.;
      }
    }

    if (decay.GetMix().compare("Ue") == 0  && efficiency && decay.GetProdMass().size() < 3) {
      fileName += "ElectronPion";
      if (rdpPOT_) {
        FC = 3.89;
        nn = 1.;
        //FC = 2.3;
        //nn = 0.;
      }
    }

    if (decay.GetMix().compare("UmuUtauMix") == 0) {
      fileName += "DiMuon";
      if (rdpPOT_) {
        //FC = 5.32;
        //nn = 2;
        FC = 6.68;
        nn = 3.;
        //FC = 3.89;
        //nn = 1;
      }
    }

    if (FC == 2.3)
      std::cout << "Scaling to zero background" << std::endl;

    TGraph* MClimit = new TGraph();



    Int_t nBin = Events_[i].histo->GetNbinsX();
    Int_t j = 0;
    for (Int_t k = 0; k < nBin; ++k) {
      Double_t content = Events_[i].histo->GetBinContent(k);
      if (content > 0.1) {
        MClimit->SetPoint(j, Events_[i].histo->GetBinCenter(k), sqrt(FC/(2.*content)));
        ++j;
      }
    }

    TMultiGraph* mgResult = new TMultiGraph();
    TLegend* legend = new TLegend(1. - EventCanvas->GetRightMargin() - 0.25,   1 - EventCanvas->GetTopMargin() - 0.25,
      1 - EventCanvas->GetRightMargin(), 1. - EventCanvas->GetTopMargin());

    MClimit->SetLineColor(8);
    MClimit->SetLineWidth(3);

    hnlProdMode prod(Events_[i].ProdMode);
    hnlDecayMode dec(Events_[i].DecayMode);

    std::cout << prod.GetMix() << dec.GetMix() << "     " << FC << "    " << nn << std::endl;

    // if efficiency should be taken into account
    if (efficiency) {
      if (!decay.GetMix().compare("Umu") && !decay.GetMix().compare("Ue") && !decay.GetMix().compare("DiMuon")) {
        std::cout << "WARNING in HeavyNu::PlotEventsNumber. Efficiency couldn't be taken for unknown mode, and wouldn't" << std::endl;
        std::cout << "program will exit to avoid incorrect info output" << std::endl;
        std::exit(1);
      }

      TGraph* Eff = new TGraph();
      // get efficiency data from file
      TString Filename1 = fileName + ".dat";
      if (!LoadLimitGraph(Eff, Filename1.Data())) {
        std::cout << "WARNING in HeavyNu::PlotEventsNumber. Something went wrong while making graph from efficiency dat file" << std::endl;
        std::cout << "program will exit to avoid incorrect info output" << std::endl;
        std::exit(1);
      }

      TGraph* sigma = new TGraph();
      // get efficiency data from file
      TString Filename2 = fileName + "_sys.dat";
      if (!LoadLimitGraph(sigma, Filename2.Data())) {
        std::cout << "WARNING in HeavyNu::PlotEventsNumber. Something went wrong while making graph from sigma dat file" << std::endl;
        std::cout << "program will exit to avoid incorrect info output" << std::endl;
        std::exit(1);
      }



      for (Int_t i = 0; i < MClimit->GetN(); ++i) {
        Double_t x, y;

        MClimit->GetPoint(i, x, y);

        // linear interpolation
        Double_t effic = Eff->Eval(x, 0, "");
        Double_t sig = sigma->Eval(x, 0, "");

        Double_t acc = sig;

        // for nonlinear interpolation can use TSpline3 or TSpline5, but TSpline3 gives NaN results.

        // TSpline5 *s = new TSpline5("", Eff->GetX(), Eff->GetY(), Eff->GetN());
        // TSpline3 *s = new TSpline3("", Eff->GetX(), Eff->GetY(), Eff->GetN());
        // Double_t effic = s->Eval(x);


        if (effic < 0)
          effic = 0;
        if (effic > 1)
          effic = 1;

        y /= sqrt(effic);

        Float_t E = FC - nn;

        y *= sqrt(1 + 0.5 * E * acc * acc * (1 + TMath::Power(0.5*E*acc, 2.)));
        //y *= sqrt(1. + (1. - sqrt(1. - acc*acc * E*E))/E);

        MClimit->SetPoint(i, x, y);
        std::cout << prod.GetMix() << dec.GetMix() << "     " << x << "    " << y << std::endl;
      }
    }



    // Define the mixing element
    std::string mixing = "";
    std::string mixingTitle = "";
    if (prod.GetMix().compare(dec.GetMix()) == 0) {
      mixing += prod.GetMix() + "2";
      if (prod.GetMix().compare("Umu") == 0) mixingTitle += "U#mu"; else mixingTitle += prod.GetMix();
      mixingTitle = "#left|" + mixingTitle + "#right|^{2}";
    }
    else {
      mixing += prod.GetMix() + dec.GetMix();
      if (!prod.GetMix().compare("Umu")) mixingTitle += "U#mu"; else mixingTitle += prod.GetMix();
      if (!dec.GetMix().compare("Umu")) mixingTitle += "U#mu"; else mixingTitle += dec.GetMix();
      if (!mixingTitle.compare("U#muUe")) mixingTitle = "UeU#mu";
      if (!mixing.compare("UmuUe")) mixing = "UeUmu";
      mixingTitle = "#left|" + mixingTitle + "#right|";
      if (!mixing.compare("UeUmuUtauMix")) mixingTitle = "#left|U_{e}#right|#sqrt{#left|U_{e}#right|^{2}+#left|U_{#tau}#right|^{2}}";
    }


    Double_t x, y;
    Int_t npoints;

    mgResult->Add(MClimit,"l");

    if (!mixing.compare("UeUmu") && rdpPOT_ && efficiency) {
      TGraph* MClimit2 = new TGraph();

      TString fileName = getenv("ND280HNLSIMROOT") + TString("/data/");
      fileName += "MixingMix.dat";

      if (LoadLimitGraph(MClimit2, fileName.Data())){
        MClimit2->SetLineColor(4);
        MClimit2->SetLineWidth(3);
        mgResult->Add(MClimit2, "l");
        legend->AddEntry(MClimit, "Full MC K#rightarrow e(#mu#pi)", "lp");
        legend->AddEntry(MClimit2, "Full MC K#rightarrow #mu(e#pi)", "lp");
      }

      if (run26){
        TGraph* MClimit3 = new TGraph();
        TString fileName = getenv("ND280HNLSIMROOT") + TString("/data/");
        fileName += "MixingMix27.dat";
        if (LoadLimitGraph(MClimit3, fileName.Data())){
          MClimit3->SetLineColor(8);
          MClimit3->SetLineWidth(3);
          mgResult->Add(MClimit3, "l");
        }


        TGraph* MClimit4 = new TGraph();
        fileName = getenv("ND280HNLSIMROOT") + TString("/data/");
        fileName += "MixingMix27.dat";
        if (LoadLimitGraph(MClimit4, fileName.Data())){
          MClimit4->SetLineColor(4);
          MClimit4->SetLineWidth(3);
          mgResult->Add(MClimit4, "l");
        }
      }

    } else
      legend->AddEntry(MClimit, "Full MC", "lp");

    if (AsakaPrediction || run26) {
    // Draw constriction from arxiv_1212_1062 if we have

        TString fileName = getenv("ND280HNLSIMROOT");

        if (AsakaPrediction && Events_[i].arxiv_1212_1062) {
          fileName += TString("/data/external/") + mixing;

          if (dec.GetProdMass().size() == 2)
            fileName += "_2body_";
          if (dec.GetProdMass().size() == 3)
            fileName += "_3body_";

          fileName += "arxiv_1212_1062.dat";
        }

        if (run26) {
          fileName += TString("/data/");

          if (!mixing.compare("Ue2"))
            fileName += TString("MixingUe.dat");
          if (!mixing.compare("Umu2"))
            fileName += TString("MixingUmu.dat");
          if (!mixing.compare("UeUmuUtauMix"))
            fileName += TString("MixingUeMix.dat");
          if (mixing.compare("UeUmu")) {

          TGraph* MClimit5 = new TGraph();
          std::cout << fileName << std::endl;
          if (LoadLimitGraph(MClimit5, fileName.Data())){
            std::cout << "OK" << std::endl;
            MClimit5->SetLineColor(4);
            MClimit5->SetLineWidth(3);
            for (Int_t i = 0; i < MClimit5->GetN(); ++i) {
              Double_t x, y;
              MClimit5->GetPoint(i, x, y);
            }
            mgResult->Add(MClimit5, "l");
          }
        }
        }


        TGraph* limit_arxiv = new TGraph();

        if (AsakaPrediction && LoadLimitGraph(limit_arxiv, fileName.Data())){

          //rescale to new FV
          npoints = limit_arxiv->GetN();

          Double_t factor = 3.* 8.*config::config_man().DetConfig().FV().xSizeFV*config::config_man().DetConfig().FV()  .ySizeFV* config::config_man  ().DetConfig().FV().zSizeFV/(61.25*1e6);

          factor *= 2.;

          for (Int_t i = 0; i<npoints; i++){
            if(limit_arxiv->GetPoint(i, x, y)>-1)
              limit_arxiv->SetPoint(i, x, y/std::sqrt(factor));
          }

          limit_arxiv->SetLineColor(4);
          limit_arxiv->SetLineWidth(3);
          mgResult->Add(limit_arxiv,"l");
          legend->AddEntry(limit_arxiv, "Asaka prediction", "lp");

          if (run26 && LoadLimitGraph(limit_arxiv, fileName.Data())){
            // draw run 2-6 prediction
            limit_arxiv->SetLineColor(4);
            limit_arxiv->SetLineWidth(3);
            mgResult->Add(limit_arxiv,"l");
            legend->AddEntry(limit_arxiv, "Run 2-6 sensitivity", "lp");
          }
        }
      }


    std::string charge;
    //if (MesonCharge_ == 1) charge = "positive"; else charge = "negative";

    std::string title = "#splitline{" + mixingTitle + " Sensitivity for ";
    std::stringstream stream;

    if (!mixing.compare("UeUmu"))
      title += "{" + charge + " " + std::string(Events_[i].histo->GetTitle()) + " and K#rightarrow #mu+N, N#rightarrow e+#pi" + "}";
    else title += "{" + charge + " " + std::string(Events_[i].histo->GetTitle()) + "}";


    mgResult->SetMaximum(1e-6);

    mgResult->SetMinimum(1e-10);

    if (decay.GetMix().compare("UmuUtauMix") == 0) {
      mgResult->SetMaximum(1e-5);
      mgResult->SetMinimum(1e-9);
    }

    gPad->SetLogy();

    // Draw constriction from ps191 if we have
    if (Events_[i].ps191) {
      TString fileName = getenv("ND280HNLSIMROOT") + TString("/data/external/");

      fileName += mixing;

      if (dec.GetProdMass().size() == 2)
        fileName += "_2body_";

      if (dec.GetProdMass().size() == 3)
        fileName += "_3body_";

      fileName += "ps191.dat";

      // Limits from previous experiments
      TGraph* limit_ps191 = new TGraph();
      TGraph* limit_tmp = NULL;

      if (LoadLimitGraph(limit_ps191, fileName.Data())){

      Int_t npoints = limit_ps191->GetN();

        Double_t factor = 2;
        Double_t x, y;

        for (Int_t i = 0; i<npoints; i++){
          if(limit_ps191->GetPoint(i, x, y)>-1)
            limit_ps191->SetPoint(i, x, y/std::sqrt(factor));
        }


        limit_ps191->SetFillStyle(3005);
        limit_ps191->SetLineColor(2);
        limit_ps191->SetLineWidth(3);
        limit_ps191->SetFillColor(42);

        limit_tmp = new TGraph(*limit_ps191);

        mgResult->Add(limit_ps191,"f");

        mgResult->Draw("a");

        gPad->Update();

        Double_t ymax = std::pow(10, gPad->GetUymax());

        npoints = limit_ps191->GetN();

        if(limit_ps191->GetPoint(npoints-1, x, y)>-1)
          limit_ps191->SetPoint(npoints-1, x, ymax);

        if(limit_ps191->GetPoint(0, x, y)>-1)
          limit_ps191->SetPoint(0, x, ymax);

        limit_tmp->RemovePoint(0);
        limit_tmp->RemovePoint(npoints-1);

        mgResult->Add(limit_tmp,"l");
        legend->AddEntry(limit_tmp, "PS191");

      }
    }

    mgResult->Draw("a");
    legend->Draw();
    //pt->Draw();

    mgResult->GetXaxis()->SetTitle("Mass, GeV");
    mgResult->GetYaxis()->SetTitle(mixingTitle.c_str());
    mgResult->GetYaxis()->SetTitleOffset(1.1);

    gPad->Update();

    EventCanvas->Print(pdffile.c_str());

    delete MClimit;
    delete mgResult;

    EventCanvas->Clear();
  }
  EventCanvas->Print(pdffilelast.c_str());

  EventCanvas->Clear();
  gStyle->SetOptStat(kFALSE);

  EventCanvas->Divide(1,2);
  EventCanvas->cd(1);
  DecayZY_[0]->Draw("colz");
  EventCanvas->cd(2);
  DecayZX_[0]->Draw("colz");
  EventCanvas->Print("mass_distribution.pdf");
  EventCanvas->Clear();

  EventCanvas->Divide(1,2);
  EventCanvas->cd(1);
  DecayZY_[1]->Draw("colz");
  EventCanvas->cd(2);
  DecayZX_[1]->Draw("colz");
  EventCanvas->Print("mass_distribution.pdf)");*/
}

//-----------------------------------------------------------------------
void HeavyNu::PlotSecondaryParticles() {
  //-----------------------------------------------------------------------
  std::string pdffile = outputFileDir_ + "SecondaryParticles.pdf";
  std::string pdffilefirst = pdffile + "(";
  std::string pdffilelast = pdffile + ")";

  TStyle *t2kStyle= new TStyle("T2K","T2K approved plots style");

  // use plain black on white colors
  t2kStyle->SetFrameBorderMode(0);
  t2kStyle->SetCanvasBorderMode(0);
  t2kStyle->SetPadBorderMode(0);
  t2kStyle->SetPadColor(0);
  t2kStyle->SetCanvasColor(0);
  t2kStyle->SetStatColor(0);
  t2kStyle->SetLegendBorderSize(1);
  // set the paper & margin sizes
  t2kStyle->SetPaperSize(20,26);
  t2kStyle->SetPadTopMargin(0.10);
  t2kStyle->SetPadRightMargin(0.16);
  t2kStyle->SetPadBottomMargin(0.16);
  t2kStyle->SetPadLeftMargin(0.13);
  // use large Times-Roman fonts
  t2kStyle->SetTextFont(132);
  t2kStyle->SetTextSize(0.06);
  t2kStyle->SetLabelFont(132,"x");
  t2kStyle->SetLabelFont(132,"y");
  t2kStyle->SetLabelFont(132,"z");
  t2kStyle->SetLabelSize(0.05,"x");
  t2kStyle->SetTitleSize(0.06,"x");
  t2kStyle->SetLabelSize(0.05,"y");
  t2kStyle->SetTitleSize(0.06,"y");
  t2kStyle->SetTitleOffset(0.9,"y");
  t2kStyle->SetTitleOffset(0.75,"z");
  t2kStyle->SetLabelSize(0.05,"z");
  t2kStyle->SetTitleSize(0.06,"z");
  t2kStyle->SetLabelFont(132,"t");
  t2kStyle->SetTitleFont(132,"x");
  t2kStyle->SetTitleFont(132,"y");
  t2kStyle->SetTitleFont(132,"z");
  t2kStyle->SetTitleFont(132,"t");
  t2kStyle->SetTitleFillColor(0);
  t2kStyle->SetTitleX(0.25);
  t2kStyle->SetTitleFontSize(0.06);
  t2kStyle->SetTitleFont(132,"pad");
  t2kStyle->SetTitleBorderSize(1);
  t2kStyle->SetPadBorderSize(1);
  t2kStyle->SetCanvasBorderSize(1);
  // use bold lines and markers
  t2kStyle->SetMarkerStyle(20);
  t2kStyle->SetHistLineWidth((short)1.85);
  t2kStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes
  // get rid of X error bars and y error bar caps
  t2kStyle->SetErrorX(0.001);
  // do not display any of the standard histogram decorations
  //   t2kStyle->SetOptTitle(0);
  //t2kStyle->SetOptStat(0);
  t2kStyle->SetOptFit(0);
  // put tick marks on top and RHS of plots
  t2kStyle->SetPadTickX(1);
  t2kStyle->SetPadTickY(1);
  // Add a greyscale palette for 2D plots
  int ncol=50;
  double dcol = 1./float(ncol);
  double gray = 1;
  TColor **theCols = new TColor*[ncol];
  for (int i=0;i<ncol;i++) theCols[i] = new TColor(999-i,0.0,0.7,0.7);
  for (int j = 0; j < ncol; j++) {
      theCols[j]->SetRGB(gray,gray,gray);
      gray -= dcol;
  }
  int ColJul[100];
  for  (int i=0; i<100; i++) ColJul[i]=999-i;
  t2kStyle->SetPalette(ncol,ColJul);

  // End of definition of t2kStyle
  gROOT->SetStyle("T2K");

  TCanvas *SpectrumCanvas = new TCanvas("Spectrum","",50,50,1000,800);

  SpectrumCanvas->Print(pdffilefirst.c_str());

  // cicle over all modes
  for (unsigned int i = 0; i < DecFirstP_.size(); ++i) {

    /*TLegend *legendTOF = new TLegend(0.6,0.6,0.9,0.9);
    legendTOF->SetHeader("Heavy Nu mass");

    for (UInt_t j = 0; j < tofMass[i].size(); ++j) {
      Double_t integral = tofMass[i][j]->Integral();
      for (Int_t k = 0; k <= tofMass[i][j]->GetNbinsX(); ++k) {
        Double_t content = tofMass[i][j]->GetBinContent(k) / integral;
        tofMass[i][j]->SetBinContent(k, content);
      }
    }

    Double_t maxTof = 0;
    for (Int_t j = tofMass[i].size()-2; j > 0 ; --j) {
      if (tofMass[i][j]->GetBinContent(tofMass[i][j]->GetMaximumBin()) > maxTof)
        maxTof = tofMass[i][j]->GetBinContent(tofMass[i][j]->GetMaximumBin());
    }

    gStyle->SetOptStat(kFALSE);
    gPad->SetLogy(0);
    int drawToF[4] = {30, 35, 40, 45};
    tofMass[i][drawToF[0]]->SetMaximum(0.1);
    tofMass[i][drawToF[0]]->Draw();
    tofMass[i][drawToF[0]]->SetLineColor(1);
    legendTOF->AddEntry(tofMass[i][drawToF[0]], Form("%g GeV", round(gHnuMass_[drawToF[0]]*100)/100), "lp");
    for (Int_t j = 1; j < 4 ; ++j) {
      tofMass[i][drawToF[j]]->Draw("same");
      tofMass[i][drawToF[j]]->SetLineColor(j+1);
      legendTOF->AddEntry(tofMass[i][drawToF[j]], Form("%g GeV", round(gHnuMass_[drawToF[j]]*100)/100), "lp");
    }
    legendTOF->Draw();
    SpectrumCanvas->Print(pdffile.c_str());
    SpectrumCanvas->Clear();*/

    unsigned int k = 0;
    // find minimum mass index for this reaction
    while (1) {
      if (DecFirstP_[i][k]->GetBinContent(DecFirstP_[i][k]->GetMaximumBin()) > 0.1)
        break;
      else ++k;
    }
    Int_t MinIndex = k;
    // find max mass index for reaction
    while (1) {
      if (k == DecFirstP_[i].size())
        break;
      if (DecFirstP_[i][k]->GetBinContent(DecFirstP_[i][k]->GetMaximumBin()) < 0.1)
        break;
      else ++k;
    }

    Int_t MaxIndex = k;
    Int_t step = (int)( (MaxIndex - MinIndex) / 10 );

    // Set indexes for mass array for plots
    Int_t draw[4];
    for (Int_t j = 5; j < 9; ++j) {
      draw[j-5] = MinIndex + step * j ;
    }

    gPad->SetLogy();
    gStyle->SetOptStat(kFALSE);

    TLegend *legendP = new TLegend(0.5,0.5,0.8,0.8);
    legendP->SetHeader("Heavy Nu mass");
    Double_t max = 0;

    for (Int_t j = 0; j < 4; ++j) {
      Double_t test = DecFirstP_[i][draw[j]]->GetMaximum();
      if (max < test)
        max = test;
    }

    // draw first particle momentum spectrum
    DecFirstP_[i][draw[0]]->SetMinimum(5E14);
    DecFirstP_[i][draw[0]]->SetMaximum(max * 2.);
    DecFirstP_[i][draw[0]]->Draw();
    DecFirstP_[i][draw[0]]->SetLineColor(1);
    legendP->AddEntry(DecFirstP_[i][draw[0]], Form("%g GeV", round(gHnuMass_[draw[0]]*100)/100), "lp");

    for (Int_t j = 1; j < 4; ++j) {
      DecFirstP_[i][draw[j]]->Draw("SAME");
      DecFirstP_[i][draw[j]]->SetLineColor(j+1);
      legendP->AddEntry(DecFirstP_[i][draw[j]], Form("%g GeV", round(gHnuMass_[draw[j]]*100)/100), "lp");
    }

    legendP->Draw();

    SpectrumCanvas->Print(pdffile.c_str());
    SpectrumCanvas->Clear();

    gPad->SetLogy();
    gStyle->SetOptStat(kFALSE);
    legendP->Clear();
    TLegend *legendT = new TLegend(0.2,0.5,0.5,0.8);
    legendT->SetHeader("Heavy Nu mass");
    max = 0;
    for (Int_t j = 0; j < 4; ++j) {
      Double_t test = DecFirstCos_[i][draw[j]]->GetMaximum();
      if (max < test)
        max = test;
    }
    // draw first particle angular spectrum
    DecFirstCos_[i][draw[0]]->SetMinimum(1E12);
    DecFirstCos_[i][draw[0]]->SetMaximum(max * 2.);
    DecFirstCos_[i][draw[0]]->Draw();
    DecFirstCos_[i][draw[0]]->SetLineColor(1);
    legendT->AddEntry(DecFirstCos_[i][draw[0]], Form("%g GeV", round(gHnuMass_[draw[0]]*100)/100), "lp");

    for (Int_t j = 1; j < 4; ++j) {
      DecFirstCos_[i][draw[j]]->Draw("SAME");
      DecFirstCos_[i][draw[j]]->SetLineColor(j+1);
      legendT->AddEntry(DecFirstCos_[i][draw[j]], Form("%g GeV", round(gHnuMass_[draw[j]]*100)/100), "lp");
    }

    legendT->Draw();

    SpectrumCanvas->Print(pdffile.c_str());
    SpectrumCanvas->Clear();


    gPad->SetLogy();
    gStyle->SetOptStat(kFALSE);
    legendP->Clear();
    legendP->SetHeader("Heavy Nu mass");
    max = 0;
    for (Int_t j = 0; j < 4; ++j) {
      Double_t test = DecSecondP_[i][draw[j]]->GetMaximum();
      if (max < test)
        max = test;
    }

    // draw second particle momentum spectrum
    DecSecondP_[i][draw[0]]->SetMinimum(5E14);
    DecSecondP_[i][draw[0]]->SetMaximum(max * 2.);
    DecSecondP_[i][draw[0]]->Draw();
    DecSecondP_[i][draw[0]]->SetLineColor(1);
    legendP->AddEntry(DecSecondP_[i][draw[0]], Form("%g GeV", round(gHnuMass_[draw[0]]*100)/100), "lp");

    for (Int_t j = 1; j < 4; ++j) {
      DecSecondP_[i][draw[j]]->Draw("SAME");
      DecSecondP_[i][draw[j]]->SetLineColor(j+1);
      legendP->AddEntry(DecSecondP_[i][draw[j]], Form("%g GeV", round(gHnuMass_[draw[j]]*100)/100), "lp");
    }

    legendP->Draw();

    SpectrumCanvas->Print(pdffile.c_str());
    SpectrumCanvas->Clear();

    gPad->SetLogy();
    gStyle->SetOptStat(kFALSE);
    legendT->Clear();
    legendT->SetHeader("Heavy Nu mass");
    max = 0;
    for (Int_t j = 0; j < 4; ++j) {
      Double_t test = DecSecondCos_[i][draw[j]]->GetMaximum();
      if (max < test)
        max = test;
    }

    // draw second particle angular spectrum
    DecSecondCos_[i][draw[0]]->SetMinimum(1E12);
    DecSecondCos_[i][draw[0]]->SetMaximum(max * 2.);
    DecSecondCos_[i][draw[0]]->Draw();
    DecSecondCos_[i][draw[0]]->SetLineColor(1);
    legendT->AddEntry(DecSecondCos_[i][draw[0]], Form("%g GeV", round(gHnuMass_[draw[0]]*100)/100), "lp");

    for (Int_t j = 1; j < 4; ++j) {
      DecSecondCos_[i][draw[j]]->Draw("SAME");
      DecSecondCos_[i][draw[j]]->SetLineColor(j+1);
      legendT->AddEntry(DecSecondCos_[i][draw[j]], Form("%g GeV", round(gHnuMass_[draw[j]]*100)/100), "lp");
    }

    legendT->Draw();

    SpectrumCanvas->Print(pdffile.c_str());
    SpectrumCanvas->Clear();


    gPad->SetLogy();
    gStyle->SetOptStat(kFALSE);
    legendT->Clear();
    legendT->SetHeader("Heavy Nu mass");
    max = 0;
    for (Int_t j = 0; j < 4; ++j) {
      Double_t test = DecRelativeCos_[i][draw[j]]->GetMaximum();
      if (max < test)
        max = test;
    }

    // draw relative momentum spectrum
    DecRelativeCos_[i][draw[0]]->SetMinimum(1E12);
    DecRelativeCos_[i][draw[0]]->SetMaximum(max * 2.);
    DecRelativeCos_[i][draw[0]]->Draw();
    DecRelativeCos_[i][draw[0]]->SetLineColor(1);
    legendT->AddEntry(DecRelativeCos_[i][draw[0]], Form("%g GeV", round(gHnuMass_[draw[0]]*100)/100), "lp");

    for (Int_t j = 1; j < 4; ++j) {
      DecRelativeCos_[i][draw[j]]->Draw("SAME");
      DecRelativeCos_[i][draw[j]]->SetLineColor(j+1);
      legendT->AddEntry(DecRelativeCos_[i][draw[j]], Form("%g GeV", round(gHnuMass_[draw[j]]*100)/100), "lp");
    }

    legendT->Draw();

    SpectrumCanvas->Print(pdffile.c_str());
    SpectrumCanvas->Clear();
  }


  SpectrumCanvas->Print(pdffilelast.c_str());

}

//-----------------------------------------------------------------------
void HeavyNu::PlotHnuFluxData() {
  //-----------------------------------------------------------------------
  TStyle *t2kStyle= new TStyle("T2K","T2K approved plots style");

  // use plain black on white colors
  t2kStyle->SetFrameBorderMode(0);
  t2kStyle->SetCanvasBorderMode(0);
  t2kStyle->SetPadBorderMode(0);
  t2kStyle->SetPadColor(0);
  t2kStyle->SetCanvasColor(0);
  t2kStyle->SetStatColor(0);
  t2kStyle->SetLegendBorderSize(1);
  // set the paper & margin sizes
  t2kStyle->SetPaperSize(20,26);
  t2kStyle->SetPadTopMargin(0.10);
  t2kStyle->SetPadRightMargin(0.16);
  t2kStyle->SetPadBottomMargin(0.16);
  t2kStyle->SetPadLeftMargin(0.13);
  // use large Times-Roman fonts
  t2kStyle->SetTextFont(132);
  t2kStyle->SetTextSize(0.06);
  t2kStyle->SetLabelFont(132,"x");
  t2kStyle->SetLabelFont(132,"y");
  t2kStyle->SetLabelFont(132,"z");
  t2kStyle->SetLabelSize(0.05,"x");
  t2kStyle->SetTitleSize(0.06,"x");
  t2kStyle->SetLabelSize(0.05,"y");
  t2kStyle->SetTitleSize(0.06,"y");
  t2kStyle->SetTitleOffset(0.9,"y");
  t2kStyle->SetTitleOffset(0.75,"z");
  t2kStyle->SetLabelSize(0.05,"z");
  t2kStyle->SetTitleSize(0.06,"z");
  t2kStyle->SetLabelFont(132,"t");
  t2kStyle->SetTitleFont(132,"x");
  t2kStyle->SetTitleFont(132,"y");
  t2kStyle->SetTitleFont(132,"z");
  t2kStyle->SetTitleFont(132,"t");
  t2kStyle->SetTitleFillColor(0);
  t2kStyle->SetTitleX(0.25);
  t2kStyle->SetTitleFontSize(0.06);
  t2kStyle->SetTitleFont(132,"pad");
  t2kStyle->SetTitleBorderSize(1);
  t2kStyle->SetPadBorderSize(1);
  t2kStyle->SetCanvasBorderSize(1);
  // use bold lines and markers
  t2kStyle->SetMarkerStyle(20);
  t2kStyle->SetHistLineWidth((short)1.85);
  t2kStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes
  // get rid of X error bars and y error bar caps
  t2kStyle->SetErrorX(0.001);
  // do not display any of the standard histogram decorations
  t2kStyle->SetOptFit(0);
  // put tick marks on top and RHS of plots
  t2kStyle->SetPadTickX(1);
  t2kStyle->SetPadTickY(1);
  // Add a greyscale palette for 2D plots
  int ncol=50;
  double dcol = 1./float(ncol);
  double gray = 1;
  TColor **theCols = new TColor*[ncol];
  for (int i=0;i<ncol;i++) theCols[i] = new TColor(999-i,0.0,0.7,0.7);
  for (int j = 0; j < ncol; j++) {
      theCols[j]->SetRGB(gray,gray,gray);
      gray -= dcol;
  }
  int ColJul[100];
  for  (int i=0; i<100; i++) ColJul[i]=999-i;
  t2kStyle->SetPalette(ncol,ColJul);

  // End of definition of t2kStyle
  gROOT->SetStyle("T2K");
  std::string pdffile = outputFileDir_ + "FluxData.pdf";
  std::string pdffilefirst = pdffile + "(";
  std::string pdffilelast = pdffile + ")";

  TCanvas *fl = new TCanvas("fl","",50,50,1000,800);

  fl->Print(pdffilefirst.c_str());

  for (unsigned int i = 0; i < FluxFileName_.size(); ++i) {
    std::string GenRoot = FluxFileName_[i];

    TFile* FluxFile = new TFile(GenRoot.c_str(), "READ");
    if (!FluxFile->IsOpen()) {
      std::cout << "Error in 'HeavyNu::PlotHnuFluxData'. File with heavy nu flux trees was not found" << std::endl;
      std::cout << "Skipping file " << FluxFileName_[i] << std::endl;
      continue;
    }
    FluxFile->cd();

    TTree* FluxHeader = (TTree*)FluxFile->Get("header");

    // Read header tree
    FluxHeader = (TTree*)FluxFile->Get("header");

    Double_t MinMass, MaxMass;
    Int_t mode_number;
    FluxHeader->SetBranchAddress("mode",    &mode_number);
    FluxHeader->SetBranchAddress("minmass",   &MinMass);
    FluxHeader->SetBranchAddress("maxmass",   &MaxMass);
    FluxHeader->SetBranchAddress("size",    &gSizeMu_);

    FluxHeader->GetEntry(0);

    hnlProdMode::hnlProdEnum prod = utils::GetProdEnum(mode_number);
    hnlProdMode ProdMode(prod);

    // Fill the mass array from header file
    Double_t step = (MaxMass - MinMass) / gSizeMu_;
    gHnuMass_.resize(gSizeMu_, 0);
    for (Int_t k = 0; k < gSizeMu_; ++k)
      gHnuMass_[k] = MinMass + step * k;

    gStyle->SetPalette(1);

    gPad->SetLogy();

    std::vector<TH1D*> hnuFluxHistos;
    hnuFluxHistos.resize(gSizeMu_);

    std::stringstream stream;

    for (Int_t j = 0; j < gSizeMu_; ++j) {
      stream.str("");
      stream << j;
      std::string title = "hnuFlux" + stream.str();
      hnuFluxHistos[j] = (TH1D*)FluxFile->Get(title.c_str());
      if (!hnuFluxHistos[j]) {
        gSizeMu_ = j;
        break;
      }
    }

    TH1D* baseline = (TH1D*)FluxFile->Get("baseline");

    gStyle->SetOptStat(0);

    //Top-right legendP
    TLegend* leg1 = new TLegend(1. - gStyle->GetPadRightMargin() - 0.3,   1 - gStyle->GetPadTopMargin() - 0.3,
      1 - gStyle->GetPadRightMargin(), 1. - gStyle->GetPadTopMargin());

    leg1->AddEntry(baseline, "Kmu2");

    Int_t color = 1;

    gPad->SetLogy();

    baseline->Draw();
    baseline->SetLineColor(color);
    baseline->SetMaximum(3e10);
    baseline->SetMinimum(1e6);

    Double_t max = baseline->GetMaximum();


    for (Int_t j = 0; j < gSizeMu_; ++j) {
      Double_t mass = round(gHnuMass_[j]*100);
      std::cout << mass << std::endl;
      if (mass != 5 && mass != 10 && mass != 20 && mass != 30 && mass != 35)
        continue;
      leg1->AddEntry(hnuFluxHistos[j],Form("HNu Mass = %g GeV", mass/100));
      ++color;
      hnuFluxHistos[j]->SetLineColor(color);
      hnuFluxHistos[j]->Draw("same");
      if (hnuFluxHistos[j]->GetMaximum() > max)
        max = hnuFluxHistos[j]->GetMaximum();
    }

    baseline->SetMaximum(max + 0.2*max);
    std::string title = "Flux from reaction " + ProdMode.GetReaction();
    baseline->SetTitle(title.c_str());
    baseline->GetYaxis()->SetTitle("flux");
    gPad->Update();

    leg1->SetTextFont(22);
    leg1->Draw("same");

    fl->Print(pdffile.c_str());
    FluxFile->Close();
  }
  fl->Print(pdffilelast.c_str());
}

//-----------------------------------------------------------------------
void HeavyNu::PrintUsage(const std::string& programName) {
  //-----------------------------------------------------------------------
  std::cout << "usage: " << programName << " [options] " << std::endl;
  std::cout << std::endl;
  std::cout << "Options:" << std::endl;
  std::cout << "    -o <Output directory>  Set the name of an output directory. Default = $ND280HNLSIMROOT/MCStudy/" << std::endl;
  std::cout << "    -n <Flux file list>    Regenerate Heavy Nu tree file (root file)" << std::endl;
  std::cout << "    -s                     Plot sensitivity (pdf file)" << std::endl;
  std::cout << "    -f                     Plot Heavy Nu flux data" << std::endl;
  std::cout << "    -d                     Plot secondary particles histoes (pdf file)" << std::endl;
  std::cout << "    -m                     Generate nd280Control Script" << std::endl;
  std::cout << "    -p  <params fiel>      Overrite the standard parameter file with a given one" << std::endl;
  std::exit(1);
}

//-----------------------------------------------------------------------
void HeavyNu::GenerateControlScript() {
  //-----------------------------------------------------------------------
  std::ofstream ScriptFile;
  std::string ScriptFileName = outputFileDir_ + "MCStudy.cfg";
  ScriptFile.open(ScriptFileName.c_str());
  ScriptFile << "# Final step of sand simulation; running nd280mc, elecSim, oaCalib, oaRecon and oaAnalysis.\n";
  ScriptFile << "# Generated automaticaly with nd280HNLSim package!.\n";
  ScriptFile << "[configuration]\n";
  ScriptFile << "module_list = nd280MC elecSim oaCalibMC oaRecon oaAnalysis\n";
  ScriptFile << "inputfile = " << output_ << "\n";
  ScriptFile << "[geometry]\n";
  ScriptFile << "baseline = Full\n";
  ScriptFile << "p0d_water_fill = 0\n";
  ScriptFile << "[nd280mc]\n";
  ScriptFile << "num_events = 5000\n";
  ScriptFile << "mc_type = Neut_RooTracker\n";
  ScriptFile << "random_seed = TIME\n";
  ScriptFile << "nbunches = 8\n";
  ScriptFile << "interactions_per_spill = 1\n";
  ScriptFile << "count_type = fixed\n";
  ScriptFile << "mc_full_spill = 1\n";
  ScriptFile << "time_offset = 50\n";
  ScriptFile.close();

}

bool HeavyNu::LoadLimitGraph(TGraph* gr, const std::string& fileName) {
  //-----------------------------------------------------------------------
  if(!gr)
    return false;

  //reset the graph
  gr->Set(0);

  std::ifstream refFile(fileName.c_str());

  if (!refFile) {
    std::cout << "\n***** LoadLimitGraph *****\n" << "Cannot open input file '" << fileName.c_str() << std::endl;
    return false;
  }

  Int_t i = 0;
  Double_t mass;
  Double_t limit;


  bool first = true;

  while(refFile.good()){

    if(refFile.eof()) break;

    refFile >> mass;
    refFile >> limit;

    gr->SetPoint(i, mass, limit);

    i++;

    if (first){
      gr->SetPoint(i, mass, limit);
      i++;

      first = false;
    }
  }

  Double_t x, y;
  Int_t npoints = gr->GetN();

  if(gr->GetPoint(npoints-1, x, y) > -1)
    gr->SetPoint(npoints, x, y);

  return true;
}