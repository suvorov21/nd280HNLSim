#ifndef TCONFIGMANAGER_H
#define TCONFIGMANAGER_H

#include <iostream>
#include <sstream>
#include <map>

//hnuFlux
#include "Units.h"

/// A collection of utilities to provide storage for various confgurations used in analysis
namespace config{

  /// Near detector positions and size
  struct DetPlane{
    Double_t xPosND;
    Double_t yPosND;
    Double_t zPosND;

    Double_t xSizeND;
    Double_t ySizeND;
  };

  /// Effective FV
  struct DetFV{
    Double_t xSizeFV;
    Double_t ySizeFV;
    Double_t zSizeFV;

    Double_t zPosFV1;
    Double_t zPosFV2;
    Double_t zPosFV3;
  };

  /// Detector configuration that consists of basic part for flux simulation and FV definition
  /// Actually a simple pair
  typedef std::pair< config::DetPlane, config::DetFV > DetPair;  
  class DetConfig : public DetPair {
  public: 
    DetConfig(const std::string& name, bool read_params = false); 
    virtual ~DetConfig()= default;
   
    config::DetPlane& Plane(){return first;}
    config::DetFV&    FV(){return second;}
    
    Double_t Volume(){ 
      return 8.*FV().xSizeFV*FV().ySizeFV*FV().zSizeFV;
    }
     
    const std::string& Name(){return name__;}

  private:
    std::string name__;
  };

}

/// A singleton to provide dealing with configuration
class TConfigManager
{

public:
 
  virtual ~TConfigManager(){}
  
  static TConfigManager& Get(void);
  static TConfigManager& Get(const std::string&);

  /// add configuration with name 
  void add_config(const std::string& m);

  size_t NConfigs(){return config_map__.size();}

  void select_config(const std::string& config){selected_config__ = selected_config_tmp__ = config;}

  const std::string& selected_config(){return selected_config_tmp__;}

  config::DetConfig& DetConfig(){
    return *config_map__[selected_config_tmp__];
  }
  
  /// read original detector positions from a given flux file
  /// the ones used to get the initial weights
  /// get number of POT per file etc
  void ReadInitialFluxConfig(const std::string& fileName);
  
  /// return the status whether the inital flux config was initialized
  bool GetInitReady(){
    return ini_config_set__;
  }
  
  /// Get initial ND280 config used
  const Int_t& GetNDConfigInit(){
    return NDconfig__;
  }
  
  /// Getters for initial detector configuration
  const config::DetPlane& GetNDPlaneInit(){
    return detPlaneInit__;
  }
  
  Int_t GetNTriggersInFile(){
    return  nTrigFile__;
  } 
  

private: 

  /// singleton instance
  static TConfigManager* p_instance;

  /// private constructors
  TConfigManager();
  TConfigManager(const std::string&);
  TConfigManager(const TConfigManager&);  
  TConfigManager& operator=(TConfigManager&);

  std::map<std::string, config::DetConfig*> config_map__;
  std::string selected_config__;
  std::string selected_config_tmp__;  

  void select_config_tmp(const std::string& config){selected_config_tmp__ = config;}
  
  /// Whether initial flux configuration was already initialzed
  bool ini_config_set__;
  
   /// Detector configuration in original flux files
  Float_t xPosInit__[15];
  Float_t yPosInit__[15];
  Float_t zPosInit__[15];
  
  Float_t xSizeInit__[15];
  Float_t ySizeInit__[15];
  
  /// Number of triggers per file
  Int_t nTrigFile__;
  
  /// Which ND configuration to read from flux files
  Int_t NDconfig__;
  
  /// Original ND plane configuration (flux files) used in the analysis
  config::DetPlane detPlaneInit__;

};


namespace config{
  TConfigManager& config_man();
  TConfigManager& config_man(const std::string&);
}

#endif



