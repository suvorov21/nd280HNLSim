#ifndef HeavyNu_h
#define HeavyNu_h

#include <iostream>
#include <map>

#include "TVector3.h"
#include "Utils.h"


class TFile;
class TTree;
class TH1D;
class TH2D;
class TBits;
class TObjString;
class TGraph;

class HeavyNu {
public:
  HeavyNu(int argc, char *argv[]);
  virtual ~HeavyNu();
  void Initialize();
  void Process();
  void Finalize();

  void PrintUsage(const std::string& programName);
  bool TreeHistoCreate(std::string);

  // calculate decay of heavy nu
  void HnuDecay(TTree*&, hnlProdMode::hnlProdEnum&, hnlDecayMode::hnlDecayEnum);

  // Plot functions
  void PlotEventsNumber();
  void PlotSecondaryParticles();
  void PlotHnuFluxData();
  void GenerateControlScript();

  // Calculate event probability
  Double_t EventProbability(const TVector3&, const TVector3&, const Int_t&, const Double_t&, TVector3&);
    bool LoadLimitGraph(TGraph*, const std::string&);

protected:
    // file adress
  std::string outputFileDir_;
  std::string inputListFiles_;

  // vector with input flux files
  std::vector<std::string> FluxFileName_;
  std::string OutFileStart_;

  // Whether to write Hnu flux tree in to file
  bool doHnuFluxFile_;
  // whether to plot Heavy Nu flux
  bool regenerateFluxHnu_;
  // whether to plot Heavy Nu secondary particle characteristics
  bool doSecParticlePlot_;
  // whether to plot number of events
  bool doEventsNumberPlot_;
  // weather to plot heavy nu flux data
  bool doFluxPlot_;
  // weather generate nd280Control script
  bool doControlScript_;

  // reweight every run
  bool FluxReweight_;
  Int_t RunConfNum_;
  Int_t FirstRunConf_;
  Int_t LastRunConf_;

  std::vector<TH1D*> TunedFlux_;
  std::vector<std::pair<TH1D*, TH1D*> > FLuxWeight_;

  // output file name for script
  std::string output_;

  // What reactions we want to study
  // Umu*hadron mode
  bool kKMuMuPi; // K->mu(mu+pi) // mu-/pi+
  bool kKMuElePi; // K->mu(e+pi) // e-/pi+
  bool kKMuPiMu; // K->mu(pi+mu) // pi-/mu+
  bool kKMuPiEle; // K->mu(pi+e) // pi-/e+
  // Umu*Ue
  bool kKMuMuEleNue;  // K->mu(mu+e+Nu(e))
  bool kKMuEleMuNue;  // K->mu(e+mu+Nu(e))
  bool kKMuEleEleNue; // K->mu(e+e+Nu(e))
  // Umu*Umu
  bool kKMuMuMuNumu;  // K->mu(mu+mu+Nu(mu))
  bool kKMuMuEleNumu; // K->mu(mu+e+Nu(mu))
  bool kKMuEleMuNumu; // K->mu(e+mu+Nu(mu))
  // Umu*Umixed
  bool kKMuEleEleNuMix; // K->mu(e+e+Nu(mu,tau))
  bool kKMuMuMuNuMix;   // K->mu(mu+mu+Nu(e,tau))

  // Ue*hadron mode
  bool kKEleElePi; // K->e(e+pi) // e-/pi+
  bool kKEleMuPi; // K->e(mu+pi) // mu-/pi+
  bool kKElePiEle; // K->e(pi+e) // pi-/e+
  bool kKElePiMu; // K->e(pi+mu) // pi-/mu+
  // Ue*Ue
  bool kKEleMuEleNue; // K->e(mu+e+Nu(e)) // mu-/e+
  bool kKEleEleMuNue; // K->e(e+mu+Nu(e)) // e-/mu+
  bool kKEleEleEleNue; // K->e(e+e+Nu(e)) // e-/e+
  // Ue*Umu
  bool kKEleEleMuNumu; // K->e(e+mu+Nu(mu)) // e-/mu+
  bool kKEleMuEleNumu; // K->e(mu+e+Nu(mu)) // mu-/e+
  bool kKEleMuMuNumu; // K->e(mu+mu+Nu(mu))
  // Ue*Umix
  bool kKEleEleEleNuMix; // K->e(e+e+Nu(mu,tau))
  bool kKEleMuMuNuMix; // K->e(mu+mu+Nu(e,tau))

  // Ue*Umix
  bool kPiEleEleEleNuMix; // Pi->e(e+e+Nu(mu,tau)) // e-/e+
  // Ue*Ue
  bool kPiEleMuEleNue; // Pi->e(mu+e+Nu(e)) // mu-/e+
  bool kPiEleEleMuNue; // Pi->e(e+mu+Nu(e)) // e-/mu+
  bool kPiEleEleEleNue; // Pi->e(e+e+Nu(e)) // e-/e+
  // Ue*Umu
  bool kPiEleEleMuNumu; // K->e(e+mu+Nu(mu)) // e-/mu+
  bool kPiEleMuEleNumu; // K->e(mu+e+Nu(mu)) // mu-/e+

  // Umu*Ue
  bool kPiMuEleEleNue; // Pi->mu(e+e+Nu(e))
  // Umu*Umix
  bool kPiMuEleEleNuMix; // Pi->mu(e+e+Nu(mu,tau))

  /// Mass array
  Int_t gSizeMu_;
  std::vector<Double_t> gHnuMass_;

  // for one mass generation
  Int_t ValidMassiveIndex_;

  /// Detector geometry vars
  // Position ofTPCs in detector coordinate system
  std::vector<Double_t> zPosFV_;

  // Vector of TPCs volumes
  std::vector<utils::TBox3D_AAB> tpc_;

  /// Beam mode
  Int_t numode_;

  /// Generate HNL or Anti-HNL decay
  bool AntiHNL_;

  /// Events number data
    struct EventsNumber {
        hnlProdMode::hnlProdEnum ProdMode;
        hnlDecayMode::hnlDecayEnum DecayMode;
        // if there are files with external constrictions
        bool ps191;
        bool arxiv_1212_1062;
        // Number of events histo
        TH1D* histo;
    };

  std::vector<EventsNumber> Events_;

  /// Check correlation histoes
  TH2D* CorrNE;
  TH2D* CorrNZ;
  /// Decay position of Heavy Nu in detector
  TH2D* DecayZY_[2];
  TH2D* DecayZX_[2];

  TH1D* pt_histo;

  TGraph* _anuNorm;
  bool    rdpPOT_;
  Float_t rdpNuPOT_;
  Float_t rdpNuBarPOT_;

  TGraph* Br_;
  TH2D* ToF_;
  TH2D* Ntof_;
  std::vector<std::vector<TH1D*> > tofMass;

  /// Decay ppoducts first particle spectrum (momentum and angle)
  std::vector<std::vector<TH1D*> > DecFirstP_;
  std::vector<std::vector<TH1D*> > DecFirstCos_;
  // Decay ppoducts second particle spectrum
  std::vector< std::vector<TH1D*> > DecSecondP_;
  std::vector< std::vector<TH1D*> > DecSecondCos_;
  // Relative angle
  std::vector< std::vector<TH1D*> > DecRelativeCos_;

  /// Hnu flux tree branches
  /// weight of the event
  Float_t norm_;

  /// neutrino energy
  Float_t Enu_;

  // Origin ``ordinary'' neutrino energy
  Float_t Enu_orig_;

  /// neutrino pos in detetector plane (detector coordinate system)
  Float_t xnu_;

  /// neutrino pos in detetector plane (detector coordinate system)
  Float_t ynu_;

  /// decay position of the parent particle in the global coordinate system
  Float_t xpi_[3];

  /// momentum of the parent particle in the global coordinate system
  Float_t npi_[3];

  Int_t mass_index_;

  /// Meson charge
  Int_t MesonCharge_;

  /// Helicity
  Int_t helicity_;

  // for different POT calculations. "1" means 1e21
  Double_t POTscale_;

  /// NeutRooTracker vars for tree

    /// The generator-specific event flags.
    TBits*       fEvtFlags_;

    /// The generator-specific string with the 'event code'
    TObjString*  fEvtCode_;

    /// The sequence number of the event (the event number).
    int         fEvtNum_;

    /// The cross section for the event (1E-38 cm2)
    double      fEvtXSec_;

    /// The differential cross section for the event kinematics
    /// (1E-38 cm2/{K^n})
    double      fEvtDXSec_;

    /// The weight for the event
    double      fEvtWght_;

    /// The probability for the event (given the cross section, path lengths,
    /// etc.).
    double      fEvtProb_;

    /// The event vertex position in detector coord syst (in meters and
    /// seconds).
    double      fEvtVtx_[4];

    /// The number of particles in the particle arrays to track
    int         fStdHepN_;

    /// The maximum number of particles that can be in the particle arrays.
    static const int kNPmax = 4;

    /// The PDG codes for the particles to track.  This may include generator
    /// specific codes for pseudo particles.
    int         fStdHepPdg_[kNPmax]; //[fStdHepN]

    /// The a generator specific status for each particle.  Particles with a
    /// status equal to 1 will be tracked.
    ///
    /// The HEPEVT status values are as follows:
    /// - 0 : null entry.
    /// - 1 : an existing entry, which has not decayed or fragmented. This is
    ///    the main class of entries, which represents the `final state' given
    ///    by the generator.
    /// - 2 : an entry which has decayed or fragmented and is therefore not
    ///    appearing in the final state, but is retained for event history
    ///    information.
    /// - 3 : a documentation line, defined separately from the event
    ///    history. This could include the two incoming reacting particles,
    ///    etc.
    /// - 4 to 10 :
    ///    undefined, but reserved for future standards.
    /// - 11 to 200 : at the disposal of each model builder for constructs
    ///    specific to his program, but equivalent to a null line in the
    ///    context of any other program.
    /// - 201 and above : at the disposal of users, in particular for event
    /// tracking in the detector.
    ///
    /// The GENIE generator approximately follows the HEPEVT status codes.
    /// As of July 2008, the status values found the GENIE source code are:
    ///   - -1 -- Undefined particle
    ///   -  0 -- An initial state particle.InformationalVertex
    ///   -  1 -- A stable final state particle to be tracked.
    ///   -  2 -- An intermediate particle that should not be tracked.
    ///   -  3 -- A particle which decayed and should not be tracked.  If
    ///            this particle produced daughters to be tracked, they will
    ///            have a state of 1.
    int         fStdHepStatus_[kNPmax]; //[fStdHepN]

    /// The position (x, y, z, t) (fm, second) of the particle in the nuclear
    /// frame
    double      fStdHepX4_[kNPmax][4]; //[fStdHepN]

    /// The 4-momentum (px, py, pz, E) of the particle in the LAB frame (GeV)
    double      fStdHepP4_[kNPmax][4]; //[fStdHepN]

    /// The particle polarization vector.
    double      fStdHepPolz_  [kNPmax][3]; //[fStdHepN]

    /// The index of the first daughter of the particle in the arrays.
    int         fStdHepFd_[kNPmax]; //[fStdHepN]

    /// The index last daughter of the particle in the arrays.
    int         fStdHepLd_[kNPmax]; //[fStdHepN]

    /// The index of the first mother of the particle in there arrays.
    int         fStdHepFm_[kNPmax]; //[fStdHepN]

    /// The index of the last mother of the particle in the arrays.
    int         fStdHepLm_[kNPmax]; //[fStdHepN]

    //////////////////////////////
    /// The following variables are copied more or less directly from the
    /// input flux generator.
    //////////////////////////////

    /// The PDG code of the particle which created the parent neutrino.
    int         fNuParentPdg_;

    /// The interaction mode at the vertex which created the parent neutrino.
    /// This is normally the decay mode of the parent particle.
    int         fNuParentDecMode_;

    /// The 4 momentum of the particle at the vertex which created the parent
    /// neutrino.  This is normally the momentum of the parent particle at the
    /// decay point.
    double      fNuParentDecP4_[4];

    /// The position of the vertex at which the neutrino was created.  This is
    /// passed directly from the beam (or other flux) generator, and is in the
    /// native units of the original generator.
    double      fNuParentDecX4_[4];

    /// The momentum of the parent particle at it's production point.  This is
    /// in the native energy units of the flux generator.
    double      fNuParentProP4_[4];

    /// The position of the parent particle at it's production point.  This
    /// uses the target as the origin and is in the native units of the flux
    /// generator.
    double      fNuParentProX4_[4];

    /// The vertex ID of the parent particle vertex.
    int         fNuParentProNVtx_;
};
#endif