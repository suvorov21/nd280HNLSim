#include "TConfigManager.h"
//oaRuntimeParameters
#include "TParameters.h"

#include "TFile.h"
#include "TTree.h"

/*!
 * Config manager instance 
 */
TConfigManager* TConfigManager::p_instance = NULL;

namespace config{

  DetConfig::DetConfig(const std::string& name, bool read_params){
    name__ = name;
    if (read_params){

      // Read the center and sized of the plane in ineterest 
      // the one where trying to estimate the beam

      first.xPosND = ND::params().GetParameterD("nd280HNLSim.DetConfig.xPosND");
      first.yPosND = ND::params().GetParameterD("nd280HNLSim.DetConfig.yPosND");
      first.zPosND = ND::params().GetParameterD("nd280HNLSim.DetConfig.zPosND");

      first.xSizeND = ND::params().GetParameterD("nd280HNLSim.DetConfig.xSizeND");
      first.ySizeND = ND::params().GetParameterD("nd280HNLSim.DetConfig.ySizeND");

      // Read the FV definition
      second.xSizeFV = ND::params().GetParameterD("nd280HNLSim.DetConfig.xSizeFV");
      second.ySizeFV = ND::params().GetParameterD("nd280HNLSim.DetConfig.ySizeFV");
      second.zSizeFV = ND::params().GetParameterD("nd280HNLSim.DetConfig.zSizeFV");

      second.zPosFV1 = ND::params().GetParameterD("nd280HNLSim.DetConfig.zPosFV1");
      second.zPosFV2 = ND::params().GetParameterD("nd280HNLSim.DetConfig.zPosFV2");
      second.zPosFV3 = ND::params().GetParameterD("nd280HNLSim.DetConfig.zPosFV3");
    }
  }

}


/*!
 * TConfigManager singleton getter 
 */
TConfigManager& TConfigManager::Get(void){
  if(!p_instance)           
    p_instance = new TConfigManager();

  p_instance->select_config_tmp(p_instance->selected_config()); 

  return *p_instance;
}

/*!
 * TConfigManager singleton getter 
 */
TConfigManager& TConfigManager::Get(const std::string& name){
  if(!p_instance)           
    p_instance = new TConfigManager();

  p_instance->select_config_tmp(name);

  return *p_instance;
}

/*!
 * ctor
 */
TConfigManager::TConfigManager(){

  config_map__.clear();

  // add a default manager
  add_config("default");
  select_config("default");
  
  //have initialized so far, has to be done externally
  ini_config_set__ = false;
  
  // Near detector configuration to be used
  NDconfig__ = ND::params().GetParameterD("nd280HNLSim.NDconfig");

}

/*!
 * Add configuration with a given name 
 */
void TConfigManager::add_config(const std::string& name){

  //read the parameters so to always have default set
  if (config_map__.find(name) != config_map__.end()){
    std::cout << "TConfigManager::add_config(). Trying to add a configuration '" << name << "' that already exits. ERROR !!!!!" << std::endl; 
    return;
  }

  config_map__[name] = new config::DetConfig(name, true); 
}

/*!
 * Add configuration with a given name 
 */
void TConfigManager::ReadInitialFluxConfig(const std::string& fileName) {
  
  if (ini_config_set__) return;

  TFile infile(fileName.c_str(),"READ");

  if (!infile.IsOpen()){std::cerr << "BAD FILE! " << fileName << std::endl;
    return;
  } 

  //configuration, posistions etc
  TTree* h3000 = (TTree*)infile.Get("h3000");
  if (!h3000){
    return;
  }
  //header, number of triggers etc
  TTree* h1000 = (TTree*)infile.Get("h1000");
  if (!h1000){
    return;
  }

  if(h3000->GetEntries()<1 || h1000->GetEntries()<1){
    return;
  }

  //center positions for each detector configuration
  h3000->SetBranchAddress("Xfd",  &xPosInit__);
  h3000->SetBranchAddress("Yfd",  &yPosInit__);
  h3000->SetBranchAddress("Zfd",  &zPosInit__);

  h3000->SetBranchAddress("Hfd",  &xSizeInit__);
  h3000->SetBranchAddress("Vfd",  &ySizeInit__);

  //number of triggers per file
  h1000->SetBranchAddress("ntrig",  &nTrigFile__);

  h3000->GetEntry(0);
  h1000->GetEntry(0);
  
  //fill original configuration
  detPlaneInit__.xPosND = xPosInit__[NDconfig__-1]; 
  detPlaneInit__.yPosND = yPosInit__[NDconfig__-1]; 
  detPlaneInit__.zPosND = zPosInit__[NDconfig__-1]; 
  
  detPlaneInit__.xSizeND = xSizeInit__[NDconfig__-1]; 
  detPlaneInit__.ySizeND = ySizeInit__[NDconfig__-1]; 
  
  ini_config_set__ = true;
}


namespace config {
  /*!
   * Instance of the TConfigManager
   */
  TConfigManager& config_man(){
    return TConfigManager::Get(); 
  }

  /*!
   * Instance of the TConfigManager
   */
  TConfigManager& config_man(const std::string& name){
    return TConfigManager::Get(name); 
  }

}
