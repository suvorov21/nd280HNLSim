#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <sstream>
#include <map>


//ROOT
#include <TVector3.h>
#include <TLorentzVector.h>
#include <utility>
#include "TMultiGraph.h"
#include "TGraph.h"
#include "TString.h"
#include "TLegend.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TAxis.h"

//hnuFlux
#include "Units.h"


#define _(par) std::cout << #par <<" value "<< par <<std::endl;
#define stringify(name) #name


/// enums for final mixing elements
/// combination that defines the process
enum processMixEnum{
  kUmu2,
  kUele2,
  kUmuUele,
  kInvalidProcessMix = 0xDEAD
};



///  HNL decay modes
class hnlDecayMode{
public:
  enum hnlDecayEnum{
    kElemPip        = 20,
    kMumPip         = 21,
    kPimElep        = 22,
    kPimMup         = 23,

    kMumElepNumu    = 30,
    kMumElepNue     = 31,
    kElemMupNumu    = 32,
    kElemMupNue     = 33,

    kElemElepNue    = 40,
    kElemElepNuMix  = 41,
    kMumMupNumu     = 42,
    kMumMupNuMix    = 43,
    kInvalid        = 0xDEAD
  };
  enum hnlMixEnum{
    kUmu,
    kUele,
    kInvalidMix = 0xDEAD
  };
  hnlDecayMode(){
    Default();
  } //ctor
  virtual ~hnlDecayMode() = default; //dtor

  explicit hnlDecayMode(hnlDecayMode::hnlDecayEnum mode){
    switch (mode){
      case kElemPip:
        mode_           = mode;
        reaction_       = "N#rightarrow e + #pi";
        mix_name_       = "Ue";
        form_factor_    = units::f_pion;
        cabbibo_angle_  = units::Vud;
        Prod_mass_.push_back(units::mass_electron);
        Prod_mass_.push_back(units::mass_charged_pion);
        ProdCode_.push_back(units::G4electron);
        ProdCode_.push_back(units::G4pion_pos);
        break;
      case kMumPip:
        mode_           = mode;
        reaction_       = "N#rightarrow #mu + #pi";
        mix_name_       = "Umu";
        form_factor_    = units::f_pion;
        cabbibo_angle_  = units::Vud;
        Prod_mass_.push_back(units::mass_muon);
        Prod_mass_.push_back(units::mass_charged_pion);
        ProdCode_.push_back(units::G4muon);
        ProdCode_.push_back(units::G4pion_pos);
        break;
      case kMumElepNue:
        mode_           = mode;
        reaction_       = "N#rightarrow #mu + e + #nu_{e}";
        mix_name_       = "Umu";
        Prod_mass_.push_back(units::mass_muon);
        Prod_mass_.push_back(units::mass_electron);
        Prod_mass_.push_back(0.);
        ProdCode_.push_back(units::G4muon);
        ProdCode_.push_back(units::G4electron);
        break;
      case kElemMupNumu:
        mode_           = mode;
        reaction_       = "N#rightarrow e + #mu + #nu_{mu}";
        mix_name_       = "Ue";
        Prod_mass_.push_back(units::mass_electron);
        Prod_mass_.push_back(units::mass_muon);
        Prod_mass_.push_back(0.);
        ProdCode_.push_back(units::G4electron);
        ProdCode_.push_back(units::G4muon);
        break;
      case kMumMupNumu:
        mode_           = mode;
        reaction_       = "N#rightarrow #mu + #mu + #nu_{mu}";
        mix_name_       = "Umu";
        Prod_mass_.push_back(units::mass_muon);
        Prod_mass_.push_back(units::mass_muon);
        Prod_mass_.push_back(0.);
        ProdCode_.push_back(units::G4muon);
        ProdCode_.push_back(-units::G4muon);
        break;
      case kElemElepNue:
        mode_           = mode;
        reaction_       = "N#rightarrow e + e + #nu_{e}";
        mix_name_       = "Ue";
        Prod_mass_.push_back(units::mass_electron);
        Prod_mass_.push_back(units::mass_electron);
        Prod_mass_.push_back(0.);
        ProdCode_.push_back(units::G4electron);
        ProdCode_.push_back(-units::G4electron);
        break;
      case kElemElepNuMix:
        mode_           = mode;
        reaction_       = "N#rightarrow e + e + #nu_{e,#tau}";
        mix_name_       = "UeUtauMix";
        Prod_mass_.push_back(units::mass_electron);
        Prod_mass_.push_back(units::mass_electron);
        Prod_mass_.push_back(0.);
        ProdCode_.push_back(units::G4electron);
        ProdCode_.push_back(-units::G4electron);
        break;
      case kMumMupNuMix:
        mode_           = mode;
        reaction_       = "N#rightarrow #mu + #mu + #nu_{#mu,#tau}";
        mix_name_       = "UmuUtauMix";
        Prod_mass_.push_back(units::mass_muon);
        Prod_mass_.push_back(units::mass_muon);
        Prod_mass_.push_back(0.);
        ProdCode_.push_back(units::G4muon);
        ProdCode_.push_back(-units::G4muon);
        break;
      default:
        Default();
        break;
    }
  }

  hnlDecayEnum GetDecayMode() const {return mode_;};

  std::string GetMix() const {return mix_name_;};

  std::string GetReaction() const {return reaction_;}

  std::vector<Double_t> GetProdMass() const {return Prod_mass_;}

  std::vector<Int_t> GetPDGCode() const {return ProdCode_;}

  Double_t GetFormFactor() const {return form_factor_;}

  Double_t GetCabbibo() const {return cabbibo_angle_;}

  std::vector<long double> GetWidth(std::vector<Double_t> gHnuMass);

private:
  void Default(){
    mode_ = hnlDecayMode::kInvalid;
  }

  hnlDecayEnum mode_;
  std::string mix_name_;
  std::string reaction_;
  std::vector<Double_t> Prod_mass_;
  std::vector<Int_t> ProdCode_;
  Double_t form_factor_;
  Double_t cabbibo_angle_;

};

/// HNL production modes
class hnlProdMode{
public:

  enum hnlProdEnum{
    kKmu2       = 12,
    kKe2        = 13,
    kPimu2      = 11,
    kPiE2       = 10,
    kInvalid    = 0xDEAD
  };

  enum hnlParentEnum {
    kKmu        = 12,
    kPimu       = 11
  };
  hnlProdMode() {
    Default();
  } //ctor
  ~hnlProdMode() = default; //dtor
  explicit hnlProdMode(hnlProdMode::hnlProdEnum mode){
    switch (mode){
      case kKmu2:
        mode_          = mode;
        parent_mode_   = kKmu;
        parent_code_   = units::G4kaon_pos;
        reaction_      = "K#rightarrow #mu + N";
        mix_name_      = "Umu";
        mode_name_     = stringify(kKmu2);
        parent_mass_   = units::mass_charged_kaon;
        blepton_mass_  = units::mass_muon;
        form_factor_   = units::f_kaon;
        cabibbo_       = units::Vus;
        tBase_         = units::t_charged_kaon;
        break;
      case kKe2:
        mode_          = mode;
        parent_mode_   = kKmu;
        parent_code_   = units::G4kaon_pos;
        reaction_      = "K#rightarrow e + N";
        mix_name_      = "Ue";
        mode_name_     = stringify(kKe2);
        parent_mass_   = units::mass_charged_kaon;
        blepton_mass_  = units::mass_electron;
        form_factor_   = units::f_kaon;
        cabibbo_       = units::Vus;
        tBase_         = units::t_charged_kaon;
        break;
      case kPimu2:
        mode_          = mode;
        parent_mode_   = kPimu;
        parent_code_   = units::G4pion_pos;
        reaction_      = "#pi #rightarrow #mu + N";
        mix_name_      = "Umu";
        mode_name_     = stringify(kPimu2);
        parent_mass_   = units::mass_charged_pion;
        blepton_mass_  = units::mass_muon;
        form_factor_   = units::f_pion;
        cabibbo_       = units::Vud;
        tBase_         = units::t_charged_pion;
        break;
      case kPiE2:
        mode_          = mode;
        parent_mode_   = kPimu;
        parent_code_   = units::G4pion_pos;
        reaction_      = "#pi #rightarrow e + N";
        mix_name_      = "Ue";
        mode_name_     = stringify(kPiE2);
        parent_mass_   = units::mass_charged_pion;
        blepton_mass_  = units::mass_electron;
        form_factor_   = units::f_pion;
        cabibbo_       = units::Vud;
        tBase_         = units::t_charged_pion;
        break;
      default:
        Default();
        break;
    }
  }
  Double_t GetParentMass() const {
    return parent_mass_;
  }

  std::string GetMix() const {return mix_name_;};

  std::string GetReaction() const {return reaction_;}

  Double_t GetProdMass() const {return blepton_mass_;}

  hnlParentEnum GetParentEnum() const {return parent_mode_;}

  std::string GetModeName() const {
    return mode_name_;
  }
  hnlProdMode::hnlProdEnum GetProdMode() const {
    return mode_;
  }
  Double_t GetFormFactor() const {
    return form_factor_;
  }
  Double_t GetCabibbo() const {
    return cabibbo_;
  }
  Double_t GetTBase() const {
    return tBase_;
  }
  Double_t GetParentCode() const {
    return parent_code_;
  }
  void PrintModes() {
    std::cout<<"hnlProdEnum::available modes: "<<std::endl;
    _(kKmu2);
    _(kPimu2);
  }
  void Print(){
    std::cout<<"hnlProdMode:"<<std::endl;
    _(mode_);
    _(parent_mass_);
    _(form_factor_);
    _(cabibbo_);
    _(tBase_);
  }
private:
  void Default(){
    parent_mass_ =   units::kUnassigned;
    mode_name_   =   "";
    mode_        =   hnlProdMode::kInvalid;
    form_factor_ =   units::kUnassigned;
    cabibbo_     =   units::kUnassigned;
    tBase_       =   units::kUnassigned;
  }
  Double_t      parent_mass_;
  Int_t         parent_code_;
  std::string   mode_name_;
  std::string   reaction_;
  std::string   mix_name_;
  hnlProdEnum   mode_;
  hnlParentEnum parent_mode_;
  Double_t      form_factor_;
  Double_t      cabibbo_;
  Double_t      tBase_; // lifetime (1/decay_width) for the mode itself in case of the mass-less neutrino
  Double_t      blepton_mass_; // "base" (non-neutrino) lepton mass for the bass process

};

/*!
 * A namespace to provide a storage for some utility functions and classes
 */

namespace utils {

  /// convert input to production enum
  hnlProdMode::hnlProdEnum GetProdEnum(int input);

  class TRay{
    public:
      /// a constructor given a position and direction
      TRay(const TVector3&, const TVector3&);

      /// a default destructor
      virtual ~TRay() = default;;

      const TVector3& GetPos()    const {return pos_;}
      const TVector3& GetDir()    const {return dir_;}
      const TVector3& GetInvDir() const {return invDir_;}

      TVector3 Propagate(Double_t distance) const {return pos_ + distance*dir_;}


    private:
      TVector3 pos_;
      TVector3 dir_;
      TVector3 invDir_;
  };

  class TRectangle{
    public:
      /// constructor provided normal, "main" (U) axis and sizes
      TRectangle(const TVector3&, const TVector3&, const TVector3&,
          Double_t, Double_t);

      /// default destructor
      virtual ~TRectangle() = default;;

      /// return true if the point is inside the rectangle
      bool IsInside(const TVector3&) const;

      const TVector3& GetPos() const {return pos_;}

      Double_t GetU() const {return U_;}
      Double_t GetV() const {return U_;}

      const TVector3& GetUaxis() const {return vaxis_;}
      const TVector3& GetVaxis() const {return uaxis_;}

      const TVector3& GetNormal() const {return norm_;}

      /// return closest distance from the point to the plane
      Double_t ClosestDistance(const TVector3& r0) const {
        return (pos_ - r0).Dot(norm_);
      }

      /// whether a ray intersect this plane
      bool Intersect(const TRay&, Double_t&) const;

    private:

      Double_t U_;
      Double_t V_;

      TVector3 pos_;
      TVector3 norm_;

      TVector3 uaxis_;
      TVector3 vaxis_;
  };

  /// this is a simple box in 3D, for the moment axis aligned one (as is ND280 complex)
  class TBox3D_AAB{
    public:
      /// a constructor given a position of the center and half-sizes for each side
      TBox3D_AAB(const TVector3&, Double_t, Double_t, Double_t);

      /// default destructor
      virtual ~TBox3D_AAB() = default;;

      const TVector3& GetPos() const {return pos_;}

      Double_t GetDx() const {return Dx_;}
      Double_t GetDy() const {return Dy_;}
      Double_t GetDz() const {return Dz_;}

      const TVector3& GetMin() const {return min_;}
      const TVector3& GetMax() const {return max_;}

      /// whether a ray intersect this box
      bool Intersect(const TRay&, Double_t&, Double_t&) const;

    private:

      /// position of the center
      TVector3 pos_;

      /// half sizes
      Double_t Dx_;
      Double_t Dy_;
      Double_t Dz_;

      /// edge coordinates of the box
      TVector3 min_;
      TVector3 max_;
  };

  /// width of charged meson decay into charged lepton and heavy neutral one
  /// this is pure "kinematics", no additional mixing included
  /// requires: meson mass, form-factor, cabbibo-mixing element, lepton mass and heavy lepton mass
  /// arxiv:0705.1729
  long double WidthMesonToHNuLepton(Double_t, Double_t, Double_t, Double_t, Double_t);

  /// width of 2-body decay of the neutral lepton (heavy neutrino)
  /// pure kinematics, mixing element is not considered
  /// requires: heavy nu mass, form-factor, cabbibo mixing element, lepton mass and meson mass
  /// arxiv:0705.1729
  long double WidthHNuToMesonLepton(Double_t, Double_t, Double_t, Double_t, Double_t);

  /// width of 3-body decay of the neutral lepton (heavy neutrino)
  /// di-lepton case: two particle & anti-particle leptons and a neutrino
  /// pure kinematics
  /// requires: heavy nu mass, leton mass, whether all have same flavor
  long double WidthHNuToDiLeptonNu(Double_t, Double_t, bool);

  /// width of 3-body decay of the neutral lepton (heavy neutrino)
  /// lepton+lepton+neutrino (same flavor as one lepton) case: lepton1 != lepton2
  /// pure kinematics
  /// requires: heavy nu mass, first lepton mass, second lepton mass
  long double WidthHNuTo2LeptonNu(Double_t, Double_t, Double_t);
}

#endif
