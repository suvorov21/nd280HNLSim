#ifndef TTWOBODYDECAY_H
#define TTWOBODYDECAY_H

#include <iostream>

#include "TLorentzVector.h"
#include "TVector3.h"
#include "TGenPhaseSpace.h"

namespace kin {

  /// A class two deal with two body decays
  class TTwoBodyDecay
  {
  
    public:
      
      /// constructor given a mass of the decaying particles and two daughters
      TTwoBodyDecay(Double_t, Double_t, Double_t);

      virtual ~TTwoBodyDecay()= default;;

      /// probability to decay into given solid angle (and per unit area) 
      /// defined as the angle between the parent direction (first parameter is 3D momentum)
      ///  and decay point & detector position (hence dir and distance can be estimated) 
      /// note! this is performed for the second daughter for which also lab frame energy is returned
      // since two solutions may be possible then return a  vector of pairs: probability (weight) and corresponding energy 
      std::vector<std::pair<Double_t, Double_t> > Weight(const TVector3&, const TVector3&, const TVector3&) const;

      /// get decay products in lab frame given momentum
      bool Generate(const TVector3&);  

      /// get decay products in lab frame given energy and direction
      bool Generate(Double_t, const TVector3&); 

      /// getters
      Double_t GetMassParent()const {return M_parent_;} 

      Double_t GetMassProd1() const {return *M_prod_;} 

      Double_t GetMassProd2() const {return *(M_prod_+1);} 

      const TLorentzVector& GetDecayProd1()const {return *products_;} 
      
      const TLorentzVector& GetDecayProd2() const {return *(products_+1);} 

      /// setters
      void SetNorm(Double_t norm){norm_ = norm;}

      void SetMassProd1(Double_t mass);
    
      void SetMassProd2(Double_t mass); 
 
    protected:

      ///normalization
      Double_t norm_;

      ///decay products
      TLorentzVector products_[2];  

      ///parent mass
      Double_t M_parent_;

      ///products masses
      Double_t M_prod_[2];
 
      Double_t Ecm_prod_[2];

      Double_t beta_cm_[2];

      Double_t Pcm_prod_;

      TGenPhaseSpace event_;

  };

}

#endif
