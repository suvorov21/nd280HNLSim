#include "Utils.h"

#include <cmath>
#include <iostream>
#include "TMath.h"

std::vector<long double> hnlDecayMode::GetWidth(std::vector<Double_t> gHnuMass) {
  std::vector<long double> result;
  result.resize(gHnuMass.size(), 0);

  if ((mode_ == kElemPip) || (mode_ == kMumPip) || (mode_ == kPimElep) || (mode_ == kPimMup)) {
    for (unsigned int i = 0; i < gHnuMass.size(); ++i) {
      if (gHnuMass[i] < Prod_mass_[0] + Prod_mass_[1])
        continue;
      result[i] = utils::WidthHNuToMesonLepton(gHnuMass[i], form_factor_, cabbibo_angle_, Prod_mass_[0], Prod_mass_[1]);
    }
    return result;
  }
  if ((mode_ == kMumElepNue) || (mode_ == kElemMupNumu) || (mode_ == kMumElepNumu) || (mode_ == kElemMupNue)){
    for (unsigned int i = 0; i < gHnuMass.size(); ++i) {
      if (gHnuMass[i] < Prod_mass_[0] + Prod_mass_[1])
        continue;
      result[i] = utils::WidthHNuTo2LeptonNu(gHnuMass[i], Prod_mass_[0], Prod_mass_[1]);
    }
    return result;
  }

  if ((mode_ == kMumMupNumu) || (mode_ == kElemElepNue)) {
    for (unsigned int i = 0; i < gHnuMass.size(); ++i) {
      if (gHnuMass[i] < Prod_mass_[0] + Prod_mass_[1])
        continue;
      result[i] = utils::WidthHNuToDiLeptonNu(gHnuMass[i], Prod_mass_[0], true); // only one flavour
    }
    return result;
  }

  if ((mode_ == kElemElepNuMix) || (mode_ == kMumMupNuMix)) {
    for (unsigned int i = 0; i < gHnuMass.size(); ++i) {
      if (gHnuMass[i] < Prod_mass_[0] + Prod_mass_[1])
        continue;
      result[i] = 2*utils::WidthHNuToDiLeptonNu(gHnuMass[i], Prod_mass_[0], false); // 2 flavours to sum over
    }
    return result;
  }
  return result;
}

namespace utils {

  /*!
   * convert int input to HNL production enum
   */
  hnlProdMode::hnlProdEnum GetProdEnum(int input){
    switch (input){
    case hnlProdMode::kKmu2:
      return hnlProdMode::kKmu2;

    case hnlProdMode::kKe2:
      return hnlProdMode::kKe2;

    case hnlProdMode::kPimu2:
      return hnlProdMode::kPimu2;

    case hnlProdMode::kPiE2:
      return hnlProdMode::kPiE2;

    default:
      return hnlProdMode::kInvalid;
    }
  }

  hnlDecayMode::hnlDecayEnum GetDecayEnum(int input){
    switch (input){
      case hnlDecayMode::kElemPip:
        return hnlDecayMode::kElemPip;

      case hnlDecayMode::kMumPip:
        return hnlDecayMode::kMumPip;

      case hnlDecayMode::kPimElep:
        return hnlDecayMode::kPimElep;

      case hnlDecayMode::kPimMup:
        return hnlDecayMode::kPimMup;

      case hnlDecayMode::kMumElepNue:
        return hnlDecayMode::kMumElepNue;

      case hnlDecayMode::kElemMupNumu:
        return hnlDecayMode::kElemMupNumu;

      case hnlDecayMode::kMumElepNumu:
        return hnlDecayMode::kMumElepNumu;

      case hnlDecayMode::kElemMupNue:
        return hnlDecayMode::kElemMupNue;

      case hnlDecayMode::kMumMupNumu:
        return hnlDecayMode::kMumMupNumu;

      case hnlDecayMode::kElemElepNue:
        return hnlDecayMode::kElemElepNue;

      case hnlDecayMode::kElemElepNuMix:
        return hnlDecayMode::kElemElepNuMix;

      case hnlDecayMode::kMumMupNuMix:
        return hnlDecayMode::kMumMupNuMix;

      default:
        return hnlDecayMode::kInvalid;

    }
  }

  /*!
   * ctor
   */
  TRay::TRay(const TVector3& pos, const TVector3& dir){
    dir_ = dir.Unit();
    pos_ = pos;

    //inverse direction
    Double_t x = dir_.X()!=0 ? 1/dir_.X() : DBL_MAX;
    Double_t y = dir_.Y()!=0 ? 1/dir_.Y() : DBL_MAX;
    Double_t z = dir_.Z()!=0 ? 1/dir_.Z() : DBL_MAX;

    invDir_.SetXYZ(x,y,z);
  }

  /*!
   * ctor
   */
  TRectangle::TRectangle(const TVector3& pos, const TVector3& norm, const TVector3& uaxis,
      Double_t U, Double_t V){

    norm_ = norm.Unit();

    U_ = fabs(U);
    V_ = fabs(V);

    pos_ = pos;

    uaxis_ = uaxis.Unit();
    vaxis_ = norm_.Cross(uaxis_);
  }

  /*!
   * whether a point belongs to this rectangle
   */
  bool TRectangle::IsInside(const TVector3& r0) const {

    TVector3 res = (r0  - pos_);

    if (ClosestDistance(r0) > units::tolerance)
      return false;
    if ( fabs(res.Dot(uaxis_)) > U_  )
      return false;
    if ( fabs(res.Dot(vaxis_)) > V_ )
      return false;

    return true;
  }

  /*!
   * Whether this rectangle is intersected by a ray, return the distance
   */
  bool TRectangle::Intersect(const TRay& ray, Double_t& distance) const{

    distance = units::kUnassigned;

    auto u  = ray.GetDir();
    const auto& r0 = ray.GetPos();

    u = u.Unit();

    Double_t cos_theta = u.Dot(norm_);
    if (fabs(cos_theta) == 0) return false;

    distance = ClosestDistance(r0)/cos_theta;

    return IsInside(r0 + distance*u);
  }
  /*!
   * ctor
   */
  TBox3D_AAB::TBox3D_AAB(const TVector3& pos, Double_t Dx, Double_t Dy, Double_t Dz ){
    pos_   = pos;

    Dx_  = fabs(Dx);
    Dy_  = fabs(Dy);
    Dz_  = fabs(Dz);

    // get the edges
    min_ = TVector3(pos - TVector3(Dx_, Dy_, Dz_));
    max_ = TVector3(pos + TVector3(Dx_, Dy_, Dz_));
  }

  /*!
   * Whether this box is intersected by a given ray, return two param corresponding to intersection
   */

  bool TBox3D_AAB::Intersect(const TRay& ray, Double_t& t_min, Double_t& t_max) const {

    t_min = units::kUnassigned;
    t_max = units::kUnassigned;

    Double_t tmin, tmax, tymin, tymax, tzmin, tzmax;

    // X
    tmin = (min_.X() - ray.GetPos().X()) * ray.GetInvDir().X();
    tmax = (max_.X() - ray.GetPos().X()) * ray.GetInvDir().X();
    if (tmin > tmax) std::swap(tmin, tmax);

    // Y
    tymin = (min_.Y() - ray.GetPos().Y()) * ray.GetInvDir().Y();
    tymax = (max_.Y() - ray.GetPos().Y()) * ray.GetInvDir().Y();

    if (tymin > tymax) std::swap(tymin, tymax);
    if ((tmin > tymax) || (tymin > tmax))
      return false;

    if (tymin > tmin)
      tmin = tymin;
    if (tymax < tmax)
      tmax = tymax;

    // Z
    tzmin = (min_.Z() - ray.GetPos().Z()) * ray.GetInvDir().Z();
    tzmax = (max_.Z() - ray.GetPos().Z()) * ray.GetInvDir().Z();
    if (tzmin > tzmax) std::swap(tzmin, tzmax);

    if ((tmin > tzmax) || (tzmin > tmax))
      return false;

    if (tzmin > tmin)
      tmin = tzmin;
    if (tzmax < tmax)
      tmax = tzmax;

    //set the output values
    t_min =  tmin;

    t_max = tmax;

    return true;
  }



  /*!
   * meson mass, life-time, form-factor, cabbibo-mixing element, lepton mass and heavy lepton mass
   */
  long double WidthMesonToHNuLepton(Double_t Mh, Double_t fh, Double_t Vc, Double_t Ml, Double_t Mnu){

    long double Mnu2 = Mnu*Mnu;
    long double Mh2  = Mh*Mh;
    long double Ml2  = Ml*Ml;


    long double A = 1. - Mnu2/Mh2 + 2.*Ml2/Mh2 + (Ml2/Mnu2)*(1. - Ml2/Mh2);

    long double B = sqrt(pow((1. + Mnu2/Mh2 - Ml2/Mh2),2) - 4.*Mnu2/Mh2);

    long double factor  = (Vc*Vc)*((units::Gfermi*units::Gfermi)*(fh*fh)*Mh)/(8.*TMath::Pi());


    //if mass is very small
    if(Mnu<units::min_mass){
      return   factor*Ml2*(1 - Ml2/Mh2)*(1 - Ml2/Mh2)*units::inv_hplanck;

    }
    return factor*(Mnu*Mnu)*A*B*units::inv_hplanck;
  }

  /*!
   * heavy nu mass, form-factor, cabbibo mixing element, lepton mass and meson mass
   */
  long double WidthHNuToMesonLepton(Double_t Mnu, Double_t fh, Double_t Vc, Double_t Ml, Double_t Mh){

    long double Mnu2 = Mnu*Mnu;
    long double Mh2  = Mh*Mh;
    long double Ml2  = Ml*Ml;

    long double A = pow((1. - Ml2/Mnu2),2) - Mh2/Mnu2*(1. + Ml2/Mnu2);

    long double B = sqrt((1. - pow((Mh - Ml),2)/Mnu2)*(1.-pow((Mh + Ml),2)/Mnu2));

    long double factor  = ((units::Gfermi*units::Gfermi)*(Vc*Vc)*(fh*fh)*pow(Mnu,3))/(16.*TMath::Pi());

    return factor*A*B*units::inv_hplanck;

  }

  /*!
   * requires: heavy nu mass, leton mass
   */
  long double WidthHNuToDiLeptonNu(Double_t Mnu, Double_t Ml, bool same_flavor){

    if(Mnu<2*Ml)
      std::cout<<"utils::WidthHNuToDiLeptonNu() HNL mass smaller than 2*mass_lepton \n"
        << "cannot proceed!" <<std::endl;

    long double factor  = (units::Gfermi*units::Gfermi)*std::pow(Mnu, 5)/(192.*std::pow(TMath::Pi(), 3));

    long double C1 = 0.25*(1 - 4*units::sin_thw2 + 8.*pow(units::sin_thw2,2.));

    long double C2 = 0.5*units::sin_thw2*(2.*units::sin_thw2 - 1);

    long double C3 = 0.25*(1+4*units::sin_thw2+8*pow(units::sin_thw2,2.));

    long double C4 = 0.5*units::sin_thw2*(2.*units::sin_thw2 + 1);

    auto delta_ab = (int)same_flavor;

    long double xl  = Ml/Mnu;
    long double xl2 = xl*xl;
    long double xl4 = xl2*xl2;
    long double xl6 = xl4*xl2;

    long double L1 = (1/xl2)*(1. - 3.*xl2 - (1. - xl2)*(std::sqrt(1. - 4.*xl2)));
    long double L2 = (1. + std::sqrt(1. - 4.*xl2));
    long double L  = std::log(L1) - std::log(L2);

    long double main13 = C1*(1 - delta_ab) + C3*delta_ab;
    main13 *= (1.-14.*xl2-2.*xl4-12.*xl6)*std::sqrt(1. - 4.*xl2) + 12.*xl4*(xl4-1.)*L;

    long double main24 = 4.*(C2*(1 - delta_ab)+C4*delta_ab);
    main24 *= xl2*(2. + 10.*xl2 - 12.*xl4)*std::sqrt(1. - 4.*xl2)+6.*xl4*(1 - 2.*xl2 + 2*xl4)*L;

    return factor*(main13 + main24)*units::inv_hplanck;;
  }

  /*!
   * requires: heavy nu mass, first leton mass, second lepton mass
   */
  long double WidthHNuTo2LeptonNu(Double_t Mnu, Double_t Ml1, Double_t Ml2){

    if(Mnu<Ml1+Ml2)
      std::cout<<"utils::WidthHNuToDiLeptonNu() HNL mass smaller than sum of lepton masses \n"
        << "cannot proceed!" <<std::endl;

    long double factor  = (units::Gfermi*units::Gfermi)*std::pow(Mnu, 5)/(192.*std::pow(TMath::Pi(), 3));

    long double xl = std::max(Ml1, Ml2);
    xl /= Mnu;

    long double xl2 = xl*xl;
    long double xl4 = xl2*xl2;
    long double xl6 = xl4*xl2;
    long double xl8 = xl4*xl4;


    long double main = 1 -  8.*xl2 + 8*xl6 - xl8 - 12.*xl4*std::log(xl2);

    return factor*main*units::inv_hplanck;
  }
} //namespace
