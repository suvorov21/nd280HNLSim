#ifndef fluxGen_H
#define fluxGen_H

#include <iostream>
#include <sstream>
#include <map>

#include "TVector3.h"
#include "TBits.h"
#include "TObjString.h"

#include "Utils.h"

class TH1D;
class TH2D;
class TFile;
class TChain;
class TGraph;
class TTree;
class G4Event;

namespace gen {
  // a simple  class to look a flux files and re-weghting for  heavy neutrinos
  class fluxGen {
   public:

    fluxGen();
    virtual ~fluxGen();
    void Initialize(std::string, std::vector<Double_t>*);

    // Generate flux in current mode
    bool Generate(hnlProdMode::hnlProdEnum, std::string);

  protected:
    // input chaun from root files
    bool ChainInputFiles();

    /// Select random point at detector plane
    TVector3 GenerateDetPos();

  protected:

    bool initialized_;

    // mass array
    Int_t gSizeMu_;
    Double_t MinMass_, MaxMass_;
    std::vector<Double_t> gHnuMass_;

    std::string inputListFiles_;
    std::string outputFileName_;

    // width of Hnu production
    std::vector<Double_t> width_;

    // flux histoes
    std::vector<TH1D*> HnuFluxHistos_;
    TH1D* baseline_;
    TH2D* parent_;

    /// input data from MC flux beam
    TChain* chain_;
    Long64_t nEntries_;

    /// parent particle id based on Geant
    Int_t ppid_;

    /// momentum of the parent particle
    Float_t ppi_;

    /// norm to get flux/detector and 1e21 per file
    Float_t norm_;

    /// neutrino production mode
    Int_t mode_;

    /// Nu (1) or Anti-Nu (-1) mode to study
    Int_t NuMode_;

    /// neutrino energy
    Float_t Enu_;

    /// neutrino pos in detetector plane (detector coordinate system)
    Float_t xnu_;

    /// neutrino pos in detetector plane (detector coordinate system)
    Float_t ynu_;

    /// decay position of the parent particle in the global coordinate system
    Float_t xpi_[3];

    /// direction vector of the parent particle in the global coordinate system
    Float_t npi_[3];

    /// number of parents (ng==2 means the parent partilce of nu was produced in
    /// primary interaction)
    Int_t ng_;

    /// Detector ID number
    Int_t idfd_;

    /// cos of the angle between the nu parent particle and the beam
    Float_t cospibm_;

    /// Baseline branching
    Double_t br_base_;

    /// Initial flux for kmu2
    TH1D*  iniFlux_;

    /// Wich configuration to read
    Int_t NDconfig_;

    // detector front square
    Double_t plane_ini_;

    /// Total number of triggers considered
    ULong_t nTrigAll_;
  };
}
#endif
