#include "HeavyNu.h"

int main(int argc, char *argv[]){
  HeavyNu HeavyNu(argc, argv);
  HeavyNu.Initialize();
  HeavyNu.Process();
  HeavyNu.Finalize();
}